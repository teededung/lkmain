<?php
if (isset($_COOKIE['laniakeaA'])):
    // Login test
    $auth = $_COOKIE['laniakeaA'];
    $account = ragnarok_login_by_auth($auth);

    if (is_data_return_ok($account)):
        // Parse data
        $account_id = $account['account_id'];
        $username = $account['username'];
        $password = $account['password'];
        $ragnarok_db = connect_ragnarok_db();
    else:
        setcookie('laniakeaA', null, -1, '/');
        header("Refresh:0");
        exit;
    endif;
else:
    wp_redirect(home_url('/dang-nhap/'), 301);
    exit;
endif;
?>

<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>
    <div class="app-tai-khoan">
        <div class="container content">
            <?php
            $row_account = $ragnarok_db['db']->get_row(
                $ragnarok_db['db']->prepare('SELECT login.account_id, login.userid, login.sex, login.email, login.state, login.lastlogin, login.birthdate FROM `login` WHERE `userid` = %s AND `user_pass` = %s', $username, $password)
            );
            ?>
            <div class="row mb-3">
                <div class="col-lg-5 col-personal">
                    <?php if ($row_account): ?>
                    <div class="card border-primary mb-4">
                        <h5 class="card-header border-primary">Thông tin cá nhân</h5>
                        <table class="table">
                            <tr>
                                <td style="vertical-align: middle">Ngày sinh:</td>
                                <td style="vertical-align: middle"><input type="text" id="birthday" placeholder="Chưa có" value="<?php echo $row_account->birthdate ?>" class="form-control"></td>
                            </tr>
<!--                            <tr>-->
<!--                                <td>Giới tính:</td>-->
<!--                                <td>--><?php //echo ($row_account->sex == 'M') ? 'Nam' : 'Nữ' ?><!--</td>-->
<!--                            </tr>-->
                            <tr>
                                <td>Email:</td>
                                <td><?php echo $row_account->email; ?></td>
                            </tr>
                        </table>
                    </div>

                    <div class="card border-primary mb-4">
                        <h5 class="card-header border-primary">Thông tin tài khoản</h5>
                        <table class="table">
                            <tr>
                                <td>Account ID</td>
                                <td><?php echo $row_account->account_id ?></td>
                            </tr>
                            <tr>
                                <td>Tên đăng nhập:</td>
                                <td><?php echo $row_account->userid; ?></td>
                            </tr>
                            <tr>
                                <td>Lần đăng nhập cuối:</td>
                                <td><?php echo $row_account->lastlogin; ?></td>
                            </tr>
                            <tr>
                                <td>Trạng thái:</td>
                                <td><?php echo get_account_state_name($row_account->state) ?></td>
                            </tr>
                            <tr>
                                <td>CP:</td>
                                <td><?php echo number_format(get_account_cp($row_account->account_id), 0, ".", ","); ?></td>
                            </tr>
                        </table>
                        <div class="card-footer">
                            <button class="btn btn-primary" @click="changePassword">Đổi mật khẩu</button>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>

                <div class="col-lg-7 col-characters">
                    <h2 class="mb-3">Danh sách nhân vật</h2>

                    <?php
                    $row_chars = $ragnarok_db['db']->get_results(
                        $ragnarok_db['db']->prepare('SELECT account_id, char_id, name, class, base_level, job_level, zeny, sex, online FROM `char` WHERE account_id = %d', $account_id)
                    );
                    ?>
                    <?php if ($row_chars): ?>
                        <div class="row justify-content-center">
                            <?php foreach($row_chars as $char): ?>
                                <div class="col-md-6 col-character">
                                    <div class="box-character rounded">

                                        <div class="w-gender">
                                            <div class="w-gender-img">
                                                <?php if ($char->sex == 'M'): ?>
                                                    <img src="<?php echo get_template_directory_uri() . '/images/head-male.png' ?>" alt="ragnarok-mat-nam">
                                                <?php else: ?>
                                                    <img src="<?php echo get_template_directory_uri() . '/images/head-female.png' ?>" alt="ragnarok-mat-nu">
                                                <?php endif; ?>
                                            </div>
                                        </div>

                                        <div class="w-level">
                                            <?php echo $char->base_level; ?> / <?php echo $char->job_level; ?>
                                        </div>

                                        <div class="w-basic-char-info">
                                            <h4><?php echo $char->name ?></h4>
                                            <h5 class="text-muted jobname"><?php echo get_job_name($char->class) ?></h5>
                                            <h6><?php echo number_format($char->zeny , 0, '.', ','); ?> Zeny</h6>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php else: ?>
                        <div class="alert alert-info">
                            Bạn chưa tạo nhân vật. Hãy vào game và tạo một nhân vật nhé :D
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <h2 class="mb-3">Các album ảnh</h2>
            <?php
            $terms = get_terms(array(
                'taxonomy' => 'album_category',
                'hide_empty' => false,
            ));
            ?>
            <div class="row">
                <?php if ($terms): ?>
                    <?php foreach ($terms as $key => $term): ?>
                        <?php
                        $term_name = $term->name;
                        $term_slug = $term->slug;

                        $args = array(
                            'post_type' => 'albums',
                            'post_status' => array('pending'),
                            'posts_per_page' => 1,
                            'meta_query' => array(
                                'relation' => 'AND',
                                array(
                                    'key'	 	=> 'account_id',
                                    'value'	  	=> $account_id,
                                    'compare' 	=> '=',
                                ),
                            ),
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'album_category',
                                    'field'    => 'slug',
                                    'terms'    => $term_slug,
                                ),
                            ),
                        );

                        $album_object = new WP_Query($args);
                        $term_id_for_acf = 'album_category_' . $term->term_id;
                        $description = $term->description;
                        $term_img = get_field( 'category_image', $term_id_for_acf );
                        $allow_upload = get_field( 'allow_upload', $term_id_for_acf );
                        $upload_message = "";

                        if ($allow_upload):
                            $can_upload = false;
                            $start_time_str = get_field( 'upload_start_time', $term_id_for_acf );
                            $end_time_str = get_field( 'upload_end_time', $term_id_for_acf );
                            $description = get_field( 'upload_description', $term_id_for_acf );

                            if ($end_time_str == "" || ($start_time_str == "" && $end_time_str == "")):
                                $can_upload = true;
                            else:
                                $start_time = strtotime($start_time_str);
                                $end_time = strtotime($end_time_str);
                                $now = time();

                                if ($now > $start_time):
                                    if ($now < $end_time):
                                        $can_upload = true;
                                    endif;
                                else:
                                    $upload_message = "Bạn có thể tải ảnh lên vào album bắt đầu vào ngày <strong>" . date("d/m/Y", $start_time) . "</strong> lúc <strong>" . date('H:i', $start_time) . "</strong> (" . human_time_diff( current_time( 'timestamp' ,7), $start_time ) . " nữa).";
                                endif;
                            endif;
                        endif;
                        ?>

                        <div class="col-md-6">
                            <?php if ($album_object->have_posts()): ?>
                                <?php while ($album_object->have_posts()): $album_object->the_post(); ?>
                                    <div class="card border-primary mb-4">
                                        <div class="card-body">
                                            <h5 class="card-title">Album "<?php echo $term_name ?>"</h5>
                                            <?php $gallery_items = get_field('gallery'); ?>

                                            <?php if ($gallery_items): ?>
                                                <?php $count = count($gallery_items); ?>
                                                <?php if ($count < 10): ?>
                                                    <p class="card-text">Bạn đã đăng <?php echo $count; ?> ảnh vào album "<?php echo $term_name ?>". <?php echo ($can_upload) ? "Bạn có thể thêm được " . (10 - $count) . " ảnh nữa." : '' ?></p>
                                                <?php else: ?>
                                                    <p class="card-text">Bạn đã đăng <?php echo $count; ?> ảnh vào album "<?php echo $term_name ?>". <?php echo ($can_upload) ? "Bạn đã đạt giới hạn tải lên ảnh vào album này.": ''?></p>
                                                <?php endif; ?>
                                                <div class="cbp mb-3" id="grid-container">
                                                    <?php foreach ($gallery_items as $img): ?>
                                                        <div class="cbp-item cbp-item-album" id="attach-<?php echo $img['id']; ?>">
                                                            <?php if ($can_upload): ?>
                                                                <button class="delete-img" @click="deleteImage(<?php echo $img['id'] ?>, '<?php echo $term_slug ?>')">&#10006;</button>
                                                            <?php endif; ?>
                                                            <div class="cbp-caption">
                                                                <div class="cbp-caption-defaultWrap">
                                                                    <a class="cbp-lightbox" href="<?php echo $img['url'] ?>" data-title="<?php echo $img['description']; ?>">
                                                                        <img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>" width="100%">
                                                                    </a>
                                                                </div>
                                                                <div class="cbp-caption-activeWrap">
                                                                    <h3><?php echo str_replace("Halloween 2019 -", "", $img['title']); ?></h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            <?php else: ?>
                                                <div class="alert alert-info">
                                                    Bạn chưa có ảnh nào trong album "<?php echo $term_name ?>".
                                                </div>
                                            <?php endif; ?>
                                            <?php if ($can_upload): ?>
                                                <button class="btn btn-primary mb-3" data-toggle="collapse" @click="updateTempForm(<?php echo $key; ?>)" data-target="#collapse-img-form-<?php echo $key; ?>" aria-expanded="false" aria-controls="collapse-img-form">Thêm ảnh vào album này</button>

                                                <?php if ($end_time_str != ""): ?>
                                                    <div class="alert alert-warning mb-3">
                                                        Thời gian kết thúc tải ảnh lên: <?php echo date("d/m/Y", $end_time); ?> lúc <?php echo date("H:i", $end_time) ?> (<?php echo human_time_diff( current_time( 'timestamp' ,7), $end_time ) ?> nữa).
                                                    </div>
                                                <?php endif; ?>

                                                <?php if ($upload_message != ""): ?>
                                                    <div class="alert alert-warning mb-3">
                                                        <?php echo $upload_message; ?>
                                                    </div>
                                                <?php endif; ?>

                                                <div class="collapse" id="collapse-img-form-<?php echo $key; ?>">
                                                    <form @submit.prevent="submit(<?php echo $key; ?>)" data-vv-scope="<?php echo $key; ?>" method="post" enctype="multipart/form-data">
                                                        <input type="hidden" id="event_slug_<?php echo $key; ?>" value="<?php echo $term->slug ?>">
                                                        <div class="form-group">
                                                            <label for="file">Hình ảnh:</label>
                                                            <input class="slim"
                                                                   type="file"
                                                                   data-label="Kéo thả hình vào đây hoặc click để chọn hình"
                                                                   data-fetcher="<?php echo get_template_directory_uri() . '/functions/slim/fetch.php' ?>"
                                                                   data-size="640,640"
                                                                   data-ratio="free"
                                                                   data-button-confirm-label="Xác nhận"
                                                                   data-button-cancel-label="Hủy"
                                                                   data-button-edit-title="Sửa"
                                                                   data-button-remove-title="Xóa"
                                                                   data-button-rotate-title="Xoay"
                                                                   data-label-loading="'Đang xử lý...'"
                                                                   data-max-file-size="8"
                                                                   data-status-file-size="Dung lượng hình quá lớn. Vui lòng chọn hình nhỏ hơn 8MB"
                                                                   data-status-file-type="Định dạng hình ảnh phải là ($0)"
                                                                   data-post="input"
                                                                   data-did-load="slimCheck"
                                                                   data-vv-name="image"
                                                                   data-vv-as="Hình ảnh"
                                                            />
                                                            <div class="invalid-feedback d-block" v-if="errors.has('images[<?php echo $key; ?>]', <?php echo $key; ?>)">
                                                                {{ errors.first('images[<?php echo $key; ?>]', <?php echo $key; ?>) }}
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="imgTitle-<?php echo $key; ?>">Tiêu đề ảnh:</label>
                                                            <input class="form-control" id="imgTitle-<?php echo $key; ?>" data-vv-name="imgTitles[<?php echo $key; ?>]" data-vv-as="Tiêu đề ảnh" v-validate="'required'" v-model="imgTitles[<?php echo $key; ?>]" :class="{'is-invalid': errors.has('imgTitles[<?php echo $key; ?>]', <?php echo $key; ?>)}" placeholder="Tiêu đề ảnh"/>
                                                            <div class="invalid-feedback" v-if="errors.has('imgTitles[<?php echo $key; ?>]', <?php echo $key; ?>)">
                                                                {{ errors.first('imgTitles[<?php echo $key; ?>]', <?php echo $key; ?>) }}
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="message-<?php echo $key; ?>">Nội dung giới thiệu cho ảnh:</label>
                                                            <textarea class="form-control" id="message-<?php echo $key; ?>" data-vv-name="imgDescription[<?php echo $key; ?>]" data-vv-as="Nội dung giới thiệu" v-validate="'required'" v-model="imgDescriptions[<?php echo $key; ?>]" :class="{'is-invalid': errors.has('imgDescriptions[<?php echo $key; ?>]', <?php echo $key; ?>)}" placeholder="Nội dung giới thiệu cho ảnh" rows="3"></textarea>
                                                            <div class="invalid-feedback" v-if="errors.has('imgDescriptions[<?php echo $key; ?>]', <?php echo $key; ?>)">
                                                                {{ errors.first('imgDescriptions[<?php echo $key; ?>, <?php echo $key; ?>)]') }}
                                                            </div>
                                                        </div>
                                                        <div class="text-right">
                                                            <button class="btn btn-primary" type="submit">Gửi</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            <?php else: ?>
                                <div class="card border-primary mb-4">
                                    <img class="card-img-top" src="<?php echo $term_img['url']; ?>" alt="<?php $term_img['url'] ?>">
                                    <div class="card-body">
                                        <h5 class="card-title">Album "<?php echo $term_name ?>"</h5>
                                        <p class="card-text"><?php echo $description; ?></p>
                                        <?php if ($allow_upload): ?>
                                            <?php if ($can_upload): ?>
                                                <button class="btn btn-primary mb-3" data-toggle="collapse" data-target="#collapse-img-form-<?php echo $key; ?>" aria-expanded="false" aria-controls="collapse-img-form">Tải ảnh lên</button>

                                                <?php if ($end_time_str != ""): ?>
                                                <div class="alert alert-warning mb-3">
                                                    Thời gian kết thúc tải ảnh lên: <?php echo date("d/m/Y", $end_time); ?> lúc <?php echo date("H:i", $end_time) ?> (<?php echo human_time_diff( current_time( 'timestamp' ,7), $end_time ) ?> nữa).
                                                </div>
                                                <?php endif; ?>

                                                <div class="collapse" id="collapse-img-form-<?php echo $key; ?>">
                                                    <form @submit.prevent="submit(<?php echo $key; ?>)" data-vv-scope="<?php echo $key; ?>" method="post" enctype="multipart/form-data">
                                                        <input type="hidden" id="event_slug_<?php echo $key; ?>" value="<?php echo $term->slug ?>">
                                                        <div class="form-group">
                                                            <label for="file">Hình ảnh:</label>
                                                            <input class="slim"
                                                                   type="file"
                                                                   data-label="Kéo thả hình vào đây hoặc click để chọn hình"
                                                                   data-fetcher="<?php echo get_template_directory_uri() . '/functions/slim/fetch.php' ?>"
                                                                   data-size="640,640"
                                                                   data-ratio="free"
                                                                   data-button-confirm-label="Xác nhận"
                                                                   data-button-cancel-label="Hủy"
                                                                   data-button-edit-title="Sửa"
                                                                   data-button-remove-title="Xóa"
                                                                   data-button-rotate-title="Xoay"
                                                                   data-label-loading="'Đang xử lý...'"
                                                                   data-max-file-size="8"
                                                                   data-status-file-size="Dung lượng hình quá lớn. Vui lòng chọn hình nhỏ hơn 8MB"
                                                                   data-status-file-type="Định dạng hình ảnh phải là ($0)"
                                                                   data-post="input"
                                                                   data-did-load="slimCheck"
                                                                   data-vv-name="image"
                                                                   data-vv-as="Hình ảnh"
                                                            />
                                                            <div class="invalid-feedback d-block" v-if="errors.has('images[<?php echo $key; ?>]', <?php echo $key; ?>)">
                                                                {{ errors.first('images[<?php echo $key; ?>]', <?php echo $key; ?>) }}
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="imgCharName-<?php echo $key; ?>">Chọn nhân vật chính (dùng để cho các bạn khác biết tên nhân vật của bạn):</label>
                                                            <select class="form-control" id="imgCharName-<?php echo $key; ?>" v-model="imgCharNames[<?php echo $key; ?>]" v-validate="'required'" :class="{'is-invalid': errors.has('imgCharNames[<?php echo $key; ?>]', <?php echo $key; ?>)}" data-vv-name="imgCharNames[<?php echo $key; ?>]" data-vv-as="Chọn nhân vật chính">
                                                                <option hidden disabled>-- Chọn nhân vật --</option>
                                                                <?php if ($row_chars): ?>
                                                                    <?php foreach($row_chars as $char): ?>
                                                                        <option value="<?php echo $char->name; ?>"><?php echo $char->name; ?></option>
                                                                    <?php endforeach; ?>
                                                                <?php endif; ?>
                                                            </select>
                                                            <div class="invalid-feedback" v-if="errors.has('imgCharNames[<?php echo $key; ?>]', <?php echo $key; ?>)">
                                                                {{ errors.first('imgCharNames[<?php echo $key; ?>]', <?php echo $key; ?>) }}
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="imgTitle-<?php echo $key; ?>">Tiêu đề ảnh:</label>
                                                            <input class="form-control" id="imgTitle-<?php echo $key; ?>" data-vv-name="imgTitles[<?php echo $key; ?>]" data-vv-as="Tiêu đề ảnh" v-validate="'required'" v-model="imgTitles[<?php echo $key; ?>]" :class="{'is-invalid': errors.has('imgTitles[<?php echo $key; ?>]', <?php echo $key; ?>)}" placeholder="Tiêu đề ảnh"/>
                                                            <div class="invalid-feedback" v-if="errors.has('imgTitles[<?php echo $key; ?>]', <?php echo $key; ?>)">
                                                                {{ errors.first('imgTitles[<?php echo $key; ?>]', <?php echo $key; ?>) }}
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="message-<?php echo $key; ?>">Nội dung giới thiệu cho ảnh:</label>
                                                            <textarea class="form-control" id="imgDescriptions-<?php echo $key; ?>" data-vv-name="imgDescriptions[<?php echo $key; ?>]" data-vv-as="Nội dung giới thiệu" v-validate="'required'" v-model="imgDescriptions[<?php echo $key; ?>]" :class="{'is-invalid': errors.has('imgDescriptions[<?php echo $key; ?>]', <?php echo $key; ?>)}" placeholder="Nội dung giới thiệu cho ảnh" rows="3"></textarea>
                                                            <div class="invalid-feedback" v-if="errors.has('imgDescriptions[<?php echo $key; ?>]', <?php echo $key; ?>)">
                                                                {{ errors.first('imgDescriptions[<?php echo $key; ?>]', <?php echo $key; ?>) }}
                                                            </div>
                                                        </div>

                                                        <div class="text-right">
                                                            <button class="btn btn-primary" type="submit">Gửi</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            <?php endif; ?>

                                            <?php if ($upload_message != ""): ?>
                                            <div class="alert alert-warning mb-3">
                                                <?php echo $upload_message; ?>
                                            </div>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <a class="btn btn-secondary" href="<?php echo home_url('/album/' . $term_slug) ?>">Xem ảnh</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endwhile; ?>
<?php get_footer(); ?>
