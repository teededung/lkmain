<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

	<div class="container content">
        <h1 class="text-center mb-3">Download</h1>

		<?php $links = get_field('link_download', 'option'); ?>

        <?php if ($links): ?>
            <div class="help-text mb-3">
                Chọn 1 trong <?php echo count($links) ?> đường link dưới đây để tải game:
            </div>

            <div class="list-group mb-5">
                <?php if ($links): ?>
                    <?php foreach ($links as $link): ?>
                        <a target="_blank" href="<?php echo $link['link']; ?>" class="list-group-item list-group-item-action">
                            <?php echo $link['name']; ?>
                        </a>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>

            <h3>Sau khi bạn tải xong hãy làm theo những bước sau đây:</h3>

            <ul class="list-group mb-3">
                <li class="list-group-item">
                    1. Giải nén vào 1 ổ bất kì. Ví dụ D:\Games\
                </li>
                <li class="list-group-item">
                    2. Chạy file <strong>opensetup.exe</strong> để cài đặt cấu hình như âm thành và độ phân giải. Cài dặt xong bạn click vào Apply và OK.
                </li>
                <li class="list-group-item">
                    3. Chạy <strong>Patcher.exe</strong> để cập nhật. Sau khi cập nhật xong hãy click vào "CHƠI GAME" để vào game.
                </li>
            </ul>

            <div class="alert alert-warning">
                Nếu bạn bị lỗi cài đặt game thì bạn hãy PM fanpage để được hỗ trợ.
            </div>

        <?php else: ?>
            <div class="alert alert-info">
                Link download đang được cập nhật...
            </div>
        <?php endif; ?>
	</div>

<?php endwhile; ?>
<?php get_footer(); ?>
