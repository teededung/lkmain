<?php

/**
 * CHECK SYSTEM IS LOCAL OR NOT
 * @return bool
 */
function is_local(){
	if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1'):
		return true;
	elseif ($_SERVER['REMOTE_ADDR'] == '::1'):
		return true;
	endif;

	return false;
}

/**
 * Enqueue css, js in front-end
 */
function front_end_enqueue() {
	$version = 120;

	if (is_local()):
		$version = rand ( 0, 999 );
	endif;
	
	$root_css 			= get_template_directory_uri() . '/css/';
	$root_js 			= get_template_directory_uri() . '/js/';
	$root_lib_js 		= get_template_directory_uri() . '/js/libs/';

	if (is_local()):
		wp_enqueue_script( 'vue', $root_lib_js . 'vue.js', array(), '2.3.4', true );
	else:
		wp_enqueue_script( 'vue', $root_lib_js . 'vue.min.js', array(), '2.3.4', true );
	endif;

	wp_enqueue_script( 'js.cookie', $root_lib_js . 'js.cookie.min.js', array(), '2.2.0', true );

	// Main JS
	wp_register_script('main', $root_js . 'main.js', array('jquery', 'vue', 'js.cookie'), $version, true);
	wp_enqueue_script('main');
	wp_localize_script('main', 'TEE', array(
		'ajaxurl' 	=> admin_url( 'admin-ajax.php' ),
		'n'			=> wp_create_nonce( "n" )
	));

	wp_enqueue_style( 'ionicons', $root_css . 'ionicons.min.css', array( ), '2.0.1');
	wp_enqueue_script( 'popper', $root_lib_js . 'popper.min.js', array( 'jquery' ), '1.12.3', true );

	wp_enqueue_style( 'bootstrap', $root_css . 'bootstrap.min.css', array( ), '4.3.1');
	wp_enqueue_script( 'bootstrap', $root_lib_js . 'bootstrap.min.js', array( 'jquery' ), '4.3.1', true );

	wp_enqueue_style( 'toastr', $root_css . 'toastr.min.css', array( ), '1.0');
	wp_enqueue_script( 'toastr', $root_lib_js . 'toastr.min.js', array( 'jquery' ), '1.0', true );

	wp_enqueue_script( 'loadingOverlay', $root_lib_js . 'loadingOverlay.min.js', array(), '1.5.3', true );

	wp_enqueue_style( 'sweetalert2', $root_css . 'sweetalert2.min.css', array(), '8.11.8');
	wp_enqueue_script( 'sweetalert2', $root_lib_js . 'sweetalert2.min.js', array('vue'), '8.11.8', true );

	wp_enqueue_style( 'base', $root_css . 'base.css', array(), $version);

    wp_deregister_script( 'lodash' );
	wp_enqueue_script( 'lodash', $root_lib_js . 'lodash.min.js', array( ), '4.17.4', true );

	if (is_home() || is_front_page()):
		wp_enqueue_script( 'TweenMax', $root_lib_js . 'TweenMax.min.js', array(), '1.20.2', true );

		wp_enqueue_style( 'front-page', $root_css . 'front-page.css', array(), $version);
		wp_enqueue_script( 'front-page', $root_js . 'front-page.js', false, $version, true );

	elseif (is_page('gioi-thieu')):
//		wp_enqueue_style( 'dzsparallaxer', $root_css . 'dzsparallaxer.min.css', array(), '1.0');
//		wp_enqueue_script( 'dzsparallaxer', $root_lib_js . 'dzsparallaxer.min.js', false, '1.0', true );

        wp_enqueue_style( 'cubeportfolio', $root_css . 'cubeportfolio.min.css', array(), '4.4.0');
        wp_enqueue_script( 'cubeportfolio', $root_lib_js . 'jquery.cubeportfolio.min.js', false, '4.4.0', true );

		wp_enqueue_script( 'scrollMagic', $root_lib_js . 'ScrollMagic.min.js', array(), '2.0.5', true );

		if (is_local()) {
			wp_enqueue_script( 'addIndicators', $root_lib_js . 'debug.addIndicators.min.js', array(), '2.0.5', true );
		}

		wp_enqueue_script( 'animation.gsap', $root_lib_js . 'animation.gsap.min.js', array('scrollMagic'), '2.0.5', true );
		wp_enqueue_script( 'TweenMax', $root_lib_js . 'TweenMax.min.js', array('scrollMagic'), '1.20.2', true );

		wp_enqueue_style( 'owl.carousel', $root_css . 'owl.carousel.min.css', array(), '2.2.1');
		wp_enqueue_script( 'owl.carousel', $root_lib_js . 'owl.carousel.min.js', false, '2.2.1', true );

		wp_enqueue_style( 'page-gioi-thieu', $root_css . 'page-gioi-thieu.css', array(), $version);
		wp_enqueue_script( 'page-gioi-thieu', $root_js . 'page-gioi-thieu.js', array('TweenMax', 'scrollMagic'), $version, true );

    elseif (is_page('tri-an')):
        wp_enqueue_script( 'vee-validate', $root_lib_js . 'vee-validate.min.js', array('vue'), '2.2.13', true );
        wp_enqueue_script( 'vee-vi', $root_lib_js . 'vee-vi.js', array('vee-validate'), '2.2.13', true );

        wp_enqueue_script( 'page-tri-an', $root_js . 'page-tri-an.js', array('vee-validate'), $version, true );

	elseif (is_page('dang-ky')):
		wp_enqueue_script( 'recaptcha', 'https://www.google.com/recaptcha/api.js?hl=vi', false, $version, true );

		wp_enqueue_script( 'vee-validate', $root_lib_js . 'vee-validate.min.js', array('vue'), '2.2.13', true );
		wp_enqueue_script( 'vee-vi', $root_lib_js . 'vee-vi.js', array('vee-validate'), '2.2.13', true );
		wp_enqueue_style( 'page-dang-ky', $root_css . 'page-dang-ky.css', array(), $version);

		wp_enqueue_script( 'page-dang-ky', $root_js . 'page-dang-ky.js', false, $version, true );

	elseif (is_page('dang-nhap')):
		wp_enqueue_script( 'vee-validate', $root_lib_js . 'vee-validate.min.js', array('vue'), '2.0.4', true );
        wp_enqueue_script( 'vee-vi', $root_lib_js . 'vee-vi.js', array('vee-validate'), '2.2.13', true );

		wp_enqueue_style( 'page-dang-nhap', $root_css . 'page-dang-nhap.css', array(), $version);
		wp_enqueue_script( 'page-dang-nhap', $root_js . 'page-dang-nhap.js', false, $version, true );

	elseif (is_page('tai-khoan')):
        wp_enqueue_style( 'cubeportfolio', $root_css . 'cubeportfolio.min.css', array(), '4.4.0');
        wp_enqueue_script( 'cubeportfolio', $root_lib_js . 'jquery.cubeportfolio.min.js', false, '4.4.0', true );

        wp_enqueue_script( 'vee-validate', $root_lib_js . 'vee-validate.min.js', array('vue'), '2.0.4', true );
        wp_enqueue_script( 'vee-vi', $root_lib_js . 'vee-vi.js', array('vee-validate'), '2.2.13', true );

		wp_enqueue_style( 'flatpickr', 'https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css', array(), $version);
		wp_enqueue_script( 'flatpickr', 'https://cdn.jsdelivr.net/npm/flatpickr', false, $version, true );

        wp_enqueue_style( 'slim', $root_css . 'slim.min.css', array(), '5.2.1');
        wp_enqueue_script( 'slim', $root_lib_js . 'slim.kickstart.min.js', false, '5.2.1', true );

		wp_enqueue_style( 'page-tai-khoan', $root_css . 'page-tai-khoan.css', array(), $version);
		wp_enqueue_script( 'page-tai-khoan', $root_js . 'page-tai-khoan.js', false, $version, true );

	elseif (is_page('xep-hang')):

		wp_enqueue_style( 'page-xep-hang', $root_css . 'page-xep-hang.css', array(), $version);
		wp_enqueue_script( 'page-xep-hang', $root_js . 'page-xep-hang.js', false, $version, true );

	elseif (is_page('dev-notes')):
		wp_enqueue_style( 'timeline-default', $root_css . 'timeline-default.min.css', array(), $version);
		wp_enqueue_style( 'frst-timeline-style', $root_css . 'frst-timeline-style-18.css', array(), $version);
		wp_enqueue_script( 'frst-scale-effect', $root_lib_js . 'frst-scale-effect.min.js', array(), '1', true );
		wp_enqueue_script( 'frst-timeline', $root_lib_js . 'frst-timeline.js', array(), '1', true );

		wp_enqueue_style( 'page-dev-notes', $root_css . 'page-dev-notes.css', array(), $version);
		wp_enqueue_script( 'page-dev-notes', $root_js . 'page-dev-notes.js', false, $version, true );

    elseif (is_page('cac-lenh-huu-ich-tai-laniakea-ro')):

        wp_enqueue_style( 'cac-lenh-huu-ich-tai-laniakea-ro', $root_css . 'page-cac-lenh-huu-ich-tai-laniakea-ro.css', array(), $version);

	elseif (is_page('tin-tuc') || is_page('tim-kiem') || is_category() || is_tag()):

		wp_enqueue_style( 'page-tin-tuc', $root_css . 'page-tin-tuc.css', array(), $version);
		wp_enqueue_script( 'page-tin-tuc', $root_js . 'page-tin-tuc.js', false, $version, true );

	elseif (is_page('quen-mat-khau')):

		wp_enqueue_style( 'page-quen-mat-khau', $root_css . 'page-quen-mat-khau.css', array(), $version);
		wp_enqueue_script( 'page-quen-mat-khau', $root_js . 'page-quen-mat-khau.js', false, $version, true );

	elseif (is_page('ho-tro')):

		wp_enqueue_style( 'page-ho-tro', $root_css . 'page-ho-tro.css', array(), $version);
		wp_enqueue_script( 'page-ho-tro', $root_js . 'page-ho-tro.js', false, $version, true );

	elseif (is_page('quan-tri-ragnarok')):
		wp_enqueue_style( 'sweetalert2', $root_css . 'sweetalert2.min.css', array(), '8.11.8');
		wp_enqueue_script( 'sweetalert2', $root_lib_js . 'sweetalert2.min.js', array('vue'), '8.11.8', true );

		wp_enqueue_style( 'flatpickr', 'https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css', array(), $version);
		wp_enqueue_script( 'flatpickr', 'https://cdn.jsdelivr.net/npm/flatpickr', false, $version, true );

		wp_enqueue_script( 'clipboard', $root_lib_js . 'clipboard.min.js', array(), '1.7.1', true );

		wp_enqueue_script( 'loadingOverlay', $root_lib_js . 'loadingOverlay.min.js', array(), '1.5.3', true );

		wp_enqueue_style( 'page-quan-tri-ragnarok', $root_css . 'page-quan-tri-ragnarok.css', array(), $version);
		wp_enqueue_script( 'page-quan-tri-ragnarok', $root_js . 'page-quan-tri-ragnarok.js', false, $version, true );

	elseif (is_page('dong-gop')):

		wp_enqueue_style( 'page-dong-gop', $root_css . 'page-dong-gop.css', array(), $version);
		wp_enqueue_script( 'page-dong-gop', $root_js . 'page-dong-gop.js', false, $version, true );

	elseif (is_page('album') || is_singular('albums') || is_tax('album_category')):

		wp_enqueue_style( 'cubeportfolio', $root_css . 'cubeportfolio.min.css', array(), '4.4.0');
		wp_enqueue_script( 'cubeportfolio', $root_lib_js . 'jquery.cubeportfolio.min.js', false, '4.4.0', true );

		wp_enqueue_style( 'page-album', $root_css . 'page-album.css', array(), $version);
		wp_enqueue_script( 'page-album', $root_js . 'page-album.js', false, $version, true );
	endif;

	if (is_singular('post')):
		wp_enqueue_style( 'single', $root_css . 'single.css', array(), $version);
	endif;

	wp_enqueue_script('main-footer', $root_js . 'main-footer.js', array('jquery'), $version, true);
}
add_action( 'wp_enqueue_scripts', 'front_end_enqueue' );


/**
 * Enqueue css, js in admin
 */
function admin_enqueue() {
	$root_css 			= get_template_directory_uri() . '/css/';
	// $root_js 			= get_template_directory_uri() . '/js/';
	// $root_lib_js 		= get_template_directory_uri() . '/js/libs/';
	
	wp_enqueue_style( 'starter-admin-css', $root_css . 'admin.css');
}

add_action( 'admin_enqueue_scripts', 'admin_enqueue' );