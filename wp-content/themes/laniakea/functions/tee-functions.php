<?php

/**
 * Excerpt limit
 * @param  int 		$limit 	[Number characters]
 * @return string        	[short description]
 */
if (!function_exists('excerpt')):
	function excerpt($limit) {
	     $excerpt = explode(' ', get_the_excerpt(), $limit);
	     if (count($excerpt)>=$limit) {
	          array_pop($excerpt);
	          $excerpt = implode(" ",$excerpt).'...';
	     } else {
	          $excerpt = implode(" ",$excerpt);
	     }
	     $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	     return $excerpt;
	}
endif;


/**
 * Get related posts
 * @param  array 	$categories [Array categories of post]
 * @param  int 		$number     [Number post you want to get]
 * @return object           	[object posts]

 */
if (!function_exists('get_related_posts')):
	function get_related_posts($categories, $number = 10) {
	    $category_ids = array();
	    foreach($categories as $cat)
	        $category_ids[] = $cat->term_id;

		global $post;

		$args = array(
	        'category__in'          => $category_ids,
	        'post__not_in'          => array($post->ID),
	        'posts_per_page'     	=> $number,
	        'ignore_sticky_posts '  => 1
	    );

	    $posts = new WP_query($args);

	    return $posts;
	}
endif;