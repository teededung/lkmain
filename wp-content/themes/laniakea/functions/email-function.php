<?php

function send_activation_email($post_id) {

	$username = get_the_title($post_id);

	$activation_code = get_field('activation_code', $post_id);
	$link_active = home_url('/kich-hoat-tai-khoan/?code=') . $activation_code;

	$to = get_field('email', $post_id);
	$subject = '[Laniakea RO] Kích hoạt tài khoản của bạn';
	$headers = array('Content-Type: text/html; charset=UTF-8');

	$body = 'Chào ' . $username . ',<br><br>';
	$body .= 'Cảm ơn bạn đã đăng kí tài khoản tại Laniakea RO. Dưới đây là đường link để bạn kích hoạt tài khoản: <br>';
	$body .= '<a href="'. $link_active .'">' . $link_active . '</a><br><br>';

	$body .= 'Sau khi bạn kích hoạt tài khoản thành công, bạn có thể đăng nhập được game. Chúc bạn chơi vui vẻ.<br><br><br>';
	$body .= 'Laniakea GM.';

	return wp_mail( $to, $subject, $body, $headers );
}

function resend_activation_email($post_id) {
	$array = array();
	$success = send_activation_email($post_id);

	if ($success):
		$array['status'] = 'OK';
		$array['message'] = 'Gửi email thành công với $post_id là ' . $post_id;
	else:
		$array['failed'] = 'OK';
		$array['message'] = 'Gửi email thất bại với $post_id là ' . $post_id;
	endif;

	return $array;
}