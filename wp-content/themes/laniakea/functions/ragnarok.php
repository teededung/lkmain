<?php
/**
 * @return array
 * More rights
 */
function connect_ragnarok_db2() {
    $array = array();

	$db_user = get_field('db_user', 'option');
	$db_password = get_field('db_password', 'option');
	$db_name = get_field('db_name', 'option');
	$db_host = get_field('db_host', 'option');


    if ($db_user && $db_password && $db_name && $db_host):
        $db = new wpdb($db_user, $db_password, $db_name, $db_host);

        if ($db->db_connect()):
            $array['status'] = 'OK';
            $array['db'] = $db;
        else:
            $array['status'] = 'Failed';
            $array['message'] = 'Lỗi kết nối 1. Không thể kết nối với hệ thống! Vui lòng liên hệ GM.';
        endif;
    else:
        $array['status'] = 'Failed';
        $array['message'] = 'Lỗi kết nối 2. Không thể kết nối với hệ thống! Vui lòng liên hệ GM.';
    endif;

    return $array;
}

/**
 * @return array
 * Low rights
 */
function connect_ragnarok_db() {
    $array = array();

    $db_user = "rokc";
    $db_password = "DQ(*tCrP(9T/";
    $db_name = "ragnarok";
    $db_host = "localhost";

    if ($db_user && $db_password && $db_name && $db_host):
        $db = new wpdb($db_user, $db_password, $db_name, $db_host);

        if ($db->db_connect()):
            $array['status'] = 'OK';
            $array['db'] = $db;
        else:
            $array['status'] = 'Failed';
            $array['message'] = 'Lỗi kết nối 1. Không thể kết nối với hệ thống! Vui lòng liên hệ GM.';
        endif;
    else:
        $array['status'] = 'Failed';
        $array['message'] = 'Lỗi kết nối 2. Không thể kết nối với hệ thống! Vui lòng liên hệ GM.';
    endif;

    return $array;
}


/**
 * @return array
 */
function ragnarok_get_online_chars() {
	$array = array();
	$ragnarok_db = connect_ragnarok_db();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $rows = $ragnarok_db['db']->get_results(
        $ragnarok_db['db']->prepare("SELECT char_id, name FROM `char` WHERE online = %d", 1)
    );

    if ($rows):
        $array['status'] = 'OK';
        $array['count'] = count($rows);
        $array['online_chars'] = $rows;
    else:
        $array['status']    = 'Failed';
        $array['count']     = '...';
    endif;

	return $array;
}

/**
 * @return array
 */
function ragnarok_get_number_online_chars() {
    $array = array();
    $ragnarok_db = connect_ragnarok_db();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $rows = $ragnarok_db['db']->get_results(
        $ragnarok_db['db']->prepare("SELECT count(`char_id`) as 'count' FROM `char` WHERE online = %d", 1)
    );

    if ($rows):
        $array['status'] = 'OK';
        $array['count'] = $rows[0]->count;
    else:
        $array['status']    = 'Failed';
        $array['count']     = '...';
    endif;

    return $array;
}

/**
 * @param $username
 * @param $email
 *
 * @return array
 */
function ragnarok_check_exist_account($username, $email) {
	$array = array();
	$ragnarok_db = connect_ragnarok_db();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $row_user = $ragnarok_db['db']->get_row(
        $ragnarok_db['db']->prepare( 'SELECT `userid` FROM `login` WHERE `userid` = %s', $username )
    );

    if (count($row_user)):
        $array['status']    = 'Failed';
        $array['message']   = 'Tên đăng nhập này đã có người sử dụng. Hãy sử dụng tên đăng nhập khác.';
        return $array;
    endif;

    $row_email = $ragnarok_db['db']->get_row(
        $ragnarok_db['db']->prepare( 'SELECT `email` FROM `login` WHERE `email` = %s', $email )
    );

    if (count($row_email)):
        $array['status'] = 'Failed';
        $array['message'] = 'Email này đã có người sử dụng. Hãy sử dụng Email khác.';
        return $array;
    endif;

	$array['status'] = 'OK';
	return $array;
}

/**
 * @param $username
 * @param $password
 * @param $email
 *
 * @return array
 */
function ragnarok_register_account($username, $password, $email) {
	$array = array();
	$check_exist = ragnarok_check_exist_account($username, $email);

	if (!is_data_return_ok($check_exist)):
        return $check_exist;
    endif;

    // Check database connection
    $ragnarok_db = connect_ragnarok_db();
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $user = array(
        'userid' => $username,
        'user_pass' => $password,
        'email' => $email,
        'state' => 0,
        'group_id' => 1
    );

    $result = $ragnarok_db['db']->insert('login', $user);

    if ($result):
        $row_account_id = $ragnarok_db['db']->get_row(
            $ragnarok_db['db']->prepare('SELECT `account_id` FROM `login` WHERE `email` = %s AND userid = %s', $email, $username)
        );
        $array['status'] = 'OK';
        $array['account_id'] = $row_account_id->account_id;
    else:
        $array['status'] = 'Failed';
        $array['message'] = 'Không thể đăng ký tài khoản. Vui lòng thử lại sau hoặc liên hệ GM.';
    endif;

	return $array;
}

/**
 * @param $username
 * @param $email
 *
 * @return array
 */
function ragnarok_active_account($username, $email) {
	$array = array();
	$ragnarok_db = connect_ragnarok_db2();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $data = array(
        'state' => '0'
    );

    $where = array(
        'userid' => $username,
        'email' => $email
    );

    $result = $ragnarok_db['db']->update('login', $data, $where);

    if ($result):
        $array['status'] = 'OK';
    else:
        $array['status'] = 'Failed';
        $array['message'] = 'Không thể thay đổi trạng thái kích hoạt tải khoản.';
    endif;

	return $array;
}

/**
 * @param $username
 * @param $password
 *
 * @return array has auth string
 */
function ragnarok_login($username, $password) {
	$array = array();
	$ragnarok_db = connect_ragnarok_db();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $row_user = $ragnarok_db['db']->get_row(
        $ragnarok_db['db']->prepare("SELECT `account_id` FROM `login` WHERE BINARY `userid` = %s AND `user_pass` = %s LIMIT 1", $username, $password)
    );

    if ($row_user):
        $encryped_username = encrypt_decrypt('encrypt', $username);
        $encryped_password = encrypt_decrypt('encrypt', $password);
        $encryped_account_id = encrypt_decrypt('encrypt', $row_user->account_id);

        $array['status'] = 'OK';
        $array['laniakea'] = $encryped_account_id . ".." .$encryped_username . '..' . $encryped_password;
    else:
        $array['status'] = 'Failed';
        $array['message'] = 'Sai tên đăng nhập hoặc mật khẩu.';
    endif;

	return $array;
}

/**
 * @param $auth
 * @return array has account info
 */
function ragnarok_login_by_auth($auth) {
    $array = array();
    // Parse auth
    $account = explode_auth_to_information($auth);

    if (is_data_return_ok($account)):
        // Login test
        $login = ragnarok_login($account['username'], $account['password']);

        if (is_data_return_ok($login)):

            // double check
            if ($login['laniakea'] != $auth):
                $array['status'] = 'Failed';
                $array['message'] = 'Eyya!';
                return $array;
            endif;

            $array['status'] = 'OK';
            $array['username'] = $account['username'];
            $array['password'] = $account['password'];
            $array['account_id'] = $account['account_id'];
        else:
            $array['status'] = 'Failed';
            $array['message'] = 'Sai tên đăng nhập hoặc mật khẩu.';
        endif;
    else:
        $array['status'] = 'Failed';
        $array['message'] = $account['error'];
    endif;

    return $array;
}

/**
 * @param $account_id
 *
 * @return int
 */
function get_account_cp($account_id) {
	$ragnarok_db = connect_ragnarok_db();
    $row = $ragnarok_db['db']->get_row(
        $ragnarok_db['db']->prepare("SELECT COALESCE(SUM(acc_reg_num.value), 0) AS cp FROM login LEFT JOIN acc_reg_num ON acc_reg_num.account_id = login.account_id WHERE login.account_id = %d AND acc_reg_num.key = %s", $account_id, "#CASHPOINTS")
    );
	return floatval($row->cp);
}

/**
 * GET CP By Account ID
 * @param $account_id
 * @return float
 */
function get_account_cp_by_account_id($account_id) {
	$ragnarok_db = connect_ragnarok_db();
    $row = $ragnarok_db['db']->get_row(
        $ragnarok_db['db']->prepare("SELECT COALESCE(SUM(acc_reg_num.value), 0) AS cp FROM login LEFT JOIN acc_reg_num ON acc_reg_num.account_id = login.account_id WHERE acc_reg_num.account_id = %s AND acc_reg_num.key = %s", $account_id, "#CASHPOINTS")
    );
	return floatval($row->cp);
}

/**
 * GET Unique ID By Account ID
 * @param $account_id
 * @return float
 */
function get_unique_id_by_account_id($account_id) {
    $ragnarok_db = connect_ragnarok_db();
    $row = $ragnarok_db['db']->get_row(
        $ragnarok_db['db']->prepare("SELECT `last_unique_id` FROM `login` WHERE `account_id` = %s", $account_id)
    );
    return $row->last_unique_id;
}

/**
 * Get total Zeny on Account
 * @param $username
 *
 * @return mixed
 */
function get_account_zeny_by_username($username) {
	$ragnarok_db = connect_ragnarok_db();
    $row = $ragnarok_db['db']->get_row(
        $ragnarok_db['db']->prepare("SELECT `login`.account_id, SUM(`char`.zeny) as zeny FROM `login` INNER JOIN `char` ON `login`.account_id = `char`.account_id WHERE `login`.userid = %s", $username)
    );
	if ($row):
		return floatval($row->zeny);
	endif;

	return 0;
}

/**
 * Get Char Zeny
 *
 * @param $char_id
 * @return float|int
 */
function get_char_zeny($char_id) {
	$ragnarok_db = connect_ragnarok_db();
    $row = $ragnarok_db['db']->get_row(
        $ragnarok_db['db']->prepare("SELECT `char`.zeny FROM `char` WHERE `char`.char_id = %s", $char_id)
    );
	if ($row):
		return floatval($row->zeny);
	endif;

	return 0;
}

/**
 * @param string $auth
 *
 * @return array
 */
function get_account_information($auth) {
	$array = array();

    // Check database connection
    $ragnarok_db = connect_ragnarok_db();
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

	$account = explode_auth_to_information($auth);

	if (is_data_return_ok($account)):
        $account_id = $account['account_id'];
		$username = $account['username'];
		$password = $account['password'];

        $row_account = $ragnarok_db['db']->get_row(
            $ragnarok_db['db']->prepare('SELECT login.account_id, login.userid, login.sex, login.email, login.state, login.lastlogin, login.birthdate FROM `login` WHERE `userid` = %s AND `user_pass` = %s', $username, $password)
        );

        $row_account->cp = get_account_cp($account_id);
        $row_account->zeny = get_account_zeny_by_username($username);

        $array['status'] = 'OK';
        $array['account'] = $row_account;
	else:
		$array['status'] = 'Failed';
		$array['message'] = 'Thông tin tài khoản sai!';
	endif;

	return $array;
}


/**
 * @param array $data
 *
 * @return array
 */
function ragnarok_change_password($data) {
	$array = array();

	// Check database connection
    $ragnarok_db = connect_ragnarok_db2();
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    // Login test
    $auth = $data['auth'];
	$account = ragnarok_login_by_auth($auth);

    $old_password = $data['old_password'];
    $new_password = $data['new_password'];

	// Check old password
    if ($account['password'] != $old_password):
        $array['status'] = 'Failed';
        $array['message'] = 'Mật khẩu cũ không chính xác.';
        return $array;
    endif;

    // Check new password
    if ($account['password'] == $new_password):
        $array['status'] = 'Failed';
        $array['message'] = 'Mật khẩu mới trùng mật khẩu cũ.';
        return $array;
    endif;

	// Parse auth
    // Update password
	if (is_data_return_ok($account)):
        $data = array(
            'user_pass' => $new_password
        );

        $where = array(
            'account_id' => $account['account_id']
        );

        $array['user_pass'] = $new_password;
        $array['where'] = $account['account_id'];

        $result = $ragnarok_db['db']->update('login', $data, $where);

        if ($result):
            $array['status'] = 'OK';
            $array['message'] = 'Bạn đã đổi mật khẩu thành công. Vui lòng đăng nhập lại.';
        else:
            $array['status'] = 'Failed';
            $array['message'] = 'Không thể thay đổi mật khẩu. Vui lòng thử lại sau hoặc liên hệ GM.';
        endif;
	endif;

	return $array;
}

/**
 * @param $type
 * @return array
 */
function get_ragnarok_users($type) {
    $array = array();
    $ragnarok_db = connect_ragnarok_db();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    switch ($type) {
        case 1:
            $sql = "SELECT `char`.`char_id`, `char`.`account_id`, `char`.`name`, `char`.`zeny`, `char`.`online`, `char`.`base_level`, `char`.`job_level`, `char`.`class` FROM `char` 
LEFT JOIN `vendings` ON `char`.`char_id` = `vendings`.`char_id` 
LEFT JOIN `buyingstores` ON `char`.`char_id` = `buyingstores`.`char_id` WHERE `vendings`.`char_id` IS NULL AND `buyingstores`.`char_id` IS NULL AND `char`.`online` = 1";
            break;
        case 2:
            $sql = "SELECT `char`.`char_id`, `char`.`account_id`, `char`.`name`, `char`.`zeny`, `char`.`online`, `char`.`base_level`, `char`.`job_level`, `char`.`class` FROM `char` 
RIGHT JOIN `vendings` ON `char`.`char_id` = `vendings`.`char_id` WHERE `char`.`online` = 1
UNION
SELECT `char`.`char_id`, `char`.`account_id`, `char`.`name`, `char`.`zeny`, `char`.`online`, `char`.`base_level`, `char`.`job_level`, `char`.`class` FROM `char` 
RIGHT JOIN `buyingstores` ON `char`.`char_id` = `buyingstores`.`char_id` WHERE `char`.`online` = 1";
            break;
        case 3:
            $sql = "SELECT * FROM `char` WHERE `char`.`online` = 0";
            break;
        default:
            $sql = "SELECT * FROM `char`";
    }
    $rows = $ragnarok_db['db']->get_results($sql);

    $array['status'] = 'OK';
    $array['count'] = count($rows);
    $array['chars'] = $rows;

    return $array;
}

/**
 * @return array
 */
function get_server_info() {
    $array = array();
    $ragnarok_db = connect_ragnarok_db();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $count_char = $ragnarok_db['db']->get_results("SELECT COUNT(*) as numChar FROM `char`");
    $count_acc = $ragnarok_db['db']->get_results("SELECT COUNT(*) as numAcc FROM `login`");
    $count_guild = $ragnarok_db['db']->get_results("SELECT COUNT(*) as numGuild FROM `guild`");

    $array['status'] = 'OK';
    $array['count_char'] = $count_char[0]->numChar;
    $array['count_acc'] = $count_acc[0]->numAcc;
    $array['count_guild'] = $count_guild[0]->numGuild;

    return $array;
}

/**
 * @return array
 */
function get_all_chars() {
	$array = array();
	$ragnarok_db = connect_ragnarok_db();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $rows = $ragnarok_db['db']->get_results("SELECT * FROM `char`");
    $array['status'] = 'OK';
    $array['count'] = count($rows);
    $array['chars'] = $rows;

	return $array;
}

/**
 * @return array
 */
function get_server_status() {
    $host   = gethostname();
    $ip     = gethostbyname($host);
	$array  = array();

	// Login Server
	$connection = @fsockopen($ip, 6900);
	if (is_resource($connection)):
		$array['login'] = 'OK';
		fclose($connection);
	else:
		$array['login'] = 'Failed';
	endif;

	// Char Server
	$connection = @fsockopen($ip, 6121);
	if (is_resource($connection)):
		$array['char'] = 'OK';
		fclose($connection);
	else:
		$array['char'] = 'Failed';
	endif;

	// Map Server
	$connection = @fsockopen($ip, 5121);
	if (is_resource($connection)):
		$array['map'] = 'OK';
		fclose($connection);
	else:
		$array['map'] = 'Failed';
	endif;

	return $array;
}

/**
 * @param $char_id
 * @param $unix
 *
 * @return array
 */
function ragnarok_ban_char($char_id, $unix) {
	$array = array();
	$ragnarok_db = connect_ragnarok_db2();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $added_timestamp = time() + $unix;
    $data = array(
        'unban_time' => $added_timestamp
    );

    $where = array(
        'char_id' => $char_id
    );

    $result = $ragnarok_db['db']->update('char', $data, $where);

    if ($result):
        $array['status'] = 'OK';
        $array['message'] = 'Thời gian hết ban là ' . date('F j, Y, g:i a', $added_timestamp);
    else:
        $array['status'] = 'Failed';
        $array['message'] = 'Không thể ban nhân vật này.';
    endif;

	return $array;
}

/**
 * @param $account_id
 * @param $unix
 *
 * @return array
 */
function ragnarok_ban_acc($account_id, $unix) {
	$array = array();

	$ragnarok_db = connect_ragnarok_db2();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $added_timestamp = time() + $unix;
    $data = array(
        'unban_time' => $added_timestamp
    );

    $where = array(
        'account_id' => $account_id
    );

    $result = $ragnarok_db['db']->update('login', $data, $where);

    if ($result):
        $array['status'] = 'OK';
        $array['message'] = 'Thời gian hết ban là ' . date('F j, Y, g:i a', $added_timestamp);
    else:
        $array['status'] = 'Failed';
        $array['message'] = 'Không thể ban tài khoản này.';
    endif;

	return $array;
}

/**
 * @param $account_id
 *
 * @return int
 */
function get_last_login_ip_by_account_id($account_id) {
	$ragnarok_db = connect_ragnarok_db();

    $row = $ragnarok_db['db']->get_row(
        $ragnarok_db['db']->prepare('SELECT last_ip FROM `login` WHERE `account_id` = %d', $account_id)
    );

	if ($row):
		return $row->last_ip;
	else:
		return 0;
	endif;
}


/**
 * @param $account_id
 *
 * @return int
 */
function get_last_unique_id_by_account_id($account_id) {
	$ragnarok_db = connect_ragnarok_db();
    $row = $ragnarok_db['db']->get_row(
        $ragnarok_db['db']->prepare('SELECT `last_unique_id` FROM `login` WHERE `account_id` = %d', $account_id)
    );
	if ($row):
		return floatval($row->last_unique_id);
	else:
		return 0;
	endif;
}


/**
 * @param $account_id
 * @param $unix
 * @param string $reason
 *
 * @return array
 */
function ragnarok_ban_ip_by_account_id($account_id, $unix, $reason) {
	$array = array();
	$ragnarok_db = connect_ragnarok_db();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $added_timestamp = time() + $unix;
    $start_time = date('Y-m-d H:i:s');
    $end_time = date('Y-m-d H:i:s', $added_timestamp);

    $ip_to_ban = get_last_login_ip_by_account_id($account_id);

    $result_ban = ragnarok_ban_ip($ip_to_ban, $start_time, $end_time, $reason);

    if ($result_ban['status'] === 'OK'):
        $array['status'] = 'OK';
        $array['message'] = 'Ban IP ' . $ip_to_ban . ' thành công. Thời gian hết ban là ' . date('F j, Y, g:i a', $added_timestamp);
    else:
        return $result_ban;
    endif;

	return $array;
}


/**
 * @param $account_id
 * @param $unban_time
 * @param $reason
 *
 * @return array
 */
function ragnarok_ban_gepard($account_id, $unban_time, $reason) {
	$ragnarok_db = connect_ragnarok_db();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $id_to_ban = get_last_unique_id_by_account_id($account_id);
    $array = ragnarok_ban_gepard_id($id_to_ban, $unban_time, $reason);

	return $array;
}


/**
 * @param $ip
 * @param $start_time
 * @param $end_time
 * @param $reason
 *
 * @return array
 */
function ragnarok_ban_ip($ip, $start_time, $end_time, $reason) {
	$array = array();
	$ragnarok_db = connect_ragnarok_db2();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $ban = array(
        'list' => $ip,
        'btime' => $start_time,
        'rtime' => $end_time,
        'reason' => $reason
    );

    $row = $ragnarok_db['db']->get_row(
        $ragnarok_db['db']->prepare('SELECT list FROM `ipbanlist` WHERE `list` = %s', $ip)
    );

    if ($row):
        $data = array(
            'btime' => $start_time,
            'rtime' => $end_time,
            'reason' => $reason
        );

        $where = array(
            'list' => $ip
        );

        $result = $ragnarok_db['db']->update('ipbanlist', $data, $where);
    else:
        $result = $ragnarok_db['db']->insert('ipbanlist', $ban);
    endif;

    if ($result):
        $array['status'] = 'OK';
        $array['ip'] = $ip;
    else:
        $array['status'] = 'Failed';
        $array['message'] = 'Không thể ban IP.';
        $array['ip'] = $ip;
    endif;

	return $array;
}


/**
 * @param $id
 * @param $unban_time
 * @param $reason
 *
 * @return array
 */
function ragnarok_ban_gepard_id($id, $unban_time, $reason) {
	$array = array();
	$ragnarok_db = connect_ragnarok_db2();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $result = $ragnarok_db['db']->replace(
        'gepard_block',
        array(
            'unique_id' => $id,
            'unban_time' => $unban_time,
            'reason' => $reason
        ),
        array(
            '%d',
            '%s',
            '%s'
        )
    );

    if ($result):
        $array['status'] = 'OK';
        $array['message'] = 'Đã ban Gepard ID ' . $id . ' thành công.';
        $array['id'] = $id;
    else:
        $array['insert'] = $result;
        $array['status'] = 'Failed';
        $array['message'] = 'Không thể ban Gepard ID ' . $id . '.';
        $array['id'] = $id;
    endif;

	return $array;
}


/**
 * @param $account_id
 * @param $amount
 *
 * @return array
 */
function ragnarok_send_cash_to_char($account_id, $amount) {
	$array = array();
	$ragnarok_db = connect_ragnarok_db();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $data = array(
        'account_id' => $account_id,
        'amount' => $amount,
        'send_time' => date("Y-m-d H:i:s"),
    );

    $result = $ragnarok_db['db']->insert(
        'redeem_cp',
        $data,
        array(
            '%d',
            '%d',
            '%s'
        )
    );

    if ($result):
        $array['status'] = 'OK';
        $array['message'] = 'Bạn vừa nhận ' . number_format($amount) . ' CP. Hãy đến gặp Nhân viên VIP hoặc Quản lý coin để nhận CP nhé ~';
    else:
        $array['status'] = 'Failed';
        $array['message'] = 'Không thể gửi CP.';
    endif;

	return $array;
}

/**
 * @param $account_id
 * @param $string
 * @param $reason
 * @return array
 */
function ragnarok_send_items_to_account($account_id, $string, $reason) {
    $array = array();
    $ragnarok_db = connect_ragnarok_db();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $data = array(
        'account_id' => $account_id,
        'items_string' => $string,
        'reason' => $reason,
        'send_time' => date("Y-m-d H:i:s"),
    );

    $array['items_string'] = $string;
    $array['reason'] = $reason;

    $result = $ragnarok_db['db']->insert(
        'redeem_items',
        $data,
        array(
            '%d',
            '%s',
            '%s',
            '%s'
        )
    );

    if ($result):
        $array['status'] = 'OK';
        $array['message'] = 'Bạn vừa nhận một số vật phẩm từ hệ thống. Hãy đến gặp Nhân viên VIP hoặc Quản lý coin để nhận nhé ~';
    else:
        $array['status'] = 'Failed';
        $array['message'] = 'Không thể gửi vật phẩm.';
    endif;

    return $array;
}

/**
 * @param $char_id
 * @param $zeny
 *
 * @return array
 */
function ragnarok_send_zeny_to_char($char_id, $zeny) {
	$array = array();
	$ragnarok_db = connect_ragnarok_db2();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $current_zeny = get_char_zeny($char_id);
    $zeny_to_send =  $current_zeny + $zeny;

    $data = array(
        'zeny' => $zeny_to_send
    );

    $where = array(
        'char_id' => $char_id
    );

    $result = $ragnarok_db['db']->update('char', $data, $where);

    if ($result):
        $array['status'] = 'OK';
        $array['message'] = 'Đã chuyển ' . number_format($zeny) . ' zeny sang nhân vật ' . $char_id . '. Số zeny hiện tại của nhân vật đó: ' . number_format($zeny_to_send) . " zeny";
    else:
        $array['status'] = 'Failed';
        $array['message'] = 'Không thể gửi Zeny cho nhân vật này.';
    endif;

	return $array;
}

/**
 * @param $char_id
 *
 * @return array
 */
function ragnarok_reset_sprites($char_id) {
	$array = array();
	$ragnarok_db = connect_ragnarok_db2();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $data = array(
        'hair' => '0',
        'hair_color' => '0',
        'clothes_color' => '0',
        'body'  => '0'
    );

    $where = array(
        'char_id' => $char_id
    );

    $result = $ragnarok_db['db']->update('char', $data, $where);

    if ($result):
        $array['status'] = 'OK';
        $array['message'] = "Đã phục hồi thời trang về gốc.";
    else:
        $array['status'] = 'Failed';
        $array['message'] = 'Không thể phục hồi hoặc thời trang của nhân vật đã về gốc rồi.';
    endif;

	return $array;
}

/**
 * @param $char_id
 *
 * @return array
 */
function ragnarok_get_off_clothes($char_id) {
	$array = array();
	$ragnarok_db = connect_ragnarok_db2();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $data = array(
        'weapon' => '0',
        'shield' => '0',
        'head_top' => '0',
        'head_mid' => '0',
        'head_bottom' => '0',
        'robe'  => '0'
    );

    $where = array(
        'char_id' => $char_id
    );

    $result_1 = $ragnarok_db['db']->update('char', $data, $where);
    $result_2 = $ragnarok_db['db']->update('inventory', array('equip' => 0), array('char_id' => $char_id));

    $array['status'] = 'OK';
    $array['result_1'] = $result_1;
    $array['result_2'] = $result_2;
    $array['message'] = 'Đã lột sạch!';

	return $array;
}

/**
 * @param $auth
 * @param $birthday
 *
 * @return array
 */
function ragnarok_update_birthday($auth, $birthday) {
	$array = array();
	$ragnarok_db = connect_ragnarok_db2();

	// Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $account = ragnarok_login_by_auth($auth);

    if (is_data_return_ok($account)):
        $account_id = $account['account_id'];

        $data = array(
            'birthdate' => $birthday
        );

        $where = array(
            'account_id' => $account_id
        );

        $result = $ragnarok_db['db']->update('login', $data, $where);

        if ($result):
            $array['status'] = 'OK';
            $array['message'] = 'Đã cập nhật ngày sinh xong!';
        else:
            $array['status'] = 'Failed';
            $array['message'] = 'Không thể cập nhật ngày sinh!';
        endif;
    else:
        return $account;
    endif;

	return $array;
}

/**
 * @param $char_id
 * @param $item_id
 *
 * @return array
 */
function ragnarok_delete_item_from_char($char_id, $item_id) {
	$array = array();
	$ragnarok_db = connect_ragnarok_db2();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $result = $ragnarok_db['db']->delete( 'inventory', array( 'char_id' => $char_id, 'nameid' => $item_id ) );

    if ($result):
        $array['status'] = 'OK';
        $array['message'] = 'Xóa item ' . $item_id . ' ở nhân vật ' . $char_id . ' thành công.';
    else:
        $array['status'] = 'Failed';
        $array['message'] = 'Xóa item thất bại.';
    endif;

	return $array;
}

/**
 * @param $email
 *
 * @return array
 */
function ragnarok_lost_password($email) {
	$array = array();
	$ragnarok_db = connect_ragnarok_db();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $row = $ragnarok_db['db']->get_row(
        $ragnarok_db['db']->prepare('SELECT `userid`, `user_pass` FROM `login` WHERE `email` =  %s', $email)
    );

    if ($row):
        $body = 'Chào ' . $row->user_id . ',<br><br>';
        $body .= 'Bạn đã quên mật khẩu ư? Đừng lo lắng vì hệ thống đã tìm giúp mật khẩu của bạn.<br>';
        $body .= 'Đây là mật khẩu của bạn:<br>';
        $body .= '<strong>'. $row->user_pass .'</strong><br><br>';
        $body .= 'Chúc bạn chơi vui vẻ.<br><br><br>';
        $body .= 'Laniakea GM.';

        $subject = '[Laniakea RO] Đã tìm thấy mật khẩu đăng nhập của bạn';
        $headers = array('Content-Type: text/html; charset=UTF-8');

        $success = wp_mail( $email, $subject, $body, $headers );
        if ($success):
            $array['status'] = 'OK';
            $array['message'] = 'Vui lòng kiểm tra hòm thư ' . $email . ' để lấy lại mật khẩu.';
        else:
            $array['status'] = 'Failed';
            $array['message'] = 'Oops! Có lỗi nào đó xảy ra trong quá trình phục hồi mật khẩu. Vui lòng thử lại sau hoặc liên hệ GM.';
        endif;
    else:
        $array['status'] = 'Failed';
        $array['message'] = 'Không có tài khoản nào đã đăng ký với địa chỉ email này. Vui lòng thử lại.';
    endif;

	return $array;
}

/**
 * @return array
 */
function get_total_number_accounts() {
	$array = array();
	$ragnarok_db = connect_ragnarok_db();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $number = $ragnarok_db['db']->get_row("SELECT count(account_id) as total FROM `login`");

    $array['status'] = 'OK';
    $array['number'] = $number->total;

	return $array;
}

/**
 * @param $number
 * @param $prefix
 * @param $count
 * @param $rewards
 *
 * @return array
 */
function ragnarok_insert_redeem_codes($number, $prefix, $count, $rewards) {
	$array = array();
	$ragnarok_db = connect_ragnarok_db2();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $coupons = coupon::generate_coupons($number, array(
        'length' => 6,
        'prefix'    => $prefix
    ));

    $redeem_codes = array();

    foreach ($coupons as $coupon):

        $redeem = array(
            'code'  => $coupon,
            'count' => floatval($count),
            'rewards' => $rewards
        );

        $result = $ragnarok_db['db']->insert(
            'redeem_codes',
            $redeem,
            array(
                '%s',
                '%d',
                '%s'
            )
        );

        $redeem['result'] = $result;
        $redeem_codes[] = $redeem;
    endforeach;

    $array['status'] = 'OK';
    $array['redeem_codes'] = $redeem_codes;

	return $array;
}

/**
 * @param int $filter
 *
 * @return array
 */
function get_redeem_codes($filter = 2) {
	$array = array();
	$ragnarok_db = connect_ragnarok_db2();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $where = '';
    if ($filter !== 2):
        if ($filter == 0):
            $where = ' WHERE count = 0';
        else:
            $where = ' WHERE count > 0';
        endif;
    endif;

    $rows = $ragnarok_db['db']->get_results("SELECT * FROM `redeem_codes`" . $where . " ORDER BY `code`");

    $array['status'] = 'OK';
    $array['message'] = 'Đã lấy xong';
    $array['codes'] = $rows;

	return $array;
}

/**
 * @param $item_id
 * @return array
 */
function delete_item_id_in_database($item_id) {
    $array = array();
    $ragnarok_db = connect_ragnarok_db2();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $message = '';
    $array['status'] = 'OK';
    $result_1 = $ragnarok_db['db']->delete( 'inventory', array( 'nameid' => $item_id ) );
    $result_2 = $ragnarok_db['db']->delete( 'storage', array( 'nameid' => $item_id ) );
    $result_3 = $ragnarok_db['db']->delete( 'vip_storage', array( 'nameid' => $item_id ) );
    $result_4 = $ragnarok_db['db']->delete( 'cart_inventory', array( 'nameid' => $item_id ) );
    $result_5 = $ragnarok_db['db']->delete( 'guild_storage', array( 'nameid' => $item_id ) );
    $result_6 = $ragnarok_db['db']->delete( 'mail_attachments', array( 'nameid' => $item_id ) );
    $result_7 = $ragnarok_db['db']->delete( 'vending_items', array( 'cartinventory_id' => $item_id ) );
    $result_8 = $ragnarok_db['db']->delete( 'buyingstore_items', array( 'item_id' => $item_id ) );

    if ($result_1 >= 0):
        $message .= 'Xóa thành công ' . $result_1 . ' ở <strong>inventory</strong>.';
    endif;
    if ($result_2 >= 0):
        $message .= '<br>Xóa thành công ' . $result_2 . ' record ở <strong>storage</strong>.';
    endif;
    if ($result_3 >= 0):
        $message .= '<br>Xóa thành công ' . $result_3 . ' record ở <strong>vip_storage</strong>.';
    endif;
    if ($result_4 >= 0):
        $message .= '<br>Xóa thành công ' . $result_4 . ' record ở <strong>cart_inventory</strong>.';
    endif;
    if ($result_5 >= 0):
        $message .= '<br>Xóa thành công ' . $result_5 . ' record ở <strong>guild_storage</strong>.';
    endif;
    if ($result_6 >= 0):
        $message .= '<br>Xóa thành công ' . $result_6 . ' record ở <strong>mail_attachments</strong>.';
    endif;
    if ($result_7 >= 0):
        $message .= '<br>Xóa thành công ' . $result_7 . ' record ở <strong>vending_items</strong>.';
    endif;
    if ($result_8 >= 0):
        $message .= '<br>Xóa thành công ' . $result_8 . ' record ở <strong>buyingstore_items</strong>.';
    endif;

    $array['message'] = $message;

    return $array;
}

/**
 * @param $old_item_id
 * @param $new_item_id
 * @return array
 */
function change_item_id_in_database($old_item_id, $new_item_id) {
    $array = array();
    $ragnarok_db = connect_ragnarok_db2();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $message = '';

    $data = array(
        'nameid' => $new_item_id
    );
    $where = array(
        'nameid' => $old_item_id
    );

    $result_1 = $ragnarok_db['db']->update('inventory', $data, $where);
    $result_2 = $ragnarok_db['db']->update('storage', $data, $where);
    $result_3 = $ragnarok_db['db']->update('vip_storage', $data, $where);
    $result_4 = $ragnarok_db['db']->update('cart_inventory', $data, $where);
    $result_5 = $ragnarok_db['db']->update('guild_storage', $data, $where);
    $result_6 = $ragnarok_db['db']->update('mail_attachments', $data, $where);
    $result_7 = $ragnarok_db['db']->update('vending_items', array('cartinventory_id' => $new_item_id), array('cartinventory_id' => $old_item_id));
    $result_8 = $ragnarok_db['db']->update('buyingstore_items', array('item_id' => $new_item_id), array('item_id' => $old_item_id));

    if ($result_1 >= 0):
        $message .= 'Đổi thành công ' . $result_1 . ' ở inventory.';
    endif;
    if ($result_2 >= 0):
        $message .= '<br>Đổi thành công ' . $result_2 . ' record ở <strong>storage</strong>.';
    endif;
    if ($result_3 >= 0):
        $message .= '<br>Đổi thành công ' . $result_3 . ' record ở <strong>vip_storage</strong>.';
    endif;
    if ($result_4 >= 0):
        $message .= '<br>Đổi thành công ' . $result_4 . ' record ở <strong>cart_inventory</strong>.';
    endif;
    if ($result_5 >= 0):
        $message .= '<br>Đổi thành công ' . $result_5 . ' record ở <strong>guild_storage</strong>.';
    endif;
    if ($result_6 >= 0):
        $message .= '<br>Đổi thành công ' . $result_6 . ' record ở <strong>mail_attachments</strong>.';
    endif;
    if ($result_7 >= 0):
        $message .= '<br>Đổi thành công ' . $result_7 . ' record ở <strong>vending_items</strong>.';
    endif;
    if ($result_8 >= 0):
        $message .= '<br>Đổi thành công ' . $result_8 . ' record ở <strong>buyingstore_items</strong>.';
    endif;

    $array['status'] = 'OK';
    $array['message'] = $message;

    return $array;
}

function count_item_in_database($item_id) {
    $array = array();
    $ragnarok_db = connect_ragnarok_db2();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $inventory = $ragnarok_db['db']->get_results(
        $ragnarok_db['db']->prepare('SELECT IFNULL(SUM(`amount`), 0) as `count` FROM `inventory` where nameid = %d', $item_id)
    );

    $storage = $ragnarok_db['db']->get_results(
        $ragnarok_db['db']->prepare('SELECT IFNULL(SUM(`amount`), 0) as `count` FROM `storage` where nameid = %d', $item_id)
    );

    $vip_storage = $ragnarok_db['db']->get_results(
        $ragnarok_db['db']->prepare('SELECT IFNULL(SUM(`amount`), 0) as `count` FROM `vip_storage` where nameid = %d', $item_id)
    );

    $cart_inventory = $ragnarok_db['db']->get_results(
        $ragnarok_db['db']->prepare('SELECT IFNULL(SUM(`amount`), 0) as `count` FROM `cart_inventory` where nameid = %d', $item_id)
    );

    $guild_storage = $ragnarok_db['db']->get_results(
        $ragnarok_db['db']->prepare('SELECT IFNULL(SUM(`amount`), 0) as `count` FROM `guild_storage` where nameid = %d', $item_id)
    );

    $mail_attachments = $ragnarok_db['db']->get_results(
        $ragnarok_db['db']->prepare('SELECT IFNULL(SUM(`amount`), 0) as `count` FROM `mail_attachments` where nameid = %d', $item_id)
    );

    $array['status'] = 'OK';
    $array['inventory'] = $inventory[0]->count;
    $array['storage'] = $storage[0]->count;
    $array['vip_storage'] = $vip_storage[0]->count;
    $array['cart_inventory'] = $cart_inventory[0]->count;
    $array['guild_storage'] = $guild_storage[0]->count;
    $array['mail_attachments'] = $mail_attachments[0]->count;
    $array['total'] = $inventory->count + $storage[0]->count + $vip_storage[0]->count + $cart_inventory[0]->count + $guild_storage[0]->count + $mail_attachments[0]->count;
    return $array;
}

function get_anniversary_gift($data) {
    $array = array();
    $ragnarok_db = connect_ragnarok_db();

    // Check database connection
    if (!is_data_return_ok($ragnarok_db)):
        return $ragnarok_db;
    endif;

    $account = ragnarok_login_by_auth($data['auth']);
    $account_id = $account['account_id'];
    $unique = get_unique_id_by_account_id($account_id);

    if ($unique):
        $args = array(
            'post_type' => 'gifts',
            'post_status' => 'publish',
            'meta_query'	=> array(
                'relation'		=> 'OR',
                array(
                    'key'	 	=> 'unique_id',
                    'value'	  	=> $unique,
                    'compare' 	=> '=',
                ),
                array(
                    'key'   => 'account_id',
                    'value' => $account_id,
                    'compare' => '='
                )
            ),
        );

        $object = new WP_Query($args);
        if ($object->have_posts()):
            $array['status'] = 'failed';
            $array['message'] = 'Bạn đã nhận quà rồi!';
            return $array;
        else:
            $newbie     = (int)$data['gift']['newbie'];
            $pvp        = (int)$data['gift']['pvp'];
            $upper      = (int)$data['gift']['upper'];
            $aura       = (int)$data['gift']['aura'];
            $message    = (int)$data['gift']['message'];
            $message_t  = $data['gift']['messageText'];

            $args = array(
                'post_type' => 'gifts',
                'post_status' => 'publish',
                'post_title' => $unique . ' - ' . $account_id . ' - ' . $account['username']
            );

            $post_id = wp_insert_post($args);

            // Nếu không thêm được bài viết
            if ($post_id == 0):
                $array['status'] = 'Failed';
                $array['message'] = 'Có lỗi xảy ra. Vui lòng thử lại sau.';
                return $array;
            endif;

            // Update fields
            update_field('account_id', $account_id, $post_id);
            update_field('unique_id', $unique, $post_id);
            update_field('value_1', $newbie, $post_id);
            update_field('value_2', $pvp, $post_id);
            update_field('value_3', $upper, $post_id);
            update_field('value_4', $aura, $post_id);
            update_field('value_5', $message, $post_id);
            update_field('value_6', $message_t, $post_id);

            // Quà bước 1
            $string = "12210:5,12208:5,50008:2,51002:2";

            // Quà bước 2
            if ($newbie == 1)
                $string .= ",52069:10,25464:100,28914:1,52080:1";
            else
                $string .= ",52009:1000,22529:10,52063:3";

            // Quà bước 3
            if ($pvp == 1)
                $string .= ",6732:500,52079:1";
            else
                $string .= ",7829:500,52081:1";

            // Quà bước 4
            if ($upper == 1)
                $string .= ",41077:1";
            else if ($upper == 2)
                $string .= ",42367:1";
            else if ($upper == 3)
                $string .= ",41130:1";
            else if ($upper == 4)
                $string .= ",42118:1";
            else if ($upper == 5)
                $string .= ",31794:1";

            // Quà bước 5
            if ($aura == 1)
                $string .= ",Aura:129,51502:1";
            else if ($aura == 2)
                $string .= ",Aura:138,51502:1";
            else if ($aura == 3)
                $string .= ",Aura:42,51502:1";
            else if ($aura == 4)
                $string .= ",Aura:97,51502:1";
            else if ($aura == 5)
                $string .= ",51504:1,51502:1";

            // Quà bước 6
            if ($message == 1) {
                $stone = rand(46029,46031);
                $string .= "," . $stone . ":1";
            } else if ($message == 2) {
                $stone = rand(46017,46019);
                $string .= "," . $stone . ":1";
            } else if ($message == 3) {
                $stone = rand(46011,46013);
                $string .= "," . $stone . ":1";
            } else if ($message == 4) {
                $string .= ",50020:1";
            }

            // Quà bước 7 (C 20th Anniversary Balloon)
            $string .= ",48006:1";

            // Quà bước 8
            $string .= ",19637:1,19634:1,42750:1";

            $data = array(
                'account_id' => $account_id,
                'items_string' => $string,
                'reason' => 'Qua tri an tu LaniakeaRO va ki niem 20 nam thanh lap Gravity',
                'send_time' => date("Y-m-d H:i:s"),
            );

            $result = $ragnarok_db['db']->insert(
                'redeem_items',
                $data,
                array(
                    '%d',
                    '%s',
                    '%s',
                    '%s'
                )
            );

            if ($result):
                $array['status'] = 'OK';
                $array['message'] = 'Bạn đã nhận quà thành công!';
            else:
                $array['status'] = 'Failed';
                $array['message'] = 'Có lỗi xảy ra, vui lòng thử lại sau!';
            endif;
        endif;
    else:
        $array['status'] = "Failed";
        $array['message'] = "Bạn cần phải tạo nhân vật và vào game trước. <a target='_blank' href='https://hahiu.com/newbie/tao-nhan-vat-gameplay/'>Hướng dẫn tạo nhân vật</a>.";
    endif;
    return $array;
}