<?php

// Register Custom Post Type
function registers_cpt() {
	$labels = array(
		'name'                  => _x( 'Registers', 'Post Type General Name', 'laniakea' ),
		'singular_name'         => _x( 'Register', 'Post Type Singular Name', 'laniakea' ),
		'menu_name'             => __( 'Registers', 'laniakea' ),
		'name_admin_bar'        => __( 'Registers', 'laniakea' ),
		'archives'              => __( 'Register Archives', 'laniakea' ),
		'attributes'            => __( 'Register Attributes', 'laniakea' ),
		'parent_item_colon'     => __( 'Parent Register:', 'laniakea' ),
		'all_items'             => __( 'All Registers', 'laniakea' ),
		'add_new_item'          => __( 'Add New Register', 'laniakea' ),
		'add_new'               => __( 'Add New', 'laniakea' ),
		'new_item'              => __( 'New Register', 'laniakea' ),
		'edit_item'             => __( 'Edit Register', 'laniakea' ),
		'update_item'           => __( 'Update Register', 'laniakea' ),
		'view_item'             => __( 'View Register', 'laniakea' ),
		'view_items'            => __( 'View Registers', 'laniakea' ),
		'search_items'          => __( 'Search Register', 'laniakea' ),
		'not_found'             => __( 'Not found', 'laniakea' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'laniakea' ),
		'featured_image'        => __( 'Featured Image', 'laniakea' ),
		'set_featured_image'    => __( 'Set featured image', 'laniakea' ),
		'remove_featured_image' => __( 'Remove featured image', 'laniakea' ),
		'use_featured_image'    => __( 'Use as featured image', 'laniakea' ),
		'insert_into_item'      => __( 'Insert into register', 'laniakea' ),
		'uploaded_to_this_item' => __( 'Uploaded to this register', 'laniakea' ),
		'items_list'            => __( 'Registers list', 'laniakea' ),
		'items_list_navigation' => __( 'Registers list navigation', 'laniakea' ),
		'filter_items_list'     => __( 'Filter registers list', 'laniakea' ),
	);
	$args = array(
		'label'                 => __( 'Registers', 'laniakea' ),
		'description'           => __( 'Registers', 'laniakea' ),
		'labels'                => $labels,
		'supports'              => array('title'),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'registers', $args );

}
add_action( 'init', 'registers_cpt', 0 );

// Albums
function cpt_albums() {
    $labels = array(
        'name'                  => _x( 'Albums', 'Post Type General Name', 'laniakea' ),
        'singular_name'         => _x( 'Album', 'Post Type Singular Name', 'laniakea' ),
        'menu_name'             => __( 'Albums', 'laniakea' ),
        'name_admin_bar'        => __( 'Albums', 'laniakea' ),
        'archives'              => __( 'Album Archives', 'laniakea' ),
        'attributes'            => __( 'Album Attributes', 'laniakea' ),
        'parent_item_colon'     => __( 'Parent Album:', 'laniakea' ),
        'all_items'             => __( 'All Albums', 'laniakea' ),
        'add_new_item'          => __( 'Add New Album', 'laniakea' ),
        'add_new'               => __( 'Add New', 'laniakea' ),
        'new_item'              => __( 'New Album', 'laniakea' ),
        'edit_item'             => __( 'Edit Album', 'laniakea' ),
        'update_item'           => __( 'Update Album', 'laniakea' ),
        'view_item'             => __( 'View Album', 'laniakea' ),
        'view_items'            => __( 'View Album', 'laniakea' ),
        'search_items'          => __( 'Search Album', 'laniakea' ),
        'not_found'             => __( 'Not found', 'laniakea' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'laniakea' ),
        'featured_image'        => __( 'Featured Image', 'laniakea' ),
        'set_featured_image'    => __( 'Set featured image', 'laniakea' ),
        'remove_featured_image' => __( 'Remove featured image', 'laniakea' ),
        'use_featured_image'    => __( 'Use as featured image', 'laniakea' ),
        'insert_into_item'      => __( 'Insert into album', 'laniakea' ),
        'uploaded_to_this_item' => __( 'Uploaded to this album', 'laniakea' ),
        'items_list'            => __( 'Albums list', 'laniakea' ),
        'items_list_navigation' => __( 'Albums list navigation', 'laniakea' ),
        'filter_items_list'     => __( 'Filter albums list', 'laniakea' ),
    );
    $args = array(
        'label'                 => __( 'Albums', 'laniakea' ),
        'description'           => __( 'Albums', 'laniakea' ),
        'labels'                => $labels,
        'supports'              => array('title', 'editor', 'thumbnail'),
        'taxonomies'            => array('album_category'),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'albums', $args );

}
add_action( 'init', 'cpt_albums', 0 );


function cpt_gifts() {
    $labels = array(
        'name'                  => _x( 'Gifts', 'Post Type General Name', 'laniakea' ),
        'singular_name'         => _x( 'Gift', 'Post Type Singular Name', 'laniakea' ),
        'menu_name'             => __( 'Gifts', 'laniakea' ),
        'name_admin_bar'        => __( 'Gifts', 'laniakea' ),
        'archives'              => __( 'Gift Archives', 'laniakea' ),
        'attributes'            => __( 'Gift Attributes', 'laniakea' ),
        'parent_item_colon'     => __( 'Parent Gift:', 'laniakea' ),
        'all_items'             => __( 'All Gifts', 'laniakea' ),
        'add_new_item'          => __( 'Add New Gift', 'laniakea' ),
        'add_new'               => __( 'Add New', 'laniakea' ),
        'new_item'              => __( 'New Gift', 'laniakea' ),
        'edit_item'             => __( 'Edit Gift', 'laniakea' ),
        'update_item'           => __( 'Update Gift', 'laniakea' ),
        'view_item'             => __( 'View Gift', 'laniakea' ),
        'view_items'            => __( 'View Gift', 'laniakea' ),
        'search_items'          => __( 'Search Gift', 'laniakea' ),
        'not_found'             => __( 'Not found', 'laniakea' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'laniakea' ),
        'featured_image'        => __( 'Featured Image', 'laniakea' ),
        'set_featured_image'    => __( 'Set featured image', 'laniakea' ),
        'remove_featured_image' => __( 'Remove featured image', 'laniakea' ),
        'use_featured_image'    => __( 'Use as featured image', 'laniakea' ),
        'insert_into_item'      => __( 'Insert into gift', 'laniakea' ),
        'uploaded_to_this_item' => __( 'Uploaded to this gift', 'laniakea' ),
        'items_list'            => __( 'Gifts list', 'laniakea' ),
        'items_list_navigation' => __( 'Gift list navigation', 'laniakea' ),
        'filter_items_list'     => __( 'Filter gifts list', 'laniakea' ),
    );
    $args = array(
        'label'                 => __( 'Gifts', 'laniakea' ),
        'description'           => __( 'Gifts', 'laniakea' ),
        'labels'                => $labels,
        'supports'              => array('title'),
        'taxonomies'            => array(),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'gifts', $args );

}
add_action( 'init', 'cpt_gifts', 0 );
