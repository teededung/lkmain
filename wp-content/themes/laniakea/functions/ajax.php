<?php

/**
 * MAIN AJAX FUNCTION
 * $ajax_nonce = wp_create_nonce( "n" );
 * @return string
 *
 * -----------------------------------------
 */
function tee_ajax_function(){
	$check_nonce = check_ajax_referer( 'n', 'n', false );
    $result = array();

	if ($check_nonce):
		$method = $_POST['method'];

		switch ($method):
            case 'register_account':
                $result = register_account($_POST['username'], $_POST['password'], $_POST['email'], $_POST['captcha']);
                break;
            case 'ragnarok_login':
				$result = ragnarok_login($_POST['username'], $_POST['password']);
				break;

			case 'get_account_information':
				$result = get_account_information($_POST['auth']);
				break;

			case 'get_cp_by_account_id':
				$result = get_account_cp_by_account_id($_POST['account_id']);
				$result = array(
					'status'    => 'OK',
					'cp'        => $result
				);
				break;

			case 'ragnarok_change_password':
				$result = ragnarok_change_password($_POST['data']);
				break;

            case 'ragnarok_update_birthday':
                $result = ragnarok_update_birthday($_POST['auth'], $_POST['birthday']);
                break;

            case 'ragnarok_lost_password':
                $result = ragnarok_lost_password($_POST['email']);
                break;

            case 'submit_content_event':
                $data = array(
                    'img_title' => $_POST['img_title'],
                    'img_description' => $_POST['img_description'],
                    'char_name' => $_POST['char_name'],
                    'event_slug' => $_POST['event_slug']
                );
                $result = submit_content_event($_POST['auth'], $data, $_FILES);
                break;

            case 'delete_img_from_album':
                $result = delete_img_from_album($_POST['auth'], $_POST['attach_id'], $_POST['event_slug']);
                break;

            case 'load_single_image_content':
                echo $result = load_single_image_content($_POST['index'], $_POST['postID']);
                exit;

            case 'vote_album':
                $result = vote_album($_POST['albumId'], $_POST['type'], $_POST['name'], $_POST['value']);
                break;

            case 'get_anniversary_gift':
                $result = get_anniversary_gift($_POST['data']);
                break;

            case 'get_mob_info_':
                $result = get_mob_info_($_POST['mobID']);
                break;

            default:
                break;
		endswitch;
	else:
        $result['status'] = 'Invalid nonce';
	endif;

	echo json_encode($result);
	exit;
}
add_action( 'wp_ajax_nopriv_tee_ajax_function', 'tee_ajax_function' );
add_action( 'wp_ajax_tee_ajax_function', 'tee_ajax_function' );


/**
 * MAIN AJAX FUNCTION WITH PRIV
 * $ajax_nonce = wp_create_nonce( "n" );
 * @return string
 *
 * -----------------------------------------
 */
function tee_ajax_function_(){
    $check_nonce = check_ajax_referer( 'n', 'n', false );
    $result = array();

    if ($check_nonce):
        $method = $_POST['method'];

        switch ($method):
            // Admin ----------------------------
            case 'get_unique_id_by_account_id':
                $result = get_unique_id_by_account_id($_POST['account_id']);
                $result = array(
                    'status' => 'OK',
                    'last_unique_id' => $result
                );
                break;

            case 'resend_activation_email':
                $result = resend_activation_email($_POST['post_id']);
                break;

            case 'get_ragnarok_users':
                $result = get_ragnarok_users($_POST['type']);
                break;

            case 'get_all_chars':
                $result = get_all_chars();
                break;

            case 'ragnarok_ban_char':
                $result = ragnarok_ban_char($_POST['char_id'], $_POST['unix_time_to_add']);
                break;

            case 'ragnarok_ban_acc':
                $result = ragnarok_ban_acc($_POST['account_id'], $_POST['unix_time_to_add']);
                break;

            case 'ragnarok_ban_ip_by_account_id':
                $result = ragnarok_ban_ip_by_account_id($_POST['account_id'], $_POST['unix_time_to_add'], $_POST['reason']);
                break;

            case 'ragnarok_ban_gepard':
                $result = ragnarok_ban_gepard($_POST['account_id'], $_POST['unban_time'], $_POST['reason']);
                break;

            case 'ragnarok_send_cash_to_char':
                $result = ragnarok_send_cash_to_char($_POST['account_id'], $_POST['cash']);
                break;

            case 'ragnarok_send_items_to_account':
                $result = ragnarok_send_items_to_account($_POST['account_id'], $_POST['items_string'], $_POST['reason']);
                break;

            case 'ragnarok_send_zeny_to_char':
                $result = ragnarok_send_zeny_to_char($_POST['char_id'], $_POST['zeny']);
                break;

            case 'ragnarok_reset_sprites':
                $result = ragnarok_reset_sprites($_POST['char_id']);
                break;

            case 'ragnarok_get_off_clothes':
                $result = ragnarok_get_off_clothes($_POST['char_id']);
                break;

            case 'ragnarok_delete_item_from_char':
                $result = ragnarok_delete_item_from_char($_POST['char_id'], $_POST['id']);
                break;

            case 'ragnarok_insert_redeem_codes':
                $result = ragnarok_insert_redeem_codes($_POST['redeemCodeNumber'], $_POST['redeemCodePrefix'], $_POST['redeemCodeCount'], $_POST['redeemCodeRewards']);
                break;

            case 'get_redeem_codes':
                $result = get_redeem_codes($_POST['filter']);
                break;

            case 'delete_item_id_in_database':
                $result = delete_item_id_in_database($_POST['item_id']);
                break;

            case 'change_item_id_in_database':
                $result = change_item_id_in_database($_POST['old_item_id'], $_POST['new_item_id']);
                break;

            case 'count_item_in_database':
                $result = count_item_in_database($_POST['item_id']);
                break;

            default:
                # code...
                break;

        endswitch;
    else:
        $result['status'] = 'Invalid nonce';
    endif;

    echo json_encode($result);
    exit;
}
add_action( 'wp_ajax_tee_ajax_function_', 'tee_ajax_function_' );
/*---------------------------------------------*/


/**
 * Just a test function
 * @return array
 */
function test_ajax() {
    $array = array();
    $array['status']    = 'OK';
    $array['data']      = array(1, 2);
    return $array;
}

function get_mob_info_($mob_id) {
    $url = "https://www.divine-pride.net/api/database/Monster/" . $mob_id . "?apiKey=ee9c0edb08497010b52696325f06fae1";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    $result = curl_exec($ch);
    curl_close($ch);
    $result = json_decode($result);

    $array['status'] = "OK";
    $array['data'] = $result;
    return $array;
}

/**
 * @param $captcha
 *
 * @return bool
 */
function check_valid_recaptcha($captcha) {
	if (is_local()) return true;

	$is_valid = true;

	if (!$captcha):
		$is_valid = false;
	endif;

	$secret     = "6LfD60YUAAAAANoCqrf29_EhvN0981F0hddD9C2B";

	$data = array(
		'secret'    => $secret,
		'response'  => $captcha
	);
	$options = array(
		'http' => array (
			'method' => 'POST',
			'content' => http_build_query($data)
		)
	);
	$context  = stream_context_create($options);
	$verify = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
	$captcha_success = json_decode($verify);

	if ($captcha_success->success != true || !$captcha_success) :
		$is_valid = false;
	endif;

	return $is_valid;
}

/**
 * @param $username
 * @param $password
 * @param $email
 * @param $captcha
 *
 * @return array
 */
function register_account($username, $password, $email, $captcha) {
	$array = array();

	if (!get_field('allow_register', 'option')):
		$array['status'] = 'Failed';
		$array['message'] = get_field('message_register', 'option');
		return $array;
	endif;

	if (check_valid_recaptcha($captcha)):
        // Register an account
		$result = ragnarok_register_account($username, $password, $email);

        // Login test
        $login_data = ragnarok_login($username, $password);

		if (is_data_return_ok($result) && is_data_return_ok($login_data)):
            $array['status'] = 'OK';
            $array['laniakea'] = $login_data['laniakea'];

            // Insert register to WP
			$user = array(
				'post_title' => $username,
				'post_type' => 'registers',
				'post_status' => 'publish'
			);

			$post_id = wp_insert_post($user);

			if ($post_id > 0):
				// Update Account ID Field
                update_field('account_id', $result['account_id'], $post_id);

                // Update Email Field
				update_field('email', $email, $post_id);
			endif;
		else:
			$array['status']  = 'Failed';
			$array['message'] = $result['message'];
		endif;
	else:
		$array['status'] = 'Failed';
		$array['message'] = 'Vui lòng xác nhận lại bạn không phải là người máy!';
	endif;

	return $array;
}


/**
 * @param $auth
 * @param $data
 * @param $file
 * @return array
 */
function submit_content_event($auth, $data, $file) {
    // Login test
    $account = ragnarok_login_by_auth($auth);
    if (!is_data_return_ok($account)):
        return $account;
    endif;

    $array = array();
    $array['status'] = 'Failed';
    $array['account_id'] = $account['account_id'];

    $event_slug = $data['event_slug'];

    $args = array(
        'post_type' => 'albums',
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key'	 	=> 'account_id',
                'value'	  	=> $account['account_id'],
                'compare' 	=> '=',
            ),
        ),
        'tax_query' => array(
            array(
                'taxonomy' => 'album_category',
                'field' => 'slug',
                'terms' => $event_slug,
            )
        )
    );

    $array['$event_slug'] = $event_slug;

    $album_term = get_term_by('slug', $event_slug, 'album_category');
    $event_name = $album_term->name;
    $array['$event_name'] = $event_name;
    $album_object = new WP_Query($args);

    $img_title = $data['img_title'];
    $img_description = $data['img_description'];

    // Insert a new album
    if (!$album_object->have_posts()):
        $char_name = $data['char_name'];

        $album_title        = $event_name . ' - ' . $char_name;
        $album_description  = "Album ". $event_name ." - của bạn " . $char_name . ".";

        $args = array(
            'post_type' => 'albums',
            'post_status' => 'pending',
            'post_title' => $album_title,
            'post_content' => $album_description
        );

        $post_id = wp_insert_post($args);

        if ($post_id > 0):
            // Add category
            wp_set_object_terms( $post_id, $event_slug, 'album_category' );

            // Update fields
            update_field('account_id', $account['account_id'], $post_id);
            update_field('char_name', $char_name, $post_id);

            // Update image
            $image      = $file['image'];
            $pretext    = str_replace(" ", "_", $event_slug);
            $pretext    = str_replace("/", "_", $pretext);
            $filename   = $pretext . '_' . $account['account_id'] . '_' . date('Ymd_His') . '.' . mime2ext($image['type']);
            $meta_data  = array(
                'title' => $event_name . " - " . $img_title,
                'caption' => $event_name . " - " . $account['account_id'] . ' - ' . $char_name,
                'description' => $img_description,
                'alt' => 'LaniakeaRO - ' . $event_name . " - " . $img_title,
            );
            $attach_id  = insert_image_to_post($filename, $image['tmp_name'], $post_id, $meta_data);

            // Add to gallery field
            $array_gallery_item = get_field('field_5aaa563462f6c', $post_id, false);
            if (!is_array($array)):
                $array_gallery_item = array();
            endif;
            $array_gallery_item[] = $attach_id;
            update_field('field_5aaa563462f6c', $array_gallery_item, $post_id);

            $array['status']    = 'OK';
            $array['type']      = 'INSERT';
            $array['title']     = 'Bạn vừa tải ảnh lên thành công!';
            $array['message']   = 'Ấn OK để tải lại trang';
        else:
            $array['title']     = 'Oops...';
            $array['message']   = "Không thể tạo album.";
        endif;

    else: // Update existed album
        while ($album_object->have_posts()): $album_object->the_post();
            $post_id = get_the_ID();
            $char_name = get_field('char_name');
            $array_gallery_item = get_field('field_5aaa563462f6c', $post_id);

            // Max
            if (count($array_gallery_item) == 10):
                $array['status'] = 'Failed';
                $array['message'] = 'Album đã đạt giới hạn 10 hình. Nếu bạn muốn đổi ảnh thì hãy xóa ảnh cũ và tải ảnh lên lại.';
                return $array;
            endif;

            // Add more image
            $image      = $file['image'];
            $pretext    = str_replace(" ", "_", $event_slug);
            $pretext    = str_replace("/", "_", $pretext);
            $filename   = str_replace(" ", "_", $pretext) . '_' . $account['account_id'] . '_' . date('Ymd_His') . '.' . mime2ext($image['type']);
            $meta_data  = array(
                'title' => $event_name . " - " . $img_title,
                'caption' => $event_name . " - " . $account['account_id'] . ' - ' . $char_name,
                'description' => $img_description,
                'alt' => 'LaniakeaRO - ' . $event_name . " - " . $img_title,
            );
            $attach_id  = insert_image_to_post($filename, $image['tmp_name'], $post_id, $meta_data);

            // Add to gallery field
            $array_gallery_item = get_field('field_5aaa563462f6c', $post_id, false);
            if (!is_array($array)):
                $array_gallery_item = array();
            endif;
            $array_gallery_item[] = $attach_id;
            update_field('field_5aaa563462f6c', $array_gallery_item, $post_id);

            $array['status'] = 'OK';
            $array['type'] = 'UPDATE';
            $array['title']     = 'Bạn vừa thêm ảnh vào album thành công!';
            $array['message']   = 'Ấn OK để tải lại trang';
        endwhile;
    endif;

    return $array;
}

/***
 * @param $auth
 * @param $attach_id
 * @param $event_slug
 * @return array
 */
function delete_img_from_album($auth, $attach_id, $event_slug) {
    // Login test
    $account = ragnarok_login_by_auth($auth);
    if (!is_data_return_ok($account)):
        return $account;
    endif;

    $array = array();

    $args = array(
        'post_type' => 'albums',
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key'	 	=> 'account_id',
                'value'	  	=> $account['account_id'],
                'compare' 	=> '=',
            ),
        ),
        'tax_query' => array(
            array(
                'taxonomy' => 'album_category',
                'field' => 'slug',
                'terms' => $event_slug,
            )
        )
    );

    $album_object = new WP_Query($args);

    // Make sure this album has this attach id
    if ($album_object->have_posts()):
        while ($album_object->have_posts()): $album_object->the_post();
            $gallery_items = get_field('gallery');
            foreach($gallery_items as $img):
                if ($img['id'] == $attach_id):
                    if (false === wp_delete_attachment($attach_id)):
                        $array['status'] = 'Failed';
                        $array['message'] = 'Không tìm thấy ảnh để xóa!';
                    else:
                        $array['status'] = 'OK';
                    endif;
                endif;
            endforeach;
        endwhile;
    else:
        $array['status'] = 'Failed';
        $array['message'] = 'Không tìm thấy ảnh để xóa!';
    endif;

    return $array;
}


function load_single_image_content($index, $post_id) {
    ob_start(); ?>

    <?php
    $author = get_field('char_name', $post_id);
    $gallery = get_field('gallery', $post_id);
    $gallery_item = $gallery[$index];
    $title = str_replace('Halloween 2019 - ', '', $gallery_item['title']);
    ?>

    <div class="cbp-l-inline">
        <div class="cbp-l-inline-left">
            <a href="<?php echo $gallery_item['url']; ?>" class="cbp-lightbox">
                <img src="<?php  echo $gallery_item['url']; ?>" alt="<?php echo $gallery_item['alt']; ?>">
            </a>
        </div>

        <div class="cbp-l-inline-right">
            <div class="cbp-l-inline-title"><?php echo $title; ?></div>
            <?php if ($author): ?>
            <div class="cbp-l-inline-subtitle">bởi <?php echo $author; ?></div>
            <?php endif; ?>
            <div class="cbp-l-inline-desc"><?php echo esc_html($gallery_item['description']); ?></div>

            <hr>

            <div class="d-flex align-items-center">
                <span class="mr-3">Chia sẻ qua:</span>
                <div class="cbp-l-project-social">
                    <a href="#" class="cbp-social-fb" title="Share on Facebook" rel="nofollow">
                        <svg width="30" height="30" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 486.392 486.392">
                            <path d="M243.196 0C108.89 0 0 108.89 0 243.196s108.89 243.196 243.196 243.196 243.196-108.89 243.196-243.196C486.392 108.86 377.502 0 243.196 0zm62.866 243.165l-39.854.03-.03 145.917h-54.69V243.196H175.01v-50.28l36.48-.03-.062-29.61c0-41.04 11.126-65.997 59.43-65.997h40.25v50.31h-25.17c-18.818 0-19.73 7.02-19.73 20.122l-.06 25.17h45.233l-5.316 50.28z"></path>
                        </svg>
                    </a>
                    <a href="#" class="cbp-social-twitter" title="Share on Twitter" rel="nofollow">
                        <svg width="30" height="30" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 486.392 486.392">
                            <path d="M243.196 0C108.891 0 0 108.891 0 243.196s108.891 243.196 243.196 243.196 243.196-108.891 243.196-243.196C486.392 108.861 377.501 0 243.196 0zm120.99 188.598l.182 7.752c0 79.16-60.221 170.359-170.359 170.359-33.804 0-65.268-9.91-91.776-26.904 4.682.547 9.454.851 14.288.851 28.059 0 53.868-9.576 74.357-25.627-26.204-.486-48.305-17.814-55.935-41.586 3.678.699 7.387 1.034 11.278 1.034 5.472 0 10.761-.699 15.777-2.067-27.39-5.533-48.031-29.7-48.031-58.701v-.76c8.086 4.499 17.297 7.174 27.116 7.509-16.051-10.731-26.63-29.062-26.63-49.825 0-10.974 2.949-21.249 8.086-30.095 29.518 36.236 73.658 60.069 123.422 62.562-1.034-4.378-1.55-8.968-1.55-13.649 0-33.044 26.812-59.857 59.887-59.857 17.206 0 32.771 7.265 43.714 18.908 13.619-2.706 26.448-7.691 38.03-14.531-4.469 13.984-13.953 25.718-26.326 33.135 12.069-1.429 23.651-4.682 34.382-9.424-8.025 11.977-18.209 22.526-29.912 30.916z"></path>
                        </svg>
                    </a>
                </div>
            </div>

            <div class="cbp-l-inline-view-wrap">
                <a href="<?php echo $gallery_item['url']; ?>" class="cbp-lightbox cbp-l-inline-view">Phóng to</a>
            </div>
        </div>
    </div>
    <?php
    return ob_get_clean();
}

/**
 * @param $album_id
 * @param $type
 * @param $name
 * @param $value
 * @return array
 */
function vote_album($album_id, $type, $name, $value) {
    $array = array();

    $album = get_post($album_id);

    if (!$album):
        $array['status'] = "Failed";
        $array['title'] = "Không thể bình chọn";
        $array['message'] = "Không tìm thấy album để bình chọn.";
        return $array;
    endif;

    $votes = get_field('votes', $album_id);

    if ($votes):
        foreach ($votes as $vote):
            if ($vote['type'] == $type && $vote['value'] == $value):
                $array['status'] = "Failed";
                $array['title'] = "Không thể bình chọn";
                $array['message'] = "Bạn đã bình chọn album này rồi.";
                return $array;
            endif;
        endforeach;
    endif;

    $row = array(
        'type'  => $type,
        'name'  => $name,
        'value' => $value
    );

    add_row('votes', $row, $album_id);
    $array['status'] = "OK";
    $array['title'] = "Bình chọn thành công";
    $array['message'] = "Bạn vừa bình chọn thành công.";
    $array['albumVotes'] = count(get_field('votes', $album_id));
    return $array;
}