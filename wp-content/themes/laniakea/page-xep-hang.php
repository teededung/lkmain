<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

	<div class="container content">

        <h1 class="text-center margin-bottom-30">Bảng xếp hạng</h1>

        <?php $ragnarok_db = connect_ragnarok_db(); ?>

        <?php if ($ragnarok_db['status'] == 'OK'): ?>
	        <?php
            $rows = $ragnarok_db['db']->get_results(
                "SELECT char.char_id, char.name, char.zeny, char.class, char.base_level, char.base_exp, char.job_level, char.job_exp, char.guild_id, guild.name as guild_name FROM `char` 
                LEFT JOIN guild ON char.guild_id = guild.guild_id
                WHERE 1 = 1 
                ORDER BY char.zeny DESC, char.base_level DESC, char.base_exp DESC, char.job_level DESC, char.job_exp DESC, char.char_id ASC LIMIT 10"
            );
            ?>
            <div class="rank-zeny">
                <h2 class="margin-bottom-20">Top phú hộ:</h2>

                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Hạng</th>
                        <th scope="col">Tên nhân vật</th>
                        <th scope="col" class="text-right">Zeny</th>
                        <th scope="col">Nghề</th>
                        <th scope="col">Cấp độ</th>
                        <th scope="col">Cấp độ nghề</th>
                        <th scope="col">Bang hội</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($rows as $key => $row): ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo $row->name; ?></td>
                                <td class="text-right"><?php echo number_format($row->zeny, 0); ?></td>
                                <td><?php echo get_job_name($row->class); ?></td>
                                <td><?php echo $row->base_level; ?></td>
                                <td><?php echo $row->job_level; ?></td>
                                <td>
                                    <?php echo ($row->guild_name) ? "<span class='guild-name'>". $row->guild_name . "</span>" : 'Chưa có'; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
	</div>

<?php endwhile; ?>
<?php get_footer(); ?>
