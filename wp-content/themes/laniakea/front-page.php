<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

    <div class="container">

        <!-- Heading Row -->
        <div class="row my-4">
            <div class="col-lg-8">
                <?php echo do_shortcode('[rev_slider alias="home-banner"]'); ?>
            </div>
            <!-- /.col-lg-8 -->
            <div class="col-lg-4 col-present">
                <h1>Laniakea RO</h1>
                <p>
                    Ragnarok Online đã từng lại mang đến cho chúng ta thật nhiều cung bậc cảm xúc...
                    Sau một thời gian vắng bóng, Ragnarok đã trở lại với nhiều tính năng, nhiều nhiệm vụ mới.
                    Quả thực thì đây chính là trò chơi có sức hồi sinh mạnh mẽ và Laniakea RO chắc rằng Ragnarok sẽ làm cho các bạn cuốn hút như thuở ban đầu.
                    Chính vì thế Laniakea RO mở ra để mọi người cùng nhau ôn lại kỉ niệm xưa, tìm lại bạn bè cũ và cùng nhau khám phá Ragnarok.
                </p>
            </div>
            <!-- /.col-md-4 -->
        </div>
        <!-- /.row -->

        <div class="card text-white bg-secondary my-4 text-center">
            <div class="card-body">
                <div class="text-white m-0">
                    <h4 class="title-information">RENEWAL</h4>
                    <div class="text-center">
                        <span class="text-primary-color">Episode 17.1</span>
                    </div>
                    <div class="text-center">
                        <span class="small">Level 200/70</span>
                    </div>
                    <hr>
                    <div class="row row-rates">
                        <div class="col">
                            <div>Base Level</div>
                            <div>x <span class="count">30</span></div>
                        </div>
                        <div class="col">
                            <div>Job Level</div>
                            <div>x <span class="count">30</span></div>
                        </div>
                        <div class="col">
                            <div>Tỉ lệ rớt đồ</div>
                            <div>x <span class="count">30</span></div>
                        </div>
                        <div class="col">
                            <div>Tỉ lệ rớt card</div>
                            <div>x <span class="count">5</span></div>
                        </div>
                        <div class="col">
                            <div>MVP</div>
                            <div>x <span class="count">1</span></div>
                        </div>
                    </div>
                    <hr>

                    <h6 class="title-server-status">Server status</h6>
                    <?php $server_status = get_server_status(); ?>

                    <div class="row justify-content-center align-item-center row-server-status">
                        <div class="col col-char">
                            Char <?php echo ($server_status['char'] == 'OK') ? '<i class="ok ion-checkmark-round"></i>' : '<i class="failed ion-close-round"></i>'; ?>
                        </div>
                        <div class="col col-login">
                            Login <?php echo ($server_status['login'] == 'OK') ? '<i class="ok ion-checkmark-round"></i>' : '<i class="failed ion-close-round"></i>'; ?>
                        </div>
                        <div class="col col-map">
                            Map <?php echo ($server_status['login'] == 'OK') ? '<i class="ok ion-checkmark-round"></i>' : '<i class="failed ion-close-round"></i>'; ?>
                        </div>
                    </div>

                    <hr>

                    <div class="row justify-content-center align-item-center row-server-status row-server-ingame w-100">
                        <?php
                        $info = get_server_info();
                        if ($info['status'] !== 'OK'):
                            $num_char = 0;
                            $num_acc = 0;
                            $num_guild = 0;
                        else:
                            $num_char = $info['count_char'];
                            $num_acc = $info['count_acc'];
                            $num_guild = $info['count_guild'];
                        endif;
                        ?>
                        <div class="col-md-3">
                            <div>Tổng số tài khoản</div>
                            <div class="ingame-num"><?php echo $num_acc; ?></div>
                        </div>
                        <div class="col-md-3">
                            <div>Tổng số nhân vật</div>
                            <div class="ingame-num"><?php echo $num_char; ?></div>
                        </div>
                        <div class="col-md-3">
                            <div>Tổng số bang hội</div>
                            <div class="ingame-num"><?php echo $num_guild; ?></div>
                        </div>

	                    <?php $users = ragnarok_get_number_online_chars(); ?>
	                    <?php if ($users['status'] == 'OK'): ?>
                        <div class="col-md-3">
                            <div>Online</div>
                            <div class="ingame-num"><?php echo $users['count']; ?></div>
                        </div>
	                    <?php endif; ?>
                    </div>

                    <hr>

                    <div class="row justify-content-center align-item-center row-server-status w-100">
                        <div class="col-md-6 mb-3 mb-md-0">
                            <div>Battle Ground</div>
                            <div class="ingame-num">Thứ 7, Chủ nhật lúc 21h00</div>
                        </div>
                        <div class="col-md-6 mb-3 mb-md-0">
                            <div>World Boss</div>
                            <div class="ingame-num">Thứ 2, 6, Chủ nhật lúc 20h05</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Content Row -->
        <div class="row">
            <div class="col-md-6 mb-4 col-function">
                <div class="card h-100">
                    <div class="card-body">
                        <h2 class="card-title">Hướng dẫn Ragnarok Online</h2>
                        <p class="card-text">Các bài viết hướng dẫn làm nhiệm vụ, cách xây dựng nhận vật, ...</p>
                    </div>
                    <div class="card-footer">
                        <a href="https://hahiu.com/" class="btn btn-primary">Xem ngay</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4 col-function col-socials">
                <div class="card h-100">
                    <div class="card-body">
                        <h2 class="card-title">Kết nối</h2>
                        <p class="card-text">Hãy tham gia kênh Discord của LaniakeaRO để thảo luận và tám các thứ về Ragnarok.</p>
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Discord" target="_blank" href="https://discord.gg/kH2QmvN">
                            <i class="ion-ios-game-controller-a-outline"></i>
                        </a>
                        <a class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Facebook" target="_blank" href="https://www.facebook.com/laniakea.ro/">
                            <i class="ion-social-facebook"></i>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.col-md-4 -->

        </div>
        <!-- /.row -->
    </div>

<?php endwhile; ?>
<?php get_footer(); ?>