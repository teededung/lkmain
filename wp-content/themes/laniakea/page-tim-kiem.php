<?php get_header(); ?>

<div class="container content">
    <div class="row">
        <div class="col-md-8">
            <?php $keyword = '';

            if (isset($_GET['keyword'])):
                $keyword = $_GET['keyword'];
            endif;
            ?>

            <?php if ($keyword): ?>
            <h2 class="page-title">Kết quả tìm kiếm của từ khóa <?php echo $_GET['keyword']; ?></h2>
            <?php endif; ?>

			<?php
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$args = array(
                'post_type' => 'post',
                'category_name'  => 'thong-bao',
				'posts_per_page' => 10,
				'paged' => $paged,
				's' => $keyword,
				'post_status' => 'publish'
			);

			$news = new WP_Query($args);

			if ($news->have_posts()):
				while ($news->have_posts()): $news->the_post();
					get_template_part('/parts/loop/loop-news');
				endwhile;
            else:
                ?>

                <div class="alert alert-warning">Không tìm thấy! Quay lại trang <a href="<?php echo home_url('/tin-tuc/'); ?>">tin tức</a>.</div>

                <?php
			endif;
			?>
        </div>

        <div class="col-md-4">
			<?php get_template_part('/parts/sidebar'); ?>
        </div>
    </div>

</div>

<?php get_footer(); ?>
