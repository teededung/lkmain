<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

	<div class="container content-md">
        <div class="box">
            <h3>Khôi phục mật khẩu</h3>
            <hr>
            <form action="" method="post" id="form-reset-password">
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" id="email" placeholder="Điền địa chỉ Email mà bạn đã đăng ký tài khoản" class="form-control">
                </div>

                <button class="btn btn-primary" type="submit">Gửi</button>
            </form>
        </div>
	</div>

<?php endwhile; ?>
<?php get_footer(); ?>
