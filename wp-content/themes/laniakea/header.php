<!DOCTYPE html>
<html>
<head>
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700&amp;subset=vietnamese" rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics -->
<!--    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114523878-1"></script>-->
<!--    <script>-->
<!--      window.dataLayer = window.dataLayer || [];-->
<!--      function gtag(){dataLayer.push(arguments);}-->
<!--      gtag('js', new Date());-->
<!--      gtag('config', 'UA-114523878-1');-->
<!--    </script>-->

    <?php wp_head(); ?>
	
    <?php $scripts_in_header = get_field('scripts_in_header', 'option'); ?>
    <?php if ($scripts_in_header): ?>
        <script>
	        <?php echo $scripts_in_header; ?>
        </script>
    <?php endif; ?>
	
</head>

<body <?php body_class(); ?>>
    <input type="hidden" id="logout_url" value="<?php echo home_url('/dang-xuat/'); ?>">

    <div class="browsehappy"></div>

    <div id="wrapper">

        <header class="header-page">
            <nav class="navbar navbar-expand-lg navbar-light" role="navigation">
                <div class="container">
                    <a class="navbar-brand" href="<?php echo home_url('/'); ?>">
                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/laniakea-logo.png'; ?>" alt="laniakea-ro-logo">
                    </a>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-toggle" aria-controls="nav-toggle" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="nav-toggle">
	                    <?php
	                    wp_nav_menu( array(
			                    'theme_location'    => 'header-nav',
			                    'depth'             => 2,
			                    'container'         => '',
			                    'container_class'   => '',
			                    'container_id'      => 'header-nav',
			                    'menu_class'        => 'nav navbar-nav mr-auto',
			                    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
			                    'walker'            => new WP_Bootstrap_Navwalker())
	                    );
	                    ?>

                        <span class="navbar-text navbar-btn">
                            <?php
                            $logged = false;
                            if (isset($_COOKIE['laniakeaA'])):
                                $auth = $_COOKIE['laniakeaA'];
                                $account = ragnarok_login_by_auth($auth);
                                if (is_data_return_ok($account)):
                                    $logged = true;
                                endif;
                            endif;
                            ?>
                            <?php if ($logged): ?>
                                <a href="<?php echo home_url('/tai-khoan/'); ?>" class="btn btn-primary">Tài khoản</a>
                                <a @click.prevent="logout" href="<?php echo home_url('/dang-xuat/') ?>" class="btn btn-secondary">Đăng xuất</a>
                            <?php else: ?>
                                <a href="<?php echo home_url('/dang-ky/'); ?>" class="btn btn-primary">Đăng ký</a>
                                <a href="<?php echo home_url('/dang-nhap/'); ?>" class="btn btn-secondary">Đăng nhập</a>
                            <?php endif; ?>
                        </span>
                    </div>
                </div>
            </nav>
        </header>


