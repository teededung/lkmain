<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

	<div class="container content-lg">
		<?php $activation_code = isset($_GET['code']) ? $_GET['code'] : 'Failed'; ?>
        <?php if (!$activation_code) $activation_code = 'Failed'; ?>

		<?php if ($activation_code == 'Failed'): ?>
			<div class="alert alert-danger" role="alert">
				Đường link kích hoạt tài khoản không đúng.
			</div>
		<?php else: ?>
			<?php
			$failed = 0;
			$just_updated = 0;

			$args = array(
				'post_type' => 'registers',
				'post_status' => 'any',
				'posts_per_page' => 1,
				'meta_query' => array(
					array(
						'key'   => 'activation_code',
						'compare' => '=',
						'value' => $activation_code
					)
				)
			);

			$register = new WP_Query($args);

			if ($register->have_posts()):
				while ($register->have_posts()): $register->the_post();
					$post_id = get_the_ID();
					$username = trim(get_the_title());
					$email = get_field('email');

					$result = ragnarok_active_account($username, $email);

					if ($result['status'] == 'OK'):
						update_field('activation_code', '');

					    $args = array(
                            'ID' => $post_id,
                            'post_status' => 'publish'
                        );

					    wp_update_post($args);
					    $just_updated = 1;
					else:
						$failed = 1;
					endif;
				endwhile;
			else:
				?>
				<div class="alert alert-primary" role="alert">
					Bạn đã kích hoạt tài khoản trước đó rồi. Bạn không cần phải kích hoạt lại nữa :3
				</div>
				<?php
			endif;
			?>

			<?php if ($failed): ?>
				<div class="alert alert-danger" role="alert">
					<?php echo $result['message']; ?>
				</div>
			<?php elseif (!$failed && $just_updated): ?>
				<div class="alert alert-success" role="alert">
					Bạn đã kích hoạt tài khoản thành công. Bạn đã có thể đăng nhập vào game ngay bây giờ. Chúc bạn chơi vui vẻ.
				</div>
			<?php endif; ?>
		<?php endif; ?>
	</div>

<?php endwhile; ?>
<?php get_footer(); ?>
