<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

<div class="container content">
    <h1 class="text-center mb-5">Các lệnh hữu ích</h1>

    <div class="mb-5">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Lệnh @</th>
                    <th>Công dụng</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><code>@rates</code></td>
                    <td>Xem thông tin rate của server.</td>
                </tr>
                <tr>
                    <td><code>@request <span>tin_nhắn</span></code></td>
                    <td>Chat với GM nếu GM có online.</td>
                </tr>
                <tr>
                    <td><code>@party <span>tên_nhóm</span></code></td>
                    <td>Tạo tổ đội.</td>
                </tr>
                <tr>
                    <td><code>@refresh</code></td>
                    <td>Làm tươi.</td>
                </tr>
                <tr>
                    <td>
                        <code>@tt</code><br>
                        <code>@tt <span>loại</span></code>
                    </td>
                    <td>
                        Hiện ra đoạn chat thông tin của bạn cho người khác thấy. Các loại:<br>
                        <ul class="mb-3">
                            <li>1: Cấp độ</li>
                            <li>2: ID nhân vật và ID tài khoản</li>
                            <li>3: Tên tổ đội</li>
                            <li>4: Kiểu tóc, màu tóc, màu quần áo</li>
                            <li>5: IP, Unique ID</li>
                        </ul>
                        <strong>Ví dụ:</strong> 1 bạn nào đó muốn biết kiểu tóc và màu tóc của bạn. Bạn hãy gõ <code>@tt 4</code>.
                    </td>
                </tr>
                <tr>
                    <td><code>@whodrops <span>id_vật_phẩm</span></code></td>
                    <td>Liệt kê các quái vật mà rơi vật phẩm có id mà bạn cần tìm.</td>
                </tr>
                <tr>
                    <td>
                        <code>@mobinfo <span>id_quái_vật</span></code><br>
                        <code>@mi <span>id_quái_vật</span></code>
                    </td>
                    <td>Xem thông tin của quái vật và các vật phẩm rơi ra từ quái vật.</td>
                </tr>
                <tr>
                    <td><code>@whereis <span>id_quái_vật</span></code></td>
                    <td>Liệt kê các bản đồ mà có quái vật theo id mà bạn cần tìm.</td>
                </tr>
                <tr>
                    <td><code>@mapmoblist</code></td>
                    <td>Liệt kê các quái vật và số lượng của bản đồ hiện tại.</td>
                </tr>
                <tr>
                    <td><code>@huyins</code></td>
                    <td>Hủy Instance hiện tại. Chỉ có thể sử dụng khi đang ở ngoài Instance.</td>
                </tr>
                <tr>
                    <td>
                        <code>@autotrade</code><br>
                        <code>@at</code>
                    </td>
                    <td>Tự động treo bán hàng, dành cho các nhân vật có kỹ năng Vending.</td>
                </tr>
                <tr>
                    <td><code>@sw</code></td>
                    <td>Chuyển trang bị nhanh.</td>
                </tr>
                <tr>
                    <td><code>@exp</code></td>
                    <td>Xem cấp độ Base/Job và % kinh nghiệm hiện tại của nhân vật.</td>
                </tr>
                <tr>
                    <td><code>@showexp</code></td>
                    <td>Hiện hoặc ẩn kinh nghiệm nhận được khi tiêu diệt quái vật.</td>
                </tr>
                <tr>
                    <td><code>@showzeny</code></td>
                    <td>Hiện hoặc ẩn số zeny nhận được hoặc mất đi khi tiêu diệt quái vật.</td>
                </tr>
                <tr>
                    <td><code>@showdelay</code></td>
                    <td>Hiện hoặc ẩn sử dụng kỹ năng thất bại khi kỹ năng còn thời gian delay.</td>
                </tr>
                <tr>
                    <td>
                        <code>@iteminfo <span>id_vật_phẩm</span></code><br>
                        <code>@ii <span>id_vật_phẩm</span></code>
                    </td>
                    <td>Xem thông tin vật phẩm theo id vật phẩm.</td>
                </tr>
                <tr>
                    <td>
                        <code>@ii2 <span>id_vật_phẩm</span></code>
                    </td>
                    <td>Xem thông tin vật phẩm (kiểu 2) theo id vật phẩm.</td>
                </tr>
                <tr>
                    <td><code>@noask</code></td>
                    <td>Từ chối mọi lời mời vào tổ đội, giao dịch, ...</td>
                </tr>
                <tr>
                    <td><code>@noks <span>loại</span></code></td>
                    <td>
                        Ngăn chặn người chơi khác ks quái vật đang đánh của bạn. Các loại:<br>
                        <ul class="mb-3">
                            <li>self: bản thân</li>
                            <li>party: đối với nhóm</li>
                            <li>guild: đối với bang hội</li>
                        </ul>
                        <strong>Ví dụ:</strong> <code>@noks self</code> sẽ ngăn chặn người chơi khác ks quái của bạn.
                    </td>
                </tr>
                <tr>
                    <td><code>@breakguild</code></td>
                    <td>Giải tán bang hội.</td>
                </tr>
                <tr>
                    <td>
                        <code>@whosell <span>id_vật_phẩm</span></code><br>
                        <code>@ws <span>id_vật_phẩm</span></code><br>
                        <code>@aiban <span>id_vật_phẩm</span></code><br>
                        <code>@mua <span>id_vật_phẩm</span></code>
                    </td>
                    <td>Tìm kiếm vật phẩm đang bán bởi người chơi khác.</td>
                </tr>
                <tr>
                    <td><code>@whosell2</code></td>
                    <td>Giống như <code>@whosell</code> nhưng lệnh này hiển thị giao diện tìm kiếm.</td>
                </tr>
                <tr>
                    <td><code>@alootid +<span>id_vật_phẩm</span></code></td>
                    <td>
                        Thêm vào danh sách tự động nhặt vật phẩm nào đó theo id.<br>
                        <strong>Ví dụ</strong> <code>@alootid +514</code> sẽ thêm Grape vào dánh sách tự động nhặt.
                    </td>
                </tr>
                <tr>
                    <td>
                        <code>@autoloottype +<span>loại</span></code><br>
                        <code>@aloottype +<span>loại</span></code>
                    </td>
                    <td>
                        Tự động nhặt đồ theo loại vật phẩm. Các loại:<br>
                        <ul class="mb-3">
                            <li>0: healing</li>
                            <li>2: usable</li>
                            <li>3: etc</li>
                            <li>4: armor</li>
                            <li>5: weapon</li>
                            <li>6: card</li>
                            <li>7: pet egg</li>
                            <li>8: pet armor</li>
                            <li>10: ammo</li>
                        </ul>
                        <strong>Ví dụ:</strong> <code>@aloottype +6</code> sẽ tự động nhặt tất cả card.
                    </td>
                </tr>
                <tr>
                    <td><code>@autoloot <span>phần_trăm</span></code></td>
                    <td>Tự động nhặt đồ theo tỉ lệ % rơi ra của vật phẩm. Ví dụ <code>@autoloot 1</code> chỉ tự động nhặt các vật phẩm từ 1% hoặc thấp hơn.<br> <code>@autoloot 0</code> để hủy tự động nhặt theo %.</td>
                </tr>
                <tr>
                    <td><code>@nhatdo</code></td>
                    <td>Tính năng quản lý tự động nhặt đồ. Xem hướng dẫn sử dụng lệnh này tại <a target="_blank" href="https://hahiu.com/he-thong-tinh-nang/quan-ly-tu-dong-nhat-do/">đây</a>.</td>
                </tr>
                <tr>
                    <td><code>@time</code></td>
                    <td>Xem giờ hiện tại của máy chủ.</td>
                </tr>
                <tr>
                    <td><code>@uptime</code></td>
                    <td>Xem thời gian mà máy chủ hoạt động từ lần bảo trì cuối.</td>
                </tr>
                <tr>
                    <td><code>@jailtime</code></td>
                    <td>Nếu nhân vật của bạn bị bỏ tù thì lệnh này dùng để xem thời gian kết thúc treo tù.</td>
                </tr>
                <tr>
                    <td>
                        <code>@checkdelay</code><br>
                        <code>@checkcd</code>
                    </td>
                    <td>Dùng để kiểm tra thời gian cần phải chờ để đi tiếp Instance hoặc các nhiệm vụ hằng ngày.</td>
                </tr>
                <tr>
                    <td><code>@daily</code></td>
                    <td>Kiểm tra quà tặng đăng nhập hàng ngày.</td>
                </tr>
                <tr>
                    <td>
                        <code>@treo</code><br>
                        <code>@treomay</code><br>
                        <code>@afk</code>
                    </td>
                    <td>Nhân vật tự động ngồi xuống và có chiếc mũ AFK trên đầu.</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="mb-5">
        <h3>Các lệnh VIP</h3>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Lệnh @</th>
                    <th>Công dụng</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><code>@storage</code></td>
                    <td>Mở rương đồ Kafra từ xa.</td>
                </tr>
                <tr>
                    <td><code>@storage2</code></td>
                    <td>Mở rương đồ VIP từ xa.</td>
                </tr>
                <tr>
                    <td><code>@mount2</code></td>
                    <td>Cưỡi thú hoặc xuống thú.</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="mb-5">
        <h3>Các lệnh Homunculus và Pet</h3>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Lệnh @</th>
                    <th>Công dụng</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><code>@hominfo</code></td>
                    <td>Xem thông tin của Homunculus như chỉ số chính, HP, SP, ATK, MATK, độ đói, độ thân mật.</td>
                </tr>
                <tr>
                    <td><code>@homstats</code></td>
                    <td>Xem chỉ số của Homunculus.</td>
                </tr>
                <tr>
                    <td><code>@petinfo</code></td>
                    <td>Xem mức độ thân mật của thú cưng.</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>
