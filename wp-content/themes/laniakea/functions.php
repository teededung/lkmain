<?php
require_once locate_template('/functions/cleanup.php');
require_once locate_template('/functions/setup.php');
require_once locate_template('/functions/enqueues.php');
require_once locate_template('/functions/navbar.php');
require_once locate_template('/functions/ajax.php');
require_once locate_template('/functions/ragnarok.php');
require_once locate_template('/functions/email-function.php');
require_once locate_template('/functions/cpt.php');
require_once locate_template('/functions/tax.php');
require_once locate_template('functions/class.coupon.php');

include_once locate_template('/functions/tee-functions.php');

/*========== Your custom functions below here ==========*/

date_default_timezone_set ('Asia/Ho_Chi_Minh');

/**
 * simple method to encrypt or decrypt a plain text string
 * initialization vector(IV) has to be the same when encrypting and decrypting
 *
 * @param string $action: can be 'encrypt' or 'decrypt'
 * @param string $string: string to encrypt or decrypt
 *
 * @return string
 */
function encrypt_decrypt($action, $string) {
	$output = false;
	$encrypt_method = OPENSSL_METHOD;
	$secret_key     = OPENSSL_SECRET;
	$secret_iv      = 'laniakea-iv';

	// hash
	$key = hash('sha256', $secret_key);

	// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
	$iv = substr(hash('sha256', $secret_iv), 0, 16);
	if ( $action == 'encrypt' ) {
		$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
		$output = base64_encode($output);
	} else if( $action == 'decrypt' ) {
		$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
	}
	return $output;
}

/**
 * @param $auth
 *
 * @return array
 */
function explode_auth_to_information($auth) {
	$array = array();
	$auth_array = explode('..', $auth);

	if (count($auth_array) == 3):
        $account_id_e = $auth_array[0];
		$username_e = $auth_array[1];
		$password_e = $auth_array[2];

        $account_id = encrypt_decrypt('decrypt', $account_id_e);
		$username = encrypt_decrypt('decrypt', $username_e);
		$password = encrypt_decrypt('decrypt', $password_e);

		$array['status'] = 'OK';
		$array['username'] = $username;
		$array['password'] = $password;
		$array['account_id'] = $account_id;
	else:
		$array['status'] = 'Failed';
	    $array['error'] = 'Explode failed';
	endif;

	return $array;
}

/**
 * @param $data
 * @return bool
 */
function is_data_return_ok($data) {
    return ($data['status'] == 'OK');
}

/**
 * @param $jobId
 * @return string
 */
function get_job_name($jobId) {
	switch (floatval($jobId)) {
		case 0:
			return 'Novice';
		case 1:
			return 'Swordman';
		case 2:
			return 'Magician';
		case 3:
			return 'Archer';
		case 4:
			return 'Acolyte';
		case 5:
			return 'Merchant';
		case 6:
			return 'Thief';
		case 7:
		case 13:
			return 'Knight';
		case 8:
			return 'Priest';
		case 9:
			return 'Wizard';
		case 10:
			return 'Blacksmith';
		case 11:
			return 'Hunter';
		case 12:
			return 'Assassin';
		case 14:
		case 21:
			return 'Crusader';
		case 15:
			return 'Monk';
		case 16:
			return 'Sage';
		case 17:
			return 'Rogue';
		case 18:
			return 'Alchemist';
		case 19:
			return 'Bard';
		case 20:
			return 'Dancer';
		case 22:
			return 'Đám cưới';
		case 23:
			return 'Super Novice';
		case 24:
			return 'Gunslinger';
		case 25:
			return 'Ninja';
		case 4001:
			return 'Novice High';
		case 4002:
			return 'Swordman High';
		case 4003:
			return 'Magician High';
		case 4004:
			return 'Archer High';
		case 4005:
			return 'Acolyte High';
		case 4006:
			return 'Merchant High';
		case 4007:
			return 'Thief High';
		case 4008:
		case 4014:
			return 'Lord Knight';
		case 4009:
			return 'High Priest';
		case 4010:
			return 'High Wizard';
		case 4011:
			return 'Whitesmith';
		case 4012:
			return 'Sniper';
		case 4013:
			return 'Assassin Cross';
		case 4015:
		case 4022:
			return 'Paladin';
		case 4016:
			return 'Champion';
		case 4017:
			return 'Professor';
		case 4018:
			return 'Stalker';
		case 4019:
			return 'Creator';
		case 4020:
			return 'Clown';
		case 4021:
			return 'Gypsy';
		case 4023:
			return 'Baby Novice';
		case 4024:
			return 'Baby Swordman';
		case 4025:
			return 'Baby Magician';
		case 4026:
			return 'Baby Archer';
		case 4027:
			return 'Baby Acolyte';
		case 4028:
			return 'Baby Merchant';
		case 4029:
			return 'Baby Thief';
		case 4030:
		case 4036:
			return 'Baby Knight';
		case 4031:
			return 'Baby Priest';
		case 4032:
			return 'Baby Wizard';
		case 4033:
			return 'Baby Blacksmith';
		case 4034:
			return 'Baby Hunter';
		case 4035:
			return 'Baby Assassin';
		case 4037:
		case 4044:
			return 'Baby Crusader';
		case 4038:
			return 'Baby Monk';
		case 4039:
			return 'Baby Sage';
		case 4040:
			return 'Baby Rogue';
		case 4041:
			return 'Baby Alchemist';
		case 4042:
			return 'Baby Bard';
		case 4043:
			return 'Baby Dancer';
		case 4045:
			return 'Baby Super Novice';
		case 4046:
			return 'Taekwon';
		case 4047:
			return 'Star Gladiator';
		case 4048:
			return 'Star Gladiator (Union)';
		case 4049:
			return 'Soul Linker';
		case 4050:
			return 'Gangsi (Bongun/Munak)';
		case 4051:
			return 'Death Knight';
		case 4052:
			return 'Dark Collector';
		case 4053:
		case 4080:
			return 'Rune Knight (Regular)';
		case 4055:
			return 'Warlock (Regular)';
		case 4056:
		case 4084:
			return 'Ranger (Regular)';
		case 4057:
			return 'Arch Bishop (Regular)';
		case 4058:
		case 4086:
			return 'Mechanic (Regular)';
		case 4059:
			return 'Guillotine Cross (Regular)';
		case 4060:
		case 4081:
			return 'Rune Knight (Trans)';
		case 4061:
			return 'Warlock (Trans)';
		case 4062:
		case 4085:
			return 'Ranger (Trans)';
		case 4063:
			return 'Arch Bishop (Trans)';
		case 4064:
		case 4087:
			return 'Mechanic (Trans)';
		case 4065:
			return 'Guillotine Cross (Trans)';
		case 4066:
		case 4082:
			return 'Royal Guard (Regular)';
		case 4067:
			return 'Sorcerer (Regular)';
		case 4068:
			return 'Minstrel (Regular)';
		case 4069:
			return 'Wanderer (Regular)';
		case 4070:
			return 'Sura (Regular)';
		case 4071:
			return 'Genetic (Regular)';
		case 4072:
			return 'Shadow Chaser (Regular)';
		case 4073:
		case 4083:
			return 'Royal Guard (Trans)';
		case 4074:
			return 'Sorcerer (Trans)';
		case 4075:
			return 'Minstrel (Trans)';
		case 4076:
			return 'Wanderer (Trans)';
		case 4077:
			return 'Sura (Trans)';
		case 4078:
			return 'Genetic (Trans)';
		case 4079:
			return 'Shadow Chaser (Trans)';
		case 4096:
		case 4109:
			return 'Baby Rune Knight';
		case 4097:
			return 'Baby Warlock';
		case 4098:
		case 4111:
			return 'Baby Ranger';
		case 4099:
			return 'Baby Arch Bishop';
		case 4100:
		case 4112:
			return 'Baby Mechanic';
		case 4101:
			return 'Baby Guillotine Cross';
		case 4102:
		case 4110:
			return 'Baby Royal Guard';
		case 4103:
			return 'Baby Sorcerer';
		case 4104:
			return 'Baby Minstrel';
		case 4105:
			return 'Baby Wanderer';
		case 4106:
			return 'Baby Sura';
		case 4107:
			return 'Baby Genetic';
		case 4108:
			return 'Baby Shadow Chaser';
		case 4190:
			return 'Super Novice (Expanded)';
		case 4191:
			return 'Super Baby (Expanded)';
		case 4211:
			return 'Kagerou';
		case 4212:
			return 'Oboro';
		case 4215:
			return 'Rebellion';
		case 4218:
			return 'Summoner';
		case 4220:
			return 'Baby Summoner';
		case 4222:
			return 'Baby Ninja';
		case 4223:
			return 'Baby Kagerou';
		case 4224:
			return 'Baby Oboro';
		case 4225:
			return 'Baby Taekwon';
		case 4226:
			return 'Baby Star Gladiator';
		case 4227:
			return 'Baby Soul Linker';
		case 4228:
			return 'Baby Gunslinger';
		case 4229:
			return 'Baby Rebellion';
		case 4238:
			return 'Baby Star Gladiator (Union)';
        case 4239:
            return 'Star Emperor';
        case 4240:
            return 'Soul Reaper';
        case 4241:
            return 'Baby Star Emperor';
        case 4242:
            return 'Baby Soul Reaper';
	}
}

/**
 * @param $state_id
 * @return string
 */
function get_account_state_name($state_id) {
    switch ($state_id) {
        case 0:
            return 'Bình thường';
        case 1:
            return 'Chưa kích hoạt';
        case 3:
            return 'Đã hết hạn';
        default:
            return 'Bị khóa';
    }
}

/**
 * @return int
 */
function update_registers_database() {
    if ( ! function_exists( 'post_exists' ) ) {
        require_once( ABSPATH . 'wp-admin/includes/post.php' );
    }

    $ragnarok_db = connect_ragnarok_db();
    $users = $ragnarok_db['db']->get_results('SELECT `account_id`, `userid`, `sex`, `email` FROM `login`');
    $count = 0;

    if ($users):
        foreach ($users as $user):
            if (!post_exists($user->userid, '', '', 'registers')):
                echo "Đã thêm userid " . $user->userid . "<br>";
                $count++;
                $pos = array(
                    'post_title' => $user->userid,
                    'post_type' => 'registers',
                    'post_status' => 'publish'
                );

                $post_id = wp_insert_post($pos);

                if ($post_id > 0):
                    //$array['post_id'] = $post_id;
                    $array['status'] = 'OK';

                    // Update Account ID Field
                    update_field('account_id', $user->account_id, $post_id);

                    // Update Email Field
                    update_field('email', $user->email, $post_id);

                    // Update Gender Field
                    update_field('gender', $user->sex == 'M' ? 'male' : 'female', $post_id);
                else:
                    echo "Failed " . $user->userid;
                endif;
            endif;
        endforeach;
        echo "----------";
        echo "Đã thêm " . $count  . " vào cơ sở dữ liệu.";
    endif;

    return $count;
}


/**
 * @param $filename
 * @param $image_data
 * @param $post_id
 * @param $meta_data
 *
 * @return bool|int|WP_Error
 */
function insert_image_to_post($filename, $image_data, $post_id, $meta_data) {
    $wp_upload_dir      = wp_upload_dir();
    $upload_file        = $wp_upload_dir['basedir'] . '/user-images/' . $filename;

    if (move_uploaded_file($image_data, $upload_file)):
        require_once(ABSPATH . "wp-admin" . '/includes/image.php');
        require_once(ABSPATH . "wp-admin" . '/includes/file.php');
        require_once(ABSPATH . "wp-admin" . '/includes/media.php');

        $wp_filetype = wp_check_filetype($filename, null);

        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title'     => $meta_data['title'],
            'post_excerpt'	 => $meta_data['caption'],
            'post_content'   => $meta_data['description'],
            'post_status'    => 'inherit'
        );

        // Insert attachment
        $attach_id = wp_insert_attachment($attachment, $upload_file, $post_id);

        $attach_data = wp_generate_attachment_metadata($attach_id, $upload_file);
        wp_update_attachment_metadata($attach_id, $attach_data);

        // Set the image Alt-Text
        update_post_meta($attach_id, '_wp_attachment_image_alt', $meta_data['alt']);

        if (function_exists('wp_rml_move')):
            wp_rml_move(1, array($attach_id));
        endif;

        return $attach_id;
    endif;
}

/**
 * @param $mime
 *
 * @return bool|int|string
 */
function mime2ext($mime){
    $all_mimes = '{"png":["image\/png","image\/x-png"],"bmp":["image\/bmp","image\/x-bmp","image\/x-bitmap","image\/x-xbitmap","image\/x-win-bitmap","image\/x-windows-bmp","image\/ms-bmp","image\/x-ms-bmp","application\/bmp","application\/x-bmp","application\/x-win-bitmap"],"gif":["image\/gif"],"jpeg":["image\/jpeg","image\/pjpeg"],"xspf":["application\/xspf+xml"],"vlc":["application\/videolan"],"wmv":["video\/x-ms-wmv","video\/x-ms-asf"],"au":["audio\/x-au"],"ac3":["audio\/ac3"],"flac":["audio\/x-flac"],"ogg":["audio\/ogg","video\/ogg","application\/ogg"],"kmz":["application\/vnd.google-earth.kmz"],"kml":["application\/vnd.google-earth.kml+xml"],"rtx":["text\/richtext"],"rtf":["text\/rtf"],"jar":["application\/java-archive","application\/x-java-application","application\/x-jar"],"zip":["application\/x-zip","application\/zip","application\/x-zip-compressed","application\/s-compressed","multipart\/x-zip"],"7zip":["application\/x-compressed"],"xml":["application\/xml","text\/xml"],"svg":["image\/svg+xml"],"3g2":["video\/3gpp2"],"3gp":["video\/3gp","video\/3gpp"],"mp4":["video\/mp4"],"m4a":["audio\/x-m4a"],"f4v":["video\/x-f4v"],"flv":["video\/x-flv"],"webm":["video\/webm"],"aac":["audio\/x-acc"],"m4u":["application\/vnd.mpegurl"],"pdf":["application\/pdf","application\/octet-stream"],"pptx":["application\/vnd.openxmlformats-officedocument.presentationml.presentation"],"ppt":["application\/powerpoint","application\/vnd.ms-powerpoint","application\/vnd.ms-office","application\/msword"],"docx":["application\/vnd.openxmlformats-officedocument.wordprocessingml.document"],"xlsx":["application\/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application\/vnd.ms-excel"],"xl":["application\/excel"],"xls":["application\/msexcel","application\/x-msexcel","application\/x-ms-excel","application\/x-excel","application\/x-dos_ms_excel","application\/xls","application\/x-xls"],"xsl":["text\/xsl"],"mpeg":["video\/mpeg"],"mov":["video\/quicktime"],"avi":["video\/x-msvideo","video\/msvideo","video\/avi","application\/x-troff-msvideo"],"movie":["video\/x-sgi-movie"],"log":["text\/x-log"],"txt":["text\/plain"],"css":["text\/css"],"html":["text\/html"],"wav":["audio\/x-wav","audio\/wave","audio\/wav"],"xhtml":["application\/xhtml+xml"],"tar":["application\/x-tar"],"tgz":["application\/x-gzip-compressed"],"psd":["application\/x-photoshop","image\/vnd.adobe.photoshop"],"exe":["application\/x-msdownload"],"js":["application\/x-javascript"],"mp3":["audio\/mpeg","audio\/mpg","audio\/mpeg3","audio\/mp3"],"rar":["application\/x-rar","application\/rar","application\/x-rar-compressed"],"gzip":["application\/x-gzip"],"hqx":["application\/mac-binhex40","application\/mac-binhex","application\/x-binhex40","application\/x-mac-binhex40"],"cpt":["application\/mac-compactpro"],"bin":["application\/macbinary","application\/mac-binary","application\/x-binary","application\/x-macbinary"],"oda":["application\/oda"],"ai":["application\/postscript"],"smil":["application\/smil"],"mif":["application\/vnd.mif"],"wbxml":["application\/wbxml"],"wmlc":["application\/wmlc"],"dcr":["application\/x-director"],"dvi":["application\/x-dvi"],"gtar":["application\/x-gtar"],"php":["application\/x-httpd-php","application\/php","application\/x-php","text\/php","text\/x-php","application\/x-httpd-php-source"],"swf":["application\/x-shockwave-flash"],"sit":["application\/x-stuffit"],"z":["application\/x-compress"],"mid":["audio\/midi"],"aif":["audio\/x-aiff","audio\/aiff"],"ram":["audio\/x-pn-realaudio"],"rpm":["audio\/x-pn-realaudio-plugin"],"ra":["audio\/x-realaudio"],"rv":["video\/vnd.rn-realvideo"],"jp2":["image\/jp2","video\/mj2","image\/jpx","image\/jpm"],"tiff":["image\/tiff"],"eml":["message\/rfc822"],"pem":["application\/x-x509-user-cert","application\/x-pem-file"],"p10":["application\/x-pkcs10","application\/pkcs10"],"p12":["application\/x-pkcs12"],"p7a":["application\/x-pkcs7-signature"],"p7c":["application\/pkcs7-mime","application\/x-pkcs7-mime"],"p7r":["application\/x-pkcs7-certreqresp"],"p7s":["application\/pkcs7-signature"],"crt":["application\/x-x509-ca-cert","application\/pkix-cert"],"crl":["application\/pkix-crl","application\/pkcs-crl"],"pgp":["application\/pgp"],"gpg":["application\/gpg-keys"],"rsa":["application\/x-pkcs7"],"ics":["text\/calendar"],"zsh":["text\/x-scriptzsh"],"cdr":["application\/cdr","application\/coreldraw","application\/x-cdr","application\/x-coreldraw","image\/cdr","image\/x-cdr","zz-application\/zz-winassoc-cdr"],"wma":["audio\/x-ms-wma"],"vcf":["text\/x-vcard"],"srt":["text\/srt"],"vtt":["text\/vtt"],"ico":["image\/x-icon","image\/x-ico","image\/vnd.microsoft.icon"],"csv":["text\/x-comma-separated-values","text\/comma-separated-values","application\/vnd.msexcel"],"json":["application\/json","text\/json"]}';
    $all_mimes = json_decode($all_mimes, true);
    foreach ($all_mimes as $key => $value) {
        if(array_search($mime,$value) !== false) return $key;
    }
    return false;
}

/**
 *
 */
function auto_generate_featured_image() {
    $args = [
        'post_type' => 'albums',
        'post_status' => 'pending',
        'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'album_category',
                'field'    => 'slug',
                'terms'    => 'happy-new-year-2020',
            ),
        ),
    ];

    $albums = new WP_Query($args);

    if ($albums->have_posts()):
        while ($albums->have_posts()): $albums->the_post();
            if (has_post_thumbnail()):
                delete_post_meta(get_the_ID(), '_thumbnail_id');
            endif;

            $gallery = get_field('gallery');
            $num_image = count($gallery);
            $index_random = rand(0, $num_image-1);

            if ($gallery):
               set_post_thumbnail(get_the_ID(), $gallery[$index_random]['ID']);
            endif;
        endwhile;
    endif;
}

function item_icon($id) {
    ob_start();?><img class="item-icon" src="https://api.hahiu.com/data/items/icons/<?php echo $id ?>.png"><?php echo ob_get_clean();
    return;
}