<?php get_header(); ?>

<div class="container content">
	<?php while(have_posts()): the_post(); ?>
	<div class="row">
        <div class="col-md-8">
            <h1><?php the_title(); ?></h1>

            <div class="share">
                <span>Chia sẻ qua</span>
                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_url(get_the_permalink()); ?>" data-toggle="tooltip" data-placement="top" title="Chia sẻ qua Facebook" class="social share-facebook">
                    <i class="ion-social-facebook"></i>
                </a>
                <a target="_blank" href="https://twitter.com/home?status=<?php echo esc_url(get_the_permalink()); ?>" data-toggle="tooltip" data-placement="top" title="Chia sẻ qua Twitter" class="social share-twitter">
                    <i class="ion-social-twitter"></i>
                </a>
            </div>

            <div class="content-post">
                <?php the_content(); ?>
            </div>

            <?php if (get_the_tags()): ?>
            <div class="tags">
                <?php $tags = get_the_tags(); ?>
                <?php if ($tags): ?>
                    <span>Thẻ:</span>
                    <?php foreach ($tags as $tag): ?>
                        <a href="<?php echo get_tag_link($tag); ?>" class="badge badge-light"><?php echo $tag->name; ?></a>
                    <?php endforeach; ?>
                <?php endif; ?>

            </div>
            <?php endif; ?>
        </div>
		<div class="col-md-4">
			<?php get_template_part('/parts/sidebar'); ?>
		</div>
	</div>
	<?php endwhile; ?>
</div><!-- /.container -->

<?php get_footer(); ?>
