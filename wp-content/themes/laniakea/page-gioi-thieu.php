<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

    <div class="w-banner">
		<?php echo do_shortcode('[rev_slider alias="home"]'); ?>
    </div>

	<?php get_template_part('/parts/gioi-thieu/1-present'); ?>

	<?php get_template_part('/parts/gioi-thieu/2-section'); ?>

	<?php get_template_part('/parts/gioi-thieu/3-section'); ?>

	<?php get_template_part('/parts/gioi-thieu/4-section'); ?>

	<?php get_template_part('/parts/gioi-thieu/5-quan-ly-nhat-do'); ?>

	<?php get_template_part('/parts/gioi-thieu/6-instances'); ?>

	<?php get_template_part('/parts/gioi-thieu/7-refining'); ?>

	<?php get_template_part('/parts/gioi-thieu/8-pet-evolution'); ?>

    <div id="congrats">
        <h3 id="title">Chúc mừng!</h3>
    </div>

<?php endwhile; ?>
<?php get_footer(); ?>
