<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>

	<div class="container content-sm" id="album-can-vote">
        <?php
        global $wp_query;
        $slug = '';
        if (isset($wp_query->query_vars['album_category'])):
            $slug = $wp_query->query_vars['album_category'];
        endif;
        ?>

        <?php if ($slug != ''): ?>
            <?php $term = get_term_by('slug', $slug, 'album_category'); ?>

            <?php if ($term): ?>
                <nav aria-label="breadcrumb" class="mb-5">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo home_url('/') ?>">Trang chủ</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo home_url('/album/') ?>">Album</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo $term->name; ?></li>
                    </ol>
                </nav>

                <h1 class="mb-5"><?php echo $term->name; ?></h1>

                <?php $allow_vote = get_field('allow_vote', $term); ?>
                <?php $can_vote = false; ?>

                <?php if ($allow_vote): ?>
                <?php
                    $start_time_str = get_field('vote_start_time', $term);
                    $end_time_str   = get_field('vote_end_time', $term);

                    if ($end_time_str == "" || ($start_time_str == "" && $end_time_str == "")):
                        $can_vote = true;
                        $vote_message = "đang diễn ra.";
                    else:
                        $start_time = strtotime($start_time_str);
                        $end_time = strtotime($end_time_str);

                        $now = time();

                        if ($now > $start_time):
                            if ($now < $end_time):
                                $can_vote = true;
                                $vote_message = "đang diễn ra.";
                            else:
                                $vote_message = "đã kết thúc vào ngày " . date("d/m/Y", $end_time)  . '.';
                            endif;
                        else:
                            $vote_message = "sẽ bắt đầu vào ngày <strong>" . date("d/m/Y", $start_time) . "</strong> lúc <strong>" . date('H:i', $start_time) . "</strong> (" . human_time_diff( current_time( 'timestamp' ,7), $start_time ) . " nữa).";
                        endif;
                    endif;
                endif;
                ?>

                <?php if ($allow_vote): ?>
                <div class="alert alert-info mb-3">
                    Sự kiện bình chọn album <strong><?php the_field('event_name', $term); ?></strong>&nbsp;<?php echo $vote_message ?><br>
                    <?php if ($can_vote && $end_time_str != ""): ?>
                        Thời gian kết thúc bình chọn: <?php echo date("d/m/Y", $end_time); ?> lúc <?php echo date("H:i", $end_time) ?> (<?php echo human_time_diff( current_time( 'timestamp' ,7), $end_time ) ?> nữa).
                    <?php endif; ?>
                </div>
                <?php endif; ?>

                <?php
                $args = [
                    'post_type' => 'albums',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'album_category',
                            'field'    => 'slug',
                            'terms'    => $term->slug,
                        ),
                    ),
                ];

                $albums = new WP_Query($args);

                if ($albums->have_posts()): ?>
                    <div class="cbp" id="grid-container">
                        <?php while($albums->have_posts()): $albums->the_post(); ?>
                            <?php include locate_template('/parts/loop/loop-album.php'); ?>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
            <?php else: ?>
                <div class="alert alert-warning">
                    Không tìm thấy danh mục.
                </div>
            <?php endif; ?>
        <?php else: ?>
            <?php
            $args = [
                'taxonomy'  => 'album_category',
                'hide_empty' => false,
                'order'     => 'name',
                'order_by'  => 'ASC'
            ];

            $terms = get_terms($args);
            ?>

            <?php if ($terms): ?>
                <h1 class="mb-5">Album</h1>

                <div class="cbp" id="grid-container">
                    <?php foreach ($terms as $term): ?>
                        <div class="cbp-item">
                            <a href="<?php echo home_url('/album/' . $term->slug) ?>">
                                <?php $image = get_field('category_image', $term); ?>
                                <?php if ($image): ?>
                                    <img class="img-thumbnail" src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>">
                                <?php endif; ?>
                                <h3 class="post-album-title">
                                    <?php echo $term->name; ?>
                                </h3>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
	</div>

<?php endwhile; ?>
<?php get_footer(); ?>
