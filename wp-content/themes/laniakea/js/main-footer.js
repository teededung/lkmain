"use strict";

(function($) {
  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

  const justLoggedIn = Cookies.get('l_login');

  if (typeof justLoggedIn !== 'undefined') {
    Cookies.remove('l_login');

    setTimeout(function() {
      toastr["success"]("Bạn vừa đăng nhập thành công :3");
    }, 1000);
  }

  $('[data-toggle="tooltip"]').tooltip();
})(jQuery);