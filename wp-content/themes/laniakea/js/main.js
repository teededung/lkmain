function isIE() {
  const ua = window.navigator.userAgent; //Check the userAgent property of the window.navigator object
  const msie = ua.indexOf('MSIE '); // IE 10 or older
  const trident = ua.indexOf('Trident/'); //IE 11

  return (msie > 0 || trident > 0);
}

let showBrowserAlert = (function () {
  if (document.querySelector('.browsehappy')) {
    var d = document.getElementsByClassName('browsehappy');

    if (isIE()) {
      d[0].innerHTML = '<span><strong>Bạn đang sử dụng trình duyệt cũ, để xem trang không bị lỗi bạn hãy nâng cấp trình duyệt lên Chrome, Edge, hoặc Firefox. Tải tại <a rel="nofollow" href="http://browsehappy.com/">đây</a>.</span></strong>';
      d[0].style.display = 'block';
    }
  }
});

document.addEventListener('DOMContentLoaded', showBrowserAlert);

(function($) {
	new Vue({
		name: 'Header Buttons',
		el: '.navbar-btn',
    methods: {
      logout: function() {
        Cookies.remove('laniakeaA');
        location.reload();
      }
	  }
	});
})(jQuery);







