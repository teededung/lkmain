(function($) {

  $('#grid-container').cubeportfolio({
    filters: '#js-filters-masonry',
    layoutMode: 'grid',
    defaultFilter: '*',
    animationType: 'slideDelay',
    gapHorizontal: 5,
    gapVertical: 5,
    gridAdjustment: 'responsive',
    mediaQueries: [{
      width: 1500,
      cols: 5,
    }, {
      width: 1100,
      cols: 4,
    }, {
      width: 800,
      cols: 3,
    }, {
      window: 480,
      cols: 2
    }],
    caption: 'overlayBottomAlong',
    displayType: 'sequentially',
    displayTypeSpeed: 100,

    // lightbox
    lightboxDelegate: '.cbp-lightbox',
    lightboxGallery: true,
    lightboxTitleSrc: 'data-title',
    lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} của {{total}}</div>',

    singlePageDelegate: null
  });

  /*--------------------------*/

  const controller = new ScrollMagic.Controller();

  const tl = new TimelineMax();
  const tl4 = new TimelineMax();
  const tl3 = new TimelineMax();

  tl4
    .from('#soul-reaper', 2.5, { autoAlpha: 0, x: -100, ease: Power3.easeOut })
    .from('#star-emperor', 2.5, { autoAlpha: 0, x: 100, ease: Power3.easeOut }, 0.5)
    .from('#new-job-1', 1, { autoAlpha: 0, x: -100, ease: Power3.easeOut }, 1)
    .from('#new-job-2', 1, { autoAlpha: 0, x: 100, ease: Power3.easeOut }, 1.2)
    .from('#content-soul-reaper-star-emperor', 1, { autoAlpha: 0, y: 100, ease: Power3.easeOut }, 1.5);

  new ScrollMagic.Scene({
    triggerElement: '#section-3 .row-job-1',
    triggerHook: 0.5,
    reverse: false
  })
  // .addIndicators({
  //   name: 'Doram',
  //   colorTrigger: '#009c00',
  //   colorStart: '#009c00'
  // })
    .setTween(tl4)
    .addTo(controller);

  tl
    .from('#doram-2', 2.5, { autoAlpha: 0, x: -100, ease: Power3.easeOut })
    .from('#doram-1', 2.5, { autoAlpha: 0, x: 100, ease: Power3.easeOut }, 0.5)
    .from('#doram-3', 1, { autoAlpha: 0, x: -100, ease: Power3.easeOut }, 1)
    .from('#doram-4', 1, { autoAlpha: 0, x: 100, ease: Power3.easeOut }, 1.2)
    .from('#doram-5', 1, { autoAlpha: 0, y: 100, ease: Power3.easeOut }, 1.5);

  new ScrollMagic.Scene({
    triggerElement: '#section-3 .row-job-2',
    triggerHook: 0.5,
    reverse: false
  })
  // .addIndicators({
  //   name: 'Doram',
  //   colorTrigger: '#009c00',
  //   colorStart: '#009c00'
  // })
      .setTween(tl)
      .addTo(controller);

  /*--------------------------*/

  tl3
  .from('#daily-26', 1.5, {
    autoAlpha: 0,
    y: 50,
    x: 50,
    rotation: '30deg',
    ease: Power3.easeOut
  })
  .from(
      '#daily-27',
      1.5,
      { autoAlpha: 0, y: 50, x: 50, rotation: '30deg', ease: Power3.easeOut },
      0.5
  )
  .from(
      '#daily-28',
      1.5,
      { autoAlpha: 0, y: 50, x: 50, rotation: '30deg', ease: Power3.easeOut },
      1
  )
  .from(
      '.help-text-daily',
      1,
      { autoAlpha: 0, y: 100, ease: Power3.easeOut,},
      1
  );

  new ScrollMagic.Scene({
    triggerElement: '#section-5',
    triggerHook: 0.1,
    reverse: false
  })
  // .addIndicators({
  //   name: 'Daily Reward',
  //   colorTrigger: '#009c00',
  //   colorStart: '#009c00'
  // })
      .setTween(tl3)
      .addTo(controller);

  /*--------------------------*/

  $('.owl-carousel').owlCarousel({
    loop: true,
    margin: 15,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1
      },
      556: {
        items: 3
      },
      767: {
        items: 4
      },
      1000: {
        items: 5
      }
    },
    nav: true,
    navText: ['<i class="ion-chevron-left"></i>', '<i class="ion-chevron-right"></i>']
  });

  /*--------------------------*/

  /**
   * Refine UI
   */
  new Vue({
    name: 'Refine UI',
    el: '.refine-section',
    data: {
      loaded: false,
      refineSourceUrl: $('#refine-source-url').val(),
      items: [
        {
          id: 2301,
          name: 'Cotton Shirt',
          isChoose: true,
          type: 'armor',
          currentRefinedLvl: 0
        },
        {
          id: 15150,
          name: 'White Shirt',
          isChoose: true,
          type: 'armor',
          currentRefinedLvl: 0
        },
        {
          id: 985,
          name: 'Elunium',
          isChoose: false,
          type: 'misc'
        }
      ],
      speed: 90,
      refineSettings: {
        1: 100,
        2: 100,
        3: 100,
        4: 100,
        5: 60,
        6: 40,
        7: 30,
        8: 20,
        9: 20,
        10: 9,
        11: 9,
        12: 9
      },
      isRefining: false,
      showEQName: false,
      showPercent: true,
      showFailed: false,
      percent: 0,
      status: 'init',
      btnStatus: 'init',
      currentSelectedEQID: '',
      currentSelectedEQName: '',
      currentSelectedMaterialID: '',
      currentSelectedMaterialName: '',
      hideMainSlot: false,
      mainRefineImages: {
        init: 'bg_refining.bmp',
        on: 'bg_refining_on.bmp',
        running1A: 'refine-original/bg_refiningA_00.bmp',
        running2A: 'refine-original/bg_refiningA_01.bmp',
        running3A: 'refine-original/bg_refiningA_02.bmp',
        running4A: 'refine-original/bg_refiningA_03.bmp',
        running5A: 'refine-original/bg_refiningA_04.bmp',
        running6A: 'refine-original/bg_refiningA_05.bmp',
        running7A: 'refine-original/bg_refiningA_05.bmp',
        running8A: 'refine-original/bg_refiningA_07.bmp',
        running9A: 'refine-original/bg_refiningA_08.bmp',
        running10A: 'refine-original/bg_refiningA_09.bmp',
        running11A: 'refine-original/bg_refiningA_10.bmp',
        running12A: 'refine-original/bg_refiningA_11.bmp',
        running13A: 'refine-original/bg_refiningA_12.bmp',
        running14A: 'refine-original/bg_refiningA_13.bmp',
        running15A: 'refine-original/bg_refiningA_14.bmp',
        success1: 'refine-success/bg_refiningA_success00.bmp',
        success2: 'refine-success/bg_refiningA_success01.bmp',
        success3: 'refine-success/bg_refiningA_success02.bmp',
        success4: 'refine-success/bg_refiningA_success03.bmp',
        fail1None: 'refine-failed/bg_refiningA_fail00_none.bmp',
        fail2None: 'refine-failed/bg_refiningA_fail01_none.bmp',
        fail3None: 'refine-failed/bg_refiningA_fail02_none.bmp',
        fail4None: 'refine-failed/bg_refiningA_fail03_none.bmp',
        fail5None: 'refine-failed/bg_refiningA_fail04_none.bmp',
        fail6None: 'refine-failed/bg_refiningA_fail05_none.bmp',
        fail7None: 'refine-failed/bg_refiningA_fail06_none.bmp',
        fail8None: 'refine-failed/bg_refiningA_fail07_none.bmp'
      },
      mainRefineButtons: {
        init: 'bt_refining_off.bmp',
        on: 'bt_refining.bmp',
        back: 'bt_refining_back.bmp'
      },
      elu: 100
    },
    methods: {
      selectEquip(equipName, id) {
        if (id === 985) return;
        if (this.showFailed) return;
        if (this.status !== 'init' && this.status !== 'on') return;

        this.status = 'on';
        this.hideMainSlot = false;
        this.showEQName = true;
        this.currentSelectedEQName = equipName;
        this.currentSelectedEQID = id;
      },
      selectMaterial(id) {
        if (this.status === 'init' || this.status === 'on') {
          this.currentSelectedMaterialID = id;
          this.currentSelectedMaterialName = this.getMaterialNameById(id);

          const currentRefinedLvl = this.getCurrentRefined();
          this.percent = this.getNextRefineSuccessPercent(currentRefinedLvl);
          this.btnStatus = 'on';
          this.status = 'on';
        }
      },
      getMaterialNameById(id) {
        const index = _.findIndex(this.items, item => {
          return item.id === id;
        });

        if (index > -1) {
          return this.items[index]['name'];
        }

        return '';
      },
      getCurrentRefined(isText = false) {
        const _this = this;
        const index = _.findIndex(_this.items, item => {
          return item.id === _this.currentSelectedEQID;
        });

        if (index > -1) {
          const refineLvl = _this.items[index]['currentRefinedLvl'];

          if (isText) {
            return refineLvl === 0 ? '' : `+${refineLvl} `;
          }

          return refineLvl;
        }
      },
      getNextRefineSuccessPercent(currentRefine) {
        return this.refineSettings[currentRefine + 1];
      },
      getCurrentMaterialImage() {
        const materialID = this.currentSelectedMaterialID;
        if (materialID) {
          return this.refineSourceUrl + materialID + '.png';
        }
      },
      getEluQuantity() {
        if (this.elu > 0) {
          return this.elu;
        }
      },
      setRefineLvl() {
        const index = _.findIndex(this.items, item => {
          return item.id === this.currentSelectedEQID;
        });
        if (index > -1) {
          this.items[index]['currentRefinedLvl']++;
        }
      },
      refine() {
        if (this.isRefining) return;
        if (this.status === 'init') return;

        if (this.status !== 'on') {
          this.status = this.showFailed ? 'init' : 'on';
          this.btnStatus = 'init';
          this.showFailed = false;

          if (this.elu === 0) return;
        } else {
          if (this.currentSelectedEQID && this.currentSelectedMaterialID) {
            this.isRefining = true;
            this.elu--;
            const currentRefine = this.getCurrentRefined();
            const currentPercent = this.getNextRefineSuccessPercent(
                currentRefine
            );
            const random = getRandomInt(0, 100);

            if (random <= currentPercent) {
              // Success
              this.setRefineLvl();
              this.animateSuccess();
            } else {
              // Failed
              this.animateFail();
            }
          }
        }
      },
      animateSuccess() {
        const status = [
          'running1A',
          'running2A',
          'running3A',
          'running4A',
          'running5A',
          'running6A',
          'running7A',
          'running8A',
          'running9A',
          'running10A',
          'running11A',
          'running12A',
          'running13A',
          'running14A',
          'success1',
          'success2',
          'success3',
          'success4'
        ];

        this.showEQName = false;
        this.animate(status, 'success');
      },
      animateFail() {
        const status = [
          'running1A',
          'running2A',
          'running3A',
          'running4A',
          'running5A',
          'running6A',
          'running7A',
          'running8A',
          'running9A',
          'running10A',
          'running11A',
          'running12A',
          'running13A',
          'running14A',
          'fail1None',
          'fail2None',
          'fail3None',
          'fail4None',
          'fail5None',
          'fail6None',
          'fail7None',
          'fail8None'
        ];

        this.showEQName = false;
        this.animate(status, 'failed');
      },
      animate(status, type) {
        const max = status.length;
        const _this = this;
        let i = 0;
        let eqID = this.currentSelectedEQID;

        const interval = setInterval(function() {
          _this.status = status[i];

          i++;

          _this.showPercent = !(i > 2 && i < max);

          if (i === 11) {
            _this.hideMainSlot = true;

            if (type === 'failed') {
              _this.currentSelectedEQID = '';
              _this.currentSelectedEQName = '';
            }
          }

          if (i === max - 2) {
            _this.currentSelectedMaterialID = '';
            _this.btnStatus = 'back';

            if (type === 'success') {
              _this.hideMainSlot = false;
              _this.showEQName = true;
            } else {
              _this.showEQName = false;
            }
          }

          if (i === max) {
            clearInterval(interval);
            _this.isRefining = false;

            if (type === 'failed') {
              _this.showFailed = true;

              _.forEach(_this.items, (item, i) => {
                if (item.id === eqID) {
                  return (_this.items[i]['currentRefinedLvl'] = 0);
                }
              });
            } else if (type === 'success') {
              if (_this.getCurrentRefined() > 6) {
                init({
                  numberOfStars: 20,
                  numberOfBlobs: 10
                });
                fancyPopIn();
              }
            }
          }
        }, this.speed);
      }
    },
    computed: {
      currentRefineImg() {
        return this.refineSourceUrl + this.mainRefineImages[`${this.status}`];
      },
      currentRefineBtn() {
        return (
            this.refineSourceUrl +
            'refine-btn/' +
            this.mainRefineButtons[`${this.btnStatus}`]
        );
      }
    },
    watch: {
      currentSelectedEQName() {
        this.currentSelectedMaterialID = '';
        this.currentSelectedMaterialName = '';
        if (!this.showFailed) {
          this.btnStatus = 'init';
        }
        this.percent = 0;
      },
      elu() {
        if (this.elu === 0) {
          _.remove(this.items, (item, i) => {
            return item.id === 985;
          });

          this.currentSelectedMaterialID = '';
          this.currentSelectedMaterialName = '';
        }
      }
    },
    mounted() {
      this.loaded = true;
    }
  });

  function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  let particles = ['.blob', '.star'],
      $congratsSection = $('#congrats'),
      $title = $('#title');

  function fancyPopIn() {
    reset();
    animateText();

    for (let i = 0, l = particles.length; i < l; i++) {
      animateParticles(particles[i]);
    }
  }

  function animateText() {
    TweenMax.set($congratsSection, { autoAlpha: 1, zIndex: 1, onComplete: function() {
      TweenMax.set($title, { autoAlpha: 1, onComplete: runAnimateText });
    } });
  }

  function runAnimateText() {
    TweenMax.from($title, 0.65, {
      scale: 0.4,
      autoAlpha: 0,
      rotation: 15,
      ease: Back.easeOut.config(5),
      onComplete: function() {
        TweenMax.to($title, 1, { autoAlpha: 0, onComplete: function() {
          TweenMax.set($congratsSection, { autoAlpha: 0, zIndex: -1 });
        } });
      }
    });
  }

  function animateParticles(selector) {
    let xSeed = _.random(350, 380);
    let ySeed = _.random(120, 170);

    $.each($(selector), function(i) {
      let $particle = $(this);
      let speed = _.random(1, 4);
      let rotation = _.random(20, 100);
      let scale = _.random(0.8, 1.5);
      let x = _.random(-xSeed, xSeed);
      let y = _.random(-ySeed, ySeed);

      TweenMax.to($particle, speed, {
        x: x,
        y: y,
        ease: Power1.easeOut,
        opacity: 0,
        rotation: rotation,
        scale: scale,
        onStartParams: [$particle],
        onStart: function($element) {
          $element.css('display', 'block');
        },
        onCompleteParams: [$particle],
        onComplete: function($element) {
          $element.css('display', 'none');
        }
      });
    });
  }

  function reset() {
    for (let i = 0, l = particles.length; i < l; i++) {
      $.each($(particles[i]), function() {
        TweenMax.set($(this), { x: 0, y: 0, opacity: 1 });
      });
    }

    TweenMax.set($title, { scale: 1, opacity: 1, rotation: 0 });
  }

  function init(properties) {
    for (let i = 0; i < properties.numberOfStars; i++) {
      $congratsSection.append('<div class="particle star fa fa-star ' + i + '"></div>');
    }

    for (let i = 0; i < properties.numberOfBlobs; i++) {
      $congratsSection.append('<div class="particle blob ' + i + '"></div>');
    }
  }
  /*--------------------------*/


  /**
   * Pet Evolution
   */
  new Vue({
    el: '#section-8',
    name: 'Pet Evolution',
    data: {
      loaded: false,
      iconSource: $('#icon_source').val(),
      petSource: $('#pet_source').val(),
      pets: [
        {
          ID: 1,
          evoLvl: 1,
          maxEvo: 3,
          name: 'Poring',
          evo: 'Mastering',
          evo2: 'Angeling',
          hasEvo: false,
          hasEvo2: false,
          ingredients: [
            { id: 610, name: 'Yggdrasil Leaf', quantity: 10 },
            { id: 619, name: 'Unripe Apple', quantity: 3 }
          ],
          ingredients2: [
            { id: 503, name: 'Yellow Potion', quantity: 20 },
            { id: 2282, name: 'Halo', quantity: 1 },
            { id: 509, name: 'White Herb', quantity: 50 },
            { id: 909, name: 'jellopy', quantity: 200 }
          ],
          stats: [
            'LUK + 2',
            'CRI + 1'
          ],
          statsEvo: [
            'LUK + 3',
            'CRI + 3'
          ],
          statsEvo2: [
            'MaxHP + 2%',
            'Heal effectiveness + 8%'
          ],
          running: false,
          progress: 0
        },
        {
          ID: 2,
          evoLvl: 1,
          maxEvo: 2,
          name: 'Drops',
          evo: 'Eggring',
          hasEvo: false,
          ingredients: [
            { id: 7032, name: 'Piece of Egg Shell', quantity: 20 },
            { id: 7031, name: 'Old Frying Pan', quantity: 10 },
            { id: 531, name: 'Apple Juice', quantity: 3 },
            { id: 4659, name: 'Eggring Card', quantity: 1 }
          ],
          stats: [
            'HIT + 3',
            'ATK + 3'
          ],
          statsEvo: [
            'HIT + 9',
            'ATK + 9'
          ],
          running: false,
          progress: 0
        },
        {
          ID: 3,
          evoLvl: 1,
          maxEvo: 2,
          name: 'Drops',
          evo: 'Sweets Drops',
          hasEvo: false,
          ingredients: [
            { id: 25290, name: 'Sweets Festival Coin', quantity: 500 },
            { id: 529, name: 'Candy', quantity: 50 },
            { id: 530, name: 'Candy Cane', quantity: 50 },
            { id: 4659, name: 'Drops Card', quantity: 1 }
          ],
          stats: [
            'HIT + 3',
            'ATK + 3'
          ],
          statsEvo: [
            'Tăng điểm kinh nghiệm thêm 1% khi giết quái vật'
          ],
          running: false,
          progress: 0
        },
      ]
    },
    methods: {
      getPetInfo(id) {
        return _.find(this.pets, pet => {
          return pet.ID === id;
        });
      },
      evo(id) {
        const index = _.findIndex(this.pets, pet => {
          return pet.ID === id;
        });

        if (index >= 0) {
          this.pets[index]['running'] = true;

          const progressInterval = setInterval(() => {
            this.pets[index]['progress'] += 2;

            if (this.pets[index]['progress'] === 100) {
              clearInterval(progressInterval);

              setTimeout(() => {
                this.pets[index]['progress']  = 0;
                this.pets[index]['running'] = false;

                if (this.pets[index]['evoLvl'] === 1) {
                  this.pets[index]['hasEvo'] = true;
                }

                if (this.pets[index]['maxEvo'] > 2 && this.pets[index]['evoLvl'] === 2) {
                  this.pets[index]['hasEvo2'] = true;
                }

                if (this.pets[index]['evoLvl'] !== this.pets[index]['maxEvo']) {
                  this.pets[index]['evoLvl']++;
                }
              }, 1000);
            }
          }, 10);
        }
      },
      hasEvo(id) {
        const pet = this.getPetInfo(id);
        return pet.hasEvo;
      },
      hasEvo2(id) {
        const pet = this.getPetInfo(id);
        return pet.hasEvo2;
      },
      getPetName(id) {
        const pet = this.getPetInfo(id);
        if (this.hasEvo2(id)) {
          return pet.evo2;
        } else if (this.hasEvo(id)) {
          return pet.evo;
        }
        return pet.name;
      },
      getButtonText(id) {
        const pet = this.getPetInfo(id);
        if (pet.evoLvl === pet.maxEvo) return 'Tiến hóa xong';
        if (pet.running) {
          return 'Đang tiến hóa';
        }
        return 'Tiến hóa';
      }
    },
    created() {
      this.loaded = true;
    }
  });
  /*--------------------------*/

})(jQuery, ScrollMagic);