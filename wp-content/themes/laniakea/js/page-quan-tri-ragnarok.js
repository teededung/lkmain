(function($) {

  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

  let vmManager = new Vue({
    name: 'Ragnarok Manage',
    el: '#ragnarok-manage',
    data: {
      hours: null,
      chars: {
        online: [],
        vending: [],
        offline: [],
        all: []
      },
      char: null,
      postIdToSendActivationEmail: '',
      currentUserTab: 1,
      search: '',
      itemIDToRemove: '',
      oldItemID: '',
      newItemID: '',
      itemIdToCount: '',
      mode: 'chars',
      mobID: 1832,
      mobInfo: {},
      redeemCodeNumber: null,
      redeemCodeCount: null,
      redeemCodeRewards: null,
      redeemCodePrefix: null,
      redeemCodes: [],
      filterRedeemCodeText: '',
      getAvailableRedeemCode: 1,
      accountIDToBan: '',
      unbanTime: '',
      reason: '',
      skillLua: '',
      skillHTML: '',
      dbLog: ''
    },
    methods: {
      // Ban
      banChar(charID) {
        Swal.fire({
          title: 'Điền timestamp muốn ban',
          input: 'text',
          showCancelButton: true,
          confirmButtonText: 'Ban',
          showLoaderOnConfirm: true,
          preConfirm: (unix) => {
            return new Promise((resolve) => {
              $.ajax({
                url: TEE.ajaxurl,
                type: 'post',
                dataType: 'json',
                data: {
                  action: 'tee_ajax_function_',
                  method: 'ragnarok_ban_char',
                  char_id: charID,
                  unix_time_to_add: unix,
                  n: TEE.n
                },
                success(response) {
                  resolve(response);
                }
              });
            });
          },
          allowOutsideClick: () => !Swal.isLoading()
        }).then(result => {
          if (typeof result.value.status !== 'undefined') {
            const { status, message } = result.value;
            if (status === 'OK') {
              Swal.fire('Xong!', message, 'success');
            } else {
              Swal.fire('Lỗi!', message, 'error');
            }
          } else {
            Swal.fire('Lỗi!', message, 'error');
          }
        });
      },
      banAcc(account_id) {
        Swal.fire({
          title: 'Điền timestamp muốn ban',
          text: 'Tài khoản sẽ bị ban mà chứa nhân vật này.',
          input: 'number',
          showCancelButton: true,
          confirmButtonText: 'Ban',
          showLoaderOnConfirm: true,
          preConfirm: (unix) => {
            return new Promise((resolve) => {
              $.ajax({
                url: TEE.ajaxurl,
                type: 'post',
                dataType: 'json',
                data: {
                  action: 'tee_ajax_function_',
                  method: 'ragnarok_ban_acc',
                  account_id: account_id,
                  unix_time_to_add: unix,
                  n: TEE.n
                },
                success(response) {
                  resolve(response);
                }
              });
            });
          },
          allowOutsideClick: () => !Swal.isLoading()
        }).then(result => {
          if (typeof result.value.status !== 'undefined') {
            const { status, message } = result.value;
            if (status === 'OK') {
              Swal.fire('Xong!', message, 'success');
            } else {
              Swal.fire('Lỗi!', message, 'error');
            }
          } else {
            Swal.fire('Lỗi!', message, 'error');
          }
        });
      },
      banIP(account_id) {
        Swal.mixin({
          input: 'text',
          confirmButtonText: 'Tiếp &rarr;',
          showCancelButton: true,
          progressSteps: ['1', '2']
        }).queue([
          'Điền Timestamp',
          'Lý do'
        ]).then((result) => {

          $.ajax({
            url: TEE.ajaxurl,
            type: 'post',
            dataType: 'json',
            data: {
              action: 'tee_ajax_function_',
              method: 'ragnarok_ban_ip_by_account_id',
              account_id: account_id,
              unix_time_to_add: result.value[0],
              reason: result.value[1],
              n: TEE.n
            },
            beforeSend() {
              showLoadingChars();
            },
            success(response) {
              hideLoadingChars();
              if (typeof response.status !== 'undefined') {
                const { status, message } = response;
                if (status === 'OK') {
                  Swal.fire('Thành công', message, 'success');
                } else {
                  Swal.fire('Lỗi!', message, 'error');
                }
              }
            },
            error(e) {
              hideLoadingChars();
              Swal.fire('Lỗi!', e, 'error');
            }
          });
        });
      },
      banGepard() {
        const _this = this;
        $.ajax({
          url: TEE.ajaxurl,
          type: 'post',
          dataType: 'json',
          data: {
            action: 'tee_ajax_function_',
            method: 'ragnarok_ban_gepard',
            account_id: _this.accountIDToBan,
            unban_time: _this.unbanTime,
            reason: _this.reason,
            n: TEE.n
          },
          beforeSend() {
            $('#modal .modal-content').LoadingOverlay('show');
          },
          success(response) {
            $('#modal .modal-content').LoadingOverlay('hide');
            const { status, message } = response;
            if (typeof response.status !== 'undefined') {
              if (status === 'OK') {
                Swal.fire('Xong!', message, 'success');
              } else {
                Swal.fire('Lỗi!', message, 'error');
              }
            } else {
              Swal.fire('Lỗi!', message, 'error');
            }
            _this.accountIDToBan = '';
            _this.unbanTime = '';
          }
        });
      },

      // User Actions
      sendCash(account_id) {
        Swal.fire({
          title: 'Điền số Cash Point',
          text: 'Số Cash Point sẽ vào tài khoản mà chứa nhân vật bạn vừa chọn.',
          input: 'number',
          showCancelButton: true,
          confirmButtonText: 'Gửi',
          showLoaderOnConfirm: true,
          preConfirm: (cash) => {
            return new Promise((resolve) => {
              $.ajax({
                url: TEE.ajaxurl,
                type: 'post',
                dataType: 'json',
                data: {
                  action: 'tee_ajax_function_',
                  method: 'ragnarok_send_cash_to_char',
                  account_id: account_id,
                  cash,
                  n: TEE.n
                },
                success(response) {
                  resolve(response);
                }
              });
            });
          },
          allowOutsideClick: () => !Swal.isLoading()
        }).then(result => {
          const { status, message } = result.value;
          if (typeof status !== 'undefined') {
            Swal.fire((status === 'OK') ? 'Thành công!': 'Lỗi!' , message, (status === 'OK') ? 'success' : 'error');
          } else {
            Swal.fire('Lỗi!', message, 'error');
          }
        });
      },
      sendItems(account_id) {
        Swal.mixin({
          input: 'text',
          confirmButtonText: 'Tiếp &rarr;',
          showCancelButton: true,
          cancelButtonText: 'Hủy',
          progressSteps: ['1', '2']
        }).queue([
          'Điền item_id:amount,item_id:amount',
          'Lý do'
        ]).then(result => {
          const items_string = result.value[0];
          const reason = result.value[1];

          $.ajax({
            url: TEE.ajaxurl,
            type: 'post',
            dataType: 'json',
            data: {
              action: 'tee_ajax_function_',
              method: 'ragnarok_send_items_to_account',
              account_id: account_id,
              items_string,
              reason,
              n: TEE.n
            },
            success(response) {
              const { status, message } = response;
              if (typeof status !== 'undefined') {
                Swal.fire((status === 'OK') ? 'Thành công!': 'Lỗi!' , message, (status === 'OK') ? 'success' : 'error');
              } else {
                Swal.fire('Lỗi!', message, 'error');
              }
            }
          });
        });
      },
      detail(char, index) {
        this.getAccountCP(char.account_id, index);
        this.getUniqueID(char.account_id, index);
        this.char = char;
        setTimeout(() => {
          $('#modal-detail').modal('show');
        }, 300);
      },
      getAccountCP(account_id, index) {
        const _this = this;

        $.ajax({
          url: TEE.ajaxurl,
          type: 'post',
          dataType: 'json',
          data: {
            action: 'tee_ajax_function_',
            method: 'get_cp_by_account_id',
            account_id,
            n: TEE.n
          },
          success(response) {
            _this.updateRowChar(index, 'cp', response.cp);
            _this.$forceUpdate();
          }
        });
      },
      getUniqueID(account_id, index) {
        const _this = this;
  
        $.ajax({
          url: TEE.ajaxurl,
          type: 'post',
          dataType: 'json',
          data: {
            action: 'tee_ajax_function_',
            method: 'get_unique_id_by_account_id',
            account_id,
            n: TEE.n
          },
          success(response) {
            _this.updateRowChar(index, 'last_unique_id', response.last_unique_id);
            _this.$forceUpdate();
          }
        });
      },
      getJobName(classId) {
        return getJobName(classId);
      },
      resetSprites(char_id) {
        Swal.fire({
          title: 'Bạn có chắc không?',
          text: "Hãy nhớ nhắc người chơi logout trước khi thực hiện điều này!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Thực hiện',
          showLoaderOnConfirm: true,
          preConfirm: () => {
            $.ajax({
              url: TEE.ajaxurl,
              type: 'post',
              dataType: 'json',
              data: {
                action: 'tee_ajax_function_',
                method: 'ragnarok_reset_sprites',
                char_id,
                n: TEE.n
              },
              success(response) {
                const { status, message } = response;
                if (typeof status !== 'undefined') {
                  if (status === 'OK') {
                    Swal.fire('Thành công', message, 'success');
                  } else {
                    Swal.fire('Lỗi', message, 'error');
                  }
                }
              }
            });
          },
          allowOutsideClick: () => !Swal.isLoading()
        });
      },
      getOffClothes(char_id) {
        Swal.fire({
          title: 'Bạn có chắc không?',
          text: "Hãy nhớ nhắc người chơi logout trước khi thực hiện điều này!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Thực hiện',
          showLoaderOnConfirm: true,
          preConfirm: () => {
            $.ajax({
              url: TEE.ajaxurl,
              type: 'post',
              dataType: 'json',
              data: {
                action: 'tee_ajax_function_',
                method: 'ragnarok_get_off_clothes',
                char_id,
                n: TEE.n
              },
              success(response) {
                const { status, message } = response;
                if (typeof status !== 'undefined') {
                  if (status === 'OK') {
                    Swal.fire('Thành công', message, 'success');
                  } else {
                    Swal.fire('Lỗi', message, 'error');
                  }
                }
              }
            });
          },
          allowOutsideClick: () => !Swal.isLoading()
        });
      },
      deleteItem(char_id) {
        Swal.fire({
          title: 'Điền ID của item',
          input: 'number',
          showCancelButton: true,
          confirmButtonText: 'Xóa',
          showLoaderOnConfirm: true,
          preConfirm: (id) => {
            return new Promise((resolve) => {
              $.ajax({
                url: TEE.ajaxurl,
                type: 'post',
                dataType: 'json',
                data: {
                  action: 'tee_ajax_function_',
                  method: 'ragnarok_delete_item_from_char',
                  char_id,
                  id,
                  n: TEE.n
                },
                success(response) {
                  resolve(response);
                }
              });
            });
          },
          allowOutsideClick: () => !Swal.isLoading()
        }).then(result => {
          const { status, message } = result.value;
          if (typeof status !== 'undefined') {
            if (status === 'OK') {
              Swal.fire('Xong!', message, 'success');
            } else {
              Swal.fire('Lỗi!', message, 'error');
            }
          } else {
            Swal.fire('Lỗi!', message, 'error');
          }
        });
      },

      // Tools
      resendActivationEmail() {
        const _this = this;
        $.ajax({
          url: TEE.ajaxurl,
          type: 'POST',
          dataType: 'json',
          data: {
            action: 'tee_ajax_function_',
            method: 'resend_activation_email',
            post_id: _this.postIdToSendActivationEmail,
            n: TEE.n
          },
          beforeSend() {
            $.LoadingOverlay('show');
          },
          success(response) {
            $.LoadingOverlay('hide');
            const { status, message } = response;
            if (typeof status !== 'undefined') {
              if (status === 'OK') {
                Swal.fire('Xong!', message, 'success');
              } else {
                Swal.fire('Lỗi!', message, 'error');
              }
            }
          }
        });
      },
      generateRedeemCodes() {
        const _this = this;
        $.ajax({
          url: TEE.ajaxurl,
          type: 'post',
          dataType: 'json',
          data: {
            action: 'tee_ajax_function_',
            method: 'ragnarok_insert_redeem_codes',
            redeemCodeNumber: _this.redeemCodeNumber,
            redeemCodeCount: _this.redeemCodeCount,
            redeemCodeRewards: _this.redeemCodeRewards,
            redeemCodePrefix: _this.redeemCodePrefix,
            n: TEE.n
          },
          beforeSend() {
            $.LoadingOverlay('show');
          },
          success(response) {
            $.LoadingOverlay('hide');
            const { status, message } = response;

            if (typeof status !== 'undefined') {
              if (status === 'OK') {
                Swal.fire('Xong!', message, 'success');
              } else {
                Swal.fire('Lỗi!', message, 'error');
              }
            }
          },
          error(e) {
            $.LoadingOverlay('hide');
            Swal.fire('Lỗi!', e, 'error');
            console.log(e);
          }
        });
      },
      getRedeemCodes() {
        const _this = this;
        $.ajax({
          url: TEE.ajaxurl,
          type: 'post',
          dataType: 'json',
          data: {
            action: 'tee_ajax_function_',
            method: 'get_redeem_codes',
            filter: Number(_this.getAvailableRedeemCode),
            n: TEE.n
          },
          beforeSend() {
            $.LoadingOverlay('show');
          },
          success(response) {
            $.LoadingOverlay('hide');
            const { status, message, codes } = response;
            if (typeof status !== 'undefined') {
              if (status === 'OK') {
                Swal.fire('Xong!', message, 'success');
                _this.redeemCodes = codes;
              } else {
                Swal.fire('Lỗi!', message, 'error');
              }
            }
          }
        });
      },
      filterRedeemCodes(codes) {
        const app = this;

        return codes.filter(function(item) {
          let regex = new RegExp('(' + app.filterRedeemCodeText + ')', 'i');
          return item.code.match(regex);
        });
      },
      getMobInfo() {
        const _this = this;
        const mobID = _this.mobID;

        $.ajax({
          url: TEE.ajaxurl,
          type: 'post',
          dataType: 'json',
          data: {
            action: 'tee_ajax_function',
            method: 'get_mob_info_',
            n: TEE.n,
            mobID
          },
          beforeSend() {
            $('#formGetMobInfo').LoadingOverlay('show');
          },
          success(response) {
            $('#formGetMobInfo').LoadingOverlay('hide');
            const { status, data } = response;

            if (typeof status !== 'undefined') {
              if (status === 'OK') {

                console.log(data);
                // ID,Sprite_Name,kROName,iROName,LV,HP,SP,EXP,JEXP,Range1,ATK1,ATK2,DEF,MDEF,STR,AGI,VIT,INT,DEX,LUK,Range2,Range3,Scale,Race,Element,Mode,Speed,aDelay,aMotion,dMotion,MEXP,MVP1id,MVP1per,MVP2id,MVP2per,MVP3id,MVP3per,Drop1id,Drop1per,Drop2id,Drop2per,Drop3id,Drop3per,Drop4id,Drop4per,Drop5id,Drop5per,Drop6id,Drop6per,Drop7id,Drop7per,Drop8id,Drop8per,Drop9id,Drop9per,DropCardid,DropCardper
                const { id, dbname, name, stats, drops, mvpdrops, skill } = data;
                const { level, health, sp, str, int, vit, dex, agi, luk, atk1, atk2, defense, magicDefense, baseExperience, jobExperience, attackRange, aggroRange, escapeRange, rechargeTime, movementSpeed, attackSpeed, attackedSpeed, element, scale, race, mvp } = stats;
                const normalDrops = _.filter(drops, function(d) { return d['serverTypeName'] === 'Renewal'; });
                let mobDBText = "";
                let mobSkillTexts = [];
                let mode = "0x0";
                if (mvp) mode = "0x6283695";

                mobDBText = `${id},${dbname},${name},${name},${level},${health},${sp},${baseExperience},${jobExperience},${attackRange},${atk1},${atk2},${defense},${magicDefense},${str},${agi},${vit},${int},${dex},${luk},${aggroRange},${escapeRange},${scale},${race},${element},${mode},${movementSpeed},${rechargeTime},${attackSpeed},${attackedSpeed},0`;

                // MVP Drops
                let text = "";
                for (let i = 0; i < 3; i++) {
                  if (typeof mvpdrops[i] !== "undefined") {
                    text = "," + mvpdrops[i]['itemId'] + "," + mvpdrops[i]['chance'];
                  } else {
                    text = ",0,0";
                  }
                  mobDBText += text;
                }

                // Normal Drops
                const length = normalDrops.length;
                if (length) {
                  text = "";
                  for (let i = 0; i < length-1; i++) {
                    text = "," + normalDrops[i]['itemId'] + "," + normalDrops[i]['chance'];
                    mobDBText += text;
                  }

                  // Empty Drops
                  for (let i = 0; i < (10 - length); i++) {
                    mobDBText += ",0,0";
                  }

                  // Card Drops
                  mobDBText += "," + normalDrops[length-1]['itemId'] + "," + normalDrops[length-1]['chance'];
                }

                // Skills
                // MobID,Dummy value (info only),State,SkillID,SkillLv,Rate,CastTime,Delay,Cancelable,Target,Condition type,Condition value,val1,val2,val3,val4,val5,Emotion,Chat
                skill.forEach((sk) => {
                  let text = `${id},${name}@SkillName,${_this.getSkillState(sk['status'])},${sk['skillId']},${sk['level']},${sk['chance']*10},${sk['casttime']},${sk['delay']},${sk['interruptable'] ? 'yes' : 'no'},${_this.getSkillTarget(sk['skillId'])},${_this.getSkillCondition(sk['condition'])},${sk['conditionValue'] === null ? ',' : sk['conditionValue']},,,,,,${sk['sendValue'] === null ? ',' : sk['sendValue']}`;
                  mobSkillTexts = [...mobSkillTexts,text];
                });

                _this.mobInfo = data;
                _this.mobInfo.mobDBText = mobDBText;
                _this.mobInfo.mobSkillTexts = mobSkillTexts;
                _this.$forceUpdate();

              } else {
                Swal.fire('Lỗi!', 'Check code in server side', 'error');
              }
            }
          },
          error(e) {
            $('#formGetMobInfo').LoadingOverlay('hide');
            toastr["error"]("Không tìm thấy!");
            console.log(e);
          }
        });
      },
      getSkillState(state) {
        switch (state) {
          case 'IDLE_ST':
            return 'idle';
          case 'BERSERK_ST':
            return 'attack';
          case 'RUSH_ST':
            return 'chase';
          case 'RMOVE_ST':
            return 'walk';
          case 'MOVEITEM_ST':
            return 'loot';
          case 'FOLLOW_S':
            return 'follow';
          case 'ANGRY_ST':
            return 'angry';
          case 'DEAD_ST':
            return 'dead';
        }
      },
      getSkillCondition(condition) {
        switch (condition) {
          case 'IF_RANGEATTACKED':
            return 'longrangeattacked';
          case 'IF_RUDEATTACK':
            return 'rudeattacked';
          case 'IF_SLAVENUM':
            return 'slavele';
          case 'IF_SKILLUSE':
            return 'skillused';
          case 'IF_HP':
            return 'myhpltmaxrate';
          case 'IF_HIDING':
            return 'mystatuson';
          case 'IF_COMRADEHP':
            return 'friendhpltmaxrate';
          case 'IF_COMRADECONDITION':
            return 'friendstatuson';
          case 'IF_ENEMYCOUNT':
            return 'attackpcge';
          case 'IF_MAGICLOCKED':
            return 'casttargeted';
          default:
            return 'always';
        }
      },
      getSkillTarget(skillID) {
        let type = 'self';
        switch (skillID) {
          case 5: // SM_BASH
          case 6: // SM_PROVOKE
          case 14: // MG_COLDBOLT
          case 15: // MG_FROSTDIVER
          case 19: // MG_FIREBOLT
          case 20: // MG_LIGHTNINGBOLT
          case 59: // KN_SPEARBOOMERANG
          case 83: // WZ_METEOR
          case 84: // WZ_JUPITEL
          case 85: // WZ_VERMILION
          case 86: // WZ_WATERBALL
          case 89: // WZ_STORMGUST
          case 110: // BS_HAMMERFALL
          case 159: // NPC_MENTALBREAKER
          case 170: // NPC_CRITICALSLASH
          case 171: // NPC_COMBOATTACK
          case 172: // NPC_GUIDEDATTACK
          case 179: // NPC_STUNATTACK
          case 183: // NPC_RANDOMATTACK
          case 184: // NPC_WATERATTACK
          case 185: // NPC_GROUNDATTACK
          case 186: // NPC_FIREATTACK
          case 187: // NPC_WINDATTACK
          case 188: // NPC_POISONATTACK
          case 189: // NPC_HOLYATTACK
          case 190: // NPC_DARKNESSATTACK
          case 289: // SA_DISPELL
          case 344: // NPC_ARMORBRAKE
          case 397: // LK_SPIRALPIERCE
          case 673: // NPC_CRITICALWOUND
            type = 'target';
        }
        return type;
      },
      getMobRace(raceNum) {
        switch (raceNum) {
          case 0:
            return 'Formless';
          case 1:
            return 'Undead';
          case 2:
            return 'Brute';
          case 3:
            return 'Plant';
          case 4:
            return 'Insect';
          case 5:
            return 'Fish';
          case 6:
            return 'Demon';
          case 7:
            return 'Demi Human';
          case 8:
            return 'Angel';
          case 9:
            return 'Dragon';
          case 10:
            return 'Boss';
          case 11:
            return 'Non Boss';
          case 12:
            return 'New Item';
          case 13:
            return 'Non Demi Human';
          default:
            return 'Unknown';
        }
      },
      getMobElement(elementDigit) {
        if (elementDigit === null) return 'Unknown';

        const elementLevel = Number(elementDigit.toString().charAt(0));
        const elementType = Number(elementDigit.toString().charAt(1));
        let strLvl = elementLevel/2;
        let type = '';

        switch (elementType) {
          case 0:
            type = 'Ele_Neutral'; break;
          case 1:
            type = 'Ele_Water'; break;
          case 2:
            type = 'Ele_Earth'; break;
          case 3:
            type = 'Ele_Fire'; break;
          case 4:
            type = 'Ele_Wind'; break;
          case 5:
            type = 'Ele_Poison'; break;
          case 6:
            type = 'Ele_Holy'; break;
          case 7:
            type = 'Ele_Dark'; break;
          case 8:
            type = 'Ele_Ghost'; break;
          case 9:
            type = 'Ele_Undead'; break;
        }

        return type + " " + strLvl;
      },
      getMobScale(scaleNum) {
        switch (scaleNum) {
          case 0:
            return 'Small';
          case 1:
            return 'Medium';
          case 2:
            return 'Large';
        }
      },
      convertLuaToHTML() {
        let str = this.skillLua;

        let lines = str.split(/\r*\n/);
        let linesLength = lines.length;

        // Remove Last comma
        for (let i = 0; i < linesLength; i++) {
          lines[i] = lines[i].trim().replace(/\\n/g,"");
          if (lines[i].includes(`",`)) {
            lines[i] = lines[i].substring(0, lines[i].length-1);
          }
        }

        // Remove First and Last Double Quotes
        for (let i = 0; i < linesLength; i++) {
          if (lines[i].charAt(0) === `"` && lines[i].charAt(lines[i].length-1) === `"`) {
            lines[i] = lines[i].substring(1, lines[i].length-1);
          }
        }

        // HTMl color
        for (let i = 0; i < linesLength; i++) {
          let value = lines[i];
          let matches = value.match(/([\dA-Fa-f]{6})/);

          let $is_description = false;
          let newValue = "";
          let closeTag = "</span>";

          // console.log(matches);

          if (matches) {
            if (matches[0] !== "000000") {
              newValue = lines[i].replace("^" + matches[0], `<span style='color: #${matches[0]};'>`);
            }

            if (lines[i].includes("Mô tả:")) {
              newValue = lines[i].replace("^" + matches[0], `<p style='color: #${matches[0]};'>`);
              $is_description = true;
            } else {
              if (matches[0] === "000000") {
                newValue = lines[i].replace("^" + matches[0], `<br>`);
              }
              $is_description = false;
            }

            if ($is_description) {
              closeTag = "<br>";
            }

            let tail = (i < linesLength-1) ? "<br>" : "";

            newValue = newValue.replace("^000000", closeTag) + tail;
          }
          else {
            newValue = lines[i] + "<br>";
          }

          lines[i] = newValue;
        }

        let finalString = "";
        for (let i = 0; i < linesLength; i++) {
          finalString += lines[i] + "\n";
        }

        this.skillHTML = finalString.trim() + "</p>";
      },
      removeItemIDInDatabase() {
        const _this = this;
        if (_this.itemIDToRemove > 0 && _this.itemIDToRemove !== '') {
          $.ajax({
            url: TEE.ajaxurl,
            type: 'post',
            dataType: 'json',
            data: {
              action: 'tee_ajax_function_',
              method: 'delete_item_id_in_database',
              item_id: _this.itemIDToRemove,
              n: TEE.n
            },
            beforeSend() {
              $.LoadingOverlay('show');
            },
            success(response) {
              $.LoadingOverlay('hide');
              const { status, message } = response;
              if (typeof status !== 'undefined') {
                Swal.fire('Thành công!', message, 'success');
                _this.dbLog = message;
              } else {
                Swal.fire('Lỗi!', message, 'error');
                _this.dbLog = message;
              }
            },
            error(e) {
              console.log(e);
              $.LoadingOverlay('hide');
            }
          });
        } else {
          toastr["error"]("Item ID không hợp lệ!");
        }
      },
      changeItemIDInDatabase() {
        const _this = this;
        if (_this.oldItemID > 0 && _this.newItemID > 0 && _this.oldItemID !== '' && _this.newItemID !== '') {
          $.ajax({
            url: TEE.ajaxurl,
            type: 'post',
            dataType: 'json',
            data: {
              action: 'tee_ajax_function_',
              method: 'change_item_id_in_database',
              old_item_id: _this.oldItemID,
              new_item_id: _this.newItemID,
              n: TEE.n
            },
            beforeSend() {
              $.LoadingOverlay('show');
            },
            success(response) {
              $.LoadingOverlay('hide');
              const { status, message } = response;
              if (typeof status !== 'undefined') {
                Swal.fire('Thành công!', message, 'success');
                _this.dbLog = message;
              } else {
                Swal.fire('Lỗi!', message, 'error');
                _this.dbLog = message;
              }
            },
            error(e) {
              console.log(e);
              $.LoadingOverlay('hide');
            }
          });
        } else {
          toastr["error"]("Item ID không hợp lệ!")
        }
      },
      countItemInDatabase() {
        const _this = this;
        $.ajax({
          url: TEE.ajaxurl,
          type: 'post',
          dataType: 'json',
          data: {
            action: 'tee_ajax_function_',
            method: 'count_item_in_database',
            item_id: _this.itemIdToCount,
            n: TEE.n
          },
          beforeSend() {
            $.LoadingOverlay('show');
          },
          success(response) {
            $.LoadingOverlay('hide');
            const { status, message } = response;
            if (typeof status !== 'undefined' && status === 'OK') {

              const { inventory, storage, vip_storage, cart_inventory, guild_storage, mail_attachments, total} = response;
              Swal.fire(
                'Đã đếm xong!',
                `
                Có <strong>${inventory}</strong> trong <code>inventory</code><br>
                Có <strong>${storage}</strong> trong <code>storage</code><br>
                Có <strong>${vip_storage}</strong> trong <code>vip_storage</code><br>
                Có <strong>${cart_inventory}</strong> trong <code>cart_inventory</code><br>
                Có <strong>${guild_storage}</strong> trong <code>guild_storage</code><br>
                Có <strong>${mail_attachments}</strong> trong <code>mail_attachments</code>
                <hr>
                Tổng cộng có ${total} vật phẩm <code>${_this.itemIdToCount}</code> trong server.<br>
                ` ,
                'success'
              );
            } else {
              Swal.fire('Lỗi!', message, 'error');
            }
          },
          error(e) {
            console.log(e);
            $.LoadingOverlay('hide');
          }
        });
      },

      // ETC
      formatZ(amount) {
        return Number(amount).formatZeny(0);
      },
      getCharsByCurrentTable() {
        switch (this.currentUserTab) {
          case 1:
            return this.chars.online;
          case 2:
            return this.chars.vending;
          case 3:
            return this.chars.offline;
          default:
            return this.chars.all;
        }
      },
      updateRowChar(charIndex, colName, value) {
        const _this = this;
        switch (_this.currentUserTab) {
          case 1:
            _this.chars['online'][charIndex][colName] = value;
            break;
          case 2:
            _this.chars['vending'][charIndex][colName] = value;
            break;
          case 3:
            _this.chars['offline'][charIndex][colName] =value;
            break;
          default:
            _this.chars['all'][charIndex][colName] = value;
            return this.chars.all;
        }
      },
      filterItems() {
        const _this = this;
        let chars = _this.getCharsByCurrentTable();

        let charsFilterName = chars.filter(function(char) {
          let regex = new RegExp('(' + _this.search + ')', 'i');
          return char.name.match(regex);
        });

        let charsFilterId = chars.filter(function(char) {
          let regex = new RegExp('(' + _this.search + ')', 'i');
          return char.account_id.match(regex);
        });

        let charsFilterCharId = chars.filter(function(char) {
          let regex = new RegExp('(' + _this.search + ')', 'i');
          return char.char_id.match(regex);
        });

        return _.merge(charsFilterName, charsFilterId, charsFilterCharId);
      },
      showModalGepard(account_id) {
        const $modal = $('#modal');
        $modal.on('shown.bs.modal', function (e) {
          flatpickr("#calendar-a", {
            dateFormat: "Y-m-d H:i:S",
          });
        });
    
        $modal.modal('show');
        this.accountIDToBan = account_id;
      },
      getUsers() {
        const _this = this;
        $.ajax({
          url: TEE.ajaxurl,
          type: 'post',
          dataType: 'json',
          data: {
            action: 'tee_ajax_function_',
            method: 'get_ragnarok_users',
            type: _this.currentUserTab,
            n: TEE.n
          },
          beforeSend() {
            showLoadingChars();
          },
          success(response) {
            hideLoadingChars();
      
            if (typeof response.status !== 'undefined') {
              if (response.status === 'OK') {
                const type = _this.currentUserTab;
                switch (type) {
                  case 1:
                    vmManager.chars.online = response.chars;
                    break;
                  case 2:
                    vmManager.chars.vending = response.chars;
                    break;
                  case 3:
                    vmManager.chars.offline = response.chars;
                    break;
                  default:
                    vmManager.chars.all = response.chars;
                    break;
                }
              } else {
              
              }
            } else {
        
            }
          },
          error(e) {
            console.log(e);
            hideLoadingChars();
          }
        });
      }
    },
    computed: {
      seconds() {
        return (this.hours * 60 * 60);
      }
    },
    watch: {
      mode(value) {
        if (value === 'tools') {
          setTimeout(() => {
            initTooltip();
          }, 300);
        }
      }
    },
    created() {
      // Init Clipboard
      initClipboard();
    }
  });

  /**
   * @param   {string}   s      the string to search in
   * @param   {string}   chars  the chars to search for
   * @param   {boolean=} rtl    option to parse from right to left
   * @return  {string}
   * @example tok( 'example.com?q=value#frag', '?#' ); // 'example.com'
   * @example tok( 'example.com?q=value#frag', '?#', true ); // 'frag'
   */
  function tok(s, chars, rtl) {
    var n, i = chars.length;
    rtl = true === rtl;
    while ( i-- ) {
      n = s.indexOf(chars[i]);
      s = n < 0 ? s : rtl
          ? s.substr(++n)
          : s.substr(0, n);
    }
    return s;
  }

  function showLoadingChars() {
    $('.w-table-chars').LoadingOverlay('show');
  }

  function hideLoadingChars() {
    $('.w-table-chars').LoadingOverlay('hide');
  }

  function initClipboard() {
    let clipboard = new ClipboardJS('.clipboard');

    clipboard.on('success', function(e) {
      toastr["success"]("Đã copy.");
    });

    $('.clipboard').click(function(e) {
      e.preventDefault();
    });
  }

  function initTooltip() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  function ajaxFunction() {
    Swal.fire({
      title: 'Điền timestamp muốn ban?',
      input: 'text',
      showCancelButton: true,
      confirmButtonText: 'Ban',
      showLoaderOnConfirm: true,
      preConfirm: (unix) => {
        return new Promise((resolve) => {
          $.ajax({
            url: TEE.ajaxurl,
            type: 'post',
            dataType: 'json',
            data: {
              action: 'tee_ajax_function_',
              method: 'ragnarok_ban_char',
              char_id: charID,
              unix_time_to_add: unix,
              n: TEE.n
            },
            success(response) {
              resolve(response);
            }
          });
        });
      },
      allowOutsideClick: () => !Swal.isLoading()
    }).then(result => {
      if (typeof result.status !== 'undefined') {
        const { status, message } = result.value;
        if (status === 'OK') {
          Swal.fire('Cảm ơn bạn đã đóng góp', message, 'success');
        } else {
          Swal.fire('Lỗi!', message, 'error');
        }
      } else {
        Swal.fire('Lỗi!', message, 'error');
      }
    });
  }

  function getJobName(jobId) {
    switch (Number(jobId)) {
      case 0:
        return 'Novice';
      case 1:
        return 'Swordman';
      case 2:
        return 'Magician';
      case 3:
        return 'Archer';
      case 4:
        return 'Acolyte';
      case 5:
        return 'Merchant';
      case 6:
        return 'Thief';
      case 7:
      case 13:
        return 'Knight';
      case 8:
        return 'Priest';
      case 9:
        return 'Wizard';
      case 10:
        return 'Blacksmith';
      case 11:
        return 'Hunter';
      case 12:
        return 'Assassin';
      case 14:
      case 21:
        return 'Crusader';
      case 15:
        return 'Monk';
      case 16:
        return 'Sage';
      case 17:
        return 'Rogue';
      case 18:
        return 'Alchemist';
      case 19:
        return 'Bard';
      case 20:
        return 'Dancer';
      case 22:
        return 'Đám cưới';
      case 23:
        return 'Super Novice';
      case 24:
        return 'Gunslinger';
      case 25:
        return 'Ninja';
      case 4001:
        return 'Novice High';
      case 4002:
        return 'Swordman High';
      case 4003:
        return 'Magician High';
      case 4004:
        return 'Archer High';
      case 4005:
        return 'Acolyte High';
      case 4006:
        return 'Merchant High';
      case 4007:
        return 'Thief High';
      case 4008:
      case 4014:
        return 'Lord Knight';
      case 4009:
        return 'High Priest';
      case 4010:
        return 'High Wizard';
      case 4011:
        return 'Whitesmith';
      case 4012:
        return 'Sniper';
      case 4013:
        return 'Assassin Cross';
      case 4015:
      case 4022:
        return 'Paladin';
      case 4016:
        return 'Champion';
      case 4017:
        return 'Professor';
      case 4018:
        return 'Stalker';
      case 4019:
        return 'Creator';
      case 4020:
        return 'Clown';
      case 4021:
        return 'Gypsy';
      case 4023:
        return 'Baby Novice';
      case 4024:
        return 'Baby Swordman';
      case 4025:
        return 'Baby Magician';
      case 4026:
        return 'Baby Archer';
      case 4027:
        return 'Baby Acolyte';
      case 4028:
        return 'Baby Merchant';
      case 4029:
        return 'Baby Thief';
      case 4030:
      case 4036:
        return 'Baby Knight';
      case 4031:
        return 'Baby Priest';
      case 4032:
        return 'Baby Wizard';
      case 4033:
        return 'Baby Blacksmith';
      case 4034:
        return 'Baby Hunter';
      case 4035:
        return 'Baby Assassin';
      case 4037:
      case 4044:
        return 'Baby Crusader';
      case 4038:
        return 'Baby Monk';
      case 4039:
        return 'Baby Sage';
      case 4040:
        return 'Baby Rogue';
      case 4041:
        return 'Baby Alchemist';
      case 4042:
        return 'Baby Bard';
      case 4043:
        return 'Baby Dancer';
      case 4045:
        return 'Baby Super Novice';
      case 4046:
        return 'Taekwon';
      case 4047:
        return 'Star Gladiator';
      case 4048:
        return 'Star Gladiator (Union)';
      case 4049:
        return 'Soul Linker';
      case 4050:
        return 'Gangsi (Bongun/Munak)';
      case 4051:
        return 'Death Knight';
      case 4052:
        return 'Dark Collector';
      case 4053:
      case 4080:
        return 'Rune Knight (Regular)';
      case 4055:
        return 'Warlock (Regular)';
      case 4056:
      case 4084:
        return 'Ranger (Regular)';
      case 4057:
        return 'Arch Bishop (Regular)';
      case 4058:
      case 4086:
        return 'Mechanic (Regular)';
      case 4059:
        return 'Guillotine Cross (Regular)';
      case 4060:
      case 4081:
        return 'Rune Knight (Trans)';
      case 4061:
        return 'Warlock (Trans)';
      case 4062:
      case 4085:
        return 'Ranger (Trans)';
      case 4063:
        return 'Arch Bishop (Trans)';
      case 4064:
      case 4087:
        return 'Mechanic (Trans)';
      case 4065:
        return 'Guillotine Cross (Trans)';
      case 4066:
      case 4082:
        return 'Royal Guard (Regular)';
      case 4067:
        return 'Sorcerer (Regular)';
      case 4068:
        return 'Minstrel (Regular)';
      case 4069:
        return 'Wanderer (Regular)';
      case 4070:
        return 'Sura (Regular)';
      case 4071:
        return 'Genetic (Regular)';
      case 4072:
        return 'Shadow Chaser (Regular)';
      case 4073:
      case 4083:
        return 'Royal Guard (Trans)';
      case 4074:
        return 'Sorcerer (Trans)';
      case 4075:
        return 'Minstrel (Trans)';
      case 4076:
        return 'Wanderer (Trans)';
      case 4077:
        return 'Sura (Trans)';
      case 4078:
        return 'Genetic (Trans)';
      case 4079:
        return 'Shadow Chaser (Trans)';
      case 4096:
      case 4109:
        return 'Baby Rune Knight';
      case 4097:
        return 'Baby Warlock';
      case 4098:
      case 4111:
        return 'Baby Ranger';
      case 4099:
        return 'Baby Arch Bishop';
      case 4100:
      case 4112:
        return 'Baby Mechanic';
      case 4101:
        return 'Baby Guillotine Cross';
      case 4102:
      case 4110:
        return 'Baby Royal Guard';
      case 4103:
        return 'Baby Sorcerer';
      case 4104:
        return 'Baby Minstrel';
      case 4105:
        return 'Baby Wanderer';
      case 4106:
        return 'Baby Sura';
      case 4107:
        return 'Baby Genetic';
      case 4108:
        return 'Baby Shadow Chaser';
      case 4190:
        return 'Super Novice (Expanded)';
      case 4191:
        return 'Super Baby (Expanded)';
      case 4211:
        return 'Kagerou';
      case 4212:
        return 'Oboro';
      case 4215:
        return 'Rebellion';
      case 4218:
        return 'Summoner';
      case 4220:
        return 'Baby Summoner';
      case 4222:
        return 'Baby Ninja';
      case 4223:
        return 'Baby Kagerou';
      case 4224:
        return 'Baby Oboro';
      case 4225:
        return 'Baby Taekwon';
      case 4226:
        return 'Baby Star Gladiator';
      case 4227:
        return 'Baby Soul Linker';
      case 4228:
        return 'Baby Gunslinger';
      case 4229:
        return 'Baby Rebellion';
      case 4238:
        return 'Baby Star Gladiator (Union)';
      case 4239:
        return 'Star Emperor';
      case 4240:
        return 'Soul Reaper';
      case 4241:
        return 'Baby Star Emperor';
      case 4242:
        return 'Baby Soul Reaper';
    }
  }

  Number.prototype.formatZeny = function(c, d, t){
    var n = this,
        c = isNaN(ce = Math.abs(c)) ? 2 : c,
        d = d === undefined ? "." : d,
        t = t === undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
  };
})(jQuery);