(function($) {
  Vue.use(VeeValidate, {
    locale: 'vi'
  });

  let registerForm;

  if ($('#register-form').length > 0) {
    registerForm = new Vue({
      name: 'Register Form',
      el: '#register-form',
      data: {
        registered: false,
        username: '',
        password: '',
        passwordConfirm: '',
        email: '',
        captcha: ''
      },
      methods: {
        register() {
          const _this = this;
          this.$validator.validateAll().then((result) => {
            if (result) {
              $.ajax({
                url: TEE.ajaxurl,
                type: 'post',
                dataType: 'json',
                data: {
                  action: 'tee_ajax_function',
                  method: 'register_account',
                  n: TEE.n,
                  username: _this.username,
                  password: _this.passwordConfirm,
                  email: _this.email,
                  captcha: _this.captcha
                },
                beforeSend() {
                  $.LoadingOverlay('show');
                },
                success(response) {
                  $.LoadingOverlay('hide');

                  if (typeof response.status !== 'undefined') {
                    const { status, message, laniakea } = response;
                    if (status === 'Failed') {
                      swal.fire(
                          'Lỗi',
                          message,
                          'error'
                      );
                    } else if (status === 'OK') {
                      _this.registered = true;
                      _this.username = '';
                      _this.password = '';
                      _this.email = '';
                      _this.passwordConfirm = '';
                      _this.errors.clear();

                      Cookies.set('laniakeaA', laniakea, { expires: 7 });
                      Cookies.set('l_login', 1, { expires: 1 });

                      swal.fire({
                        title: 'Đăng kí thành công',
                        text: 'Bạn vừa đăng kí thành công! Bạn có thể đăng nhập vào game mà không cần phải kích hoạt tài khoản.',
                        type: 'success',
                        confirmButtonText: 'Tài khoản'
                      }).then(() => {
                        location.reload();
                      });
                    }
                  } else {
                    showErrorRegister();
                  }
                },
                error(e) {
                  $.LoadingOverlay('hide');
                  showErrorRegister();
                }
              });
            }
          });
        },
        getSubmitButtonText() {
          if (this.captcha.length > 0) {
            return 'Đăng ký';
          } else {
            return 'Hãy xác nhận bạn không phải là người máy ^^!';
          }
        }
      }
    });
  }

  window.enableSubmit = function() {
    registerForm.captcha = $('#g-recaptcha-response').val();
  };

  window.disableSubmit = function() {
    registerForm.captcha = '';
  };

  function showErrorRegister() {
    Swal.fire({
      type: 'error',
      title: 'Oops...',
      text: 'Vui lòng thử lại sau!',
      footer: 'Nếu bạn cần hỗ trợ. Hãy nhắn tin cho GM qua&nbsp;<a href="https://www.facebook.com/laniakea.ro/">Fanpage</a>.',
    });
  }

})(jQuery);