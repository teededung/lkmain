(function($) {

  $('#form-reset-password').on('submit', (e) => {
    e.preventDefault();

    const email = $('#email').val();

    $.ajax({
      url: TEE.ajaxurl,
      type: 'post',
      dataType: 'json',
      data: {
        action: 'tee_ajax_function',
        method: 'ragnarok_lost_password',
        email,
        n: TEE.n
      },
      beforeSend() {
        $('.box').LoadingOverlay('show');
      },
      success(response) {
        $('.box').LoadingOverlay('hide');

        const { status, message } = response;
        if (typeof status !== 'undefined') {
          if (status === 'OK') {
            swal('Xong!', message, 'success');
          } else {
            swal('Lỗi!', message, 'error');
          }
        } else {
          swal('Lỗi!', message, 'error');
        }
      }
    });

  });

})(jQuery);