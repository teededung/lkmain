(function($, window, document, undefined) {
  'use strict';

  // init cubeportfolio
  $('#grid-container').cubeportfolio({
    filters: '#js-filters-masonry',
    layoutMode: 'grid',
    defaultFilter: '*',
    animationType: 'slideDelay',
    gapHorizontal: 20,
    gapVertical: 20,
    gridAdjustment: 'responsive',
    mediaQueries: [{
      width: 1500,
      cols: 4,
    }, {
      width: 1100,
      cols: 3,
    }, {
      width: 800,
      cols: 2,
    }, {
      window: 480,
      cols: 1
    }],
    caption: 'zoom',
    displayType: 'sequentially',
    displayTypeSpeed: 100,

    // lightbox
    lightboxDelegate: '.cbp-lightbox',
    lightboxGallery: true,
    lightboxTitleSrc: 'data-title',
    lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} của {{total}}</div>',

    // singlePageInline
    singlePageInlineDelegate: null
  });
})(jQuery, window, document);