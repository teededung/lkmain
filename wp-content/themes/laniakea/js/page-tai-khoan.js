(function($) {

  Number.prototype.formatZeny = function(c, d, t){
    var n = this,
        c = isNaN(ce = Math.abs(c)) ? 2 : c,
        d = d === undefined ? "." : d,
        t = t === undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
  };

  Vue.use(VeeValidate, {
    locale: 'vi'
  });

  let vm = new Vue({
    name: 'Account',
    el: '.app-tai-khoan',
    data: {
      auth: Cookies.get('laniakeaA'),
      birthdayFormatted: '',
      birthday: '',
      error: false,

      imgTitles: [],
      imgDescriptions: [],
      imgCharNames: [],

      preventSubmitAgain: false
    },
    methods: {
      changePassword() {
        const _this = this;

        Swal.mixin({
          input: 'password',
          confirmButtonText: 'Tiếp &rarr;',
          showCancelButton: true,
          cancelButtonText: 'Hủy',
          progressSteps: ['1', '2', '3']
        }).queue([
          'Điền mật khẩu cũ',
          'Điền mật khẩu mới',
          'Nhập lại mật khẩu mới'
        ]).then(result => {

          const oldPassword = result.value[0];
          const newPassword = result.value[1];
          const newPasswordConfirm = result.value[2];

          if (newPassword !== newPasswordConfirm) {
            Swal.fire({
              type: 'error',
              title: 'Oops...',
              text: 'Nhập lại mật khẩu không giống với mật khẩu mới!'
            });
          } else {
            $.ajax({
              url: TEE.ajaxurl,
              type: 'post',
              dataType: 'json',
              data: {
                action: 'tee_ajax_function',
                method: 'ragnarok_change_password',
                data: {
                  auth: _this.auth,
                  old_password: oldPassword,
                  new_password: newPassword
                },
                n: TEE.n
              },
              beforeSend() {
                $.LoadingOverlay('show');
              },
              success(response) {
                $.LoadingOverlay('hide');

                if (typeof response.status !== 'undefined') {
                  if (response.status === 'OK') {
                    Swal.fire({
                      type: 'success',
                      title: 'Thành công',
                      text: response.message
                    }).then(() => {
                      Cookies.remove('laniakeaA');
                      location.reload();
                    });
                  } else {
                    showError(response.message);
                  }
                } else {
                  showError(response.message);
                }
              },
              error(e) {
                $.LoadingOverlay('hide');
                showError();
              }
            });
          }
        });
      },
      updateTempForm(formID) {
        if (typeof this.imgTitles[formID] === "undefined") {
          this.imgTitles[formID] = "";
          this.imgDescriptions[formID] = "";
          this.$forceUpdate();
        }
      },
      submit(formID) {
        const _this = this;

        if (_this.preventSubmitAgain) {
          return toastr["info"]("Vui lòng tải lại trang và thử lại.");
        }

        const $formID = $('#collapse-img-form-' + formID);
        const imageData = $formID.find('input[name="slim[]"]').val();

        if (imageData.length === 0) {
          if (!_this.errors.has('images['+formID+']', `${formID}`)) {
            _this.errors.add({
              scope: `${formID}`,
              field: 'images['+formID+']',
              msg: 'Hình ảnh là bắt buộc'
            });
          }
          return;
        }

        if (_this.imgTitles[formID].length === 0) {
          _this.errors.add({
            scope: `${formID}`,
            field: 'imgTitles['+formID+']',
            msg: 'Tiêu đề là bắt buộc'
          });
          return;
        }

        if (_this.imgDescriptions[formID].length === 0) {
          _this.errors.add({
            scope: `${formID}`,
            field: 'imgDescriptions['+formID+']',
            msg: 'Nội dung giới thiệu là bắt buộc'
          });
          return;
        }

        _this.errors.remove('images['+formID+']', `${formID}`);

        let formData = new FormData();

        const imageDataJSON     = JSON.parse(imageData),
          //name                = imageDataJSON.input.name,
          base64              = imageDataJSON.input.image, // Get base64 content
          block               = base64.split(";"), // Split base64 content
          contentType         = block[0].split(":")[1], // Get type
          realData            = block[1].split(",")[1], // Get base64
          blob                = b64toBlob(realData, contentType); // base64 to blob

        formData.append('action', 'tee_ajax_function');
        formData.append('method', 'submit_content_event');
        formData.append('n', TEE.n);
        formData.append('auth', _this.auth);
        formData.append('event_slug', $('#event_slug_' + formID).val());
        formData.append('char_name', _this.imgCharNames[formID]);
        formData.append('img_title', _this.imgTitles[formID]);
        formData.append('img_description', _this.imgDescriptions[formID]);

        formData.append('type', contentType);
        formData.append('image', blob);

        $.ajax({
          url: TEE.ajaxurl,
          type: 'post',
          dataType: 'json',
          cache: false,
          contentType: false,
          processData: false,
          data: formData,
          beforeSend() {
            $.LoadingOverlay('show');
          },
          success(response) {
            $.LoadingOverlay('hide');
            if (typeof response.status !== 'undefined') {
              const { status, title, message } = response;
              if (status === 'OK') {
                _this.preventSubmitAgain = true;

                Swal.fire({
                  type: 'success',
                  title,
                  text: message,
                }).then(() => {
                  location.reload();
                });
              } else {
                Swal.fire({
                  type: 'error',
                  title,
                  text: message,
                });
              }
            }
          },
          error(e) {
            $.LoadingOverlay('hide');
            toastr['error']("Không thể gửi ảnh! Hãy thử lại sau.");
          }
        });
      },
      deleteImage(attach_id, event_slug) {
        const _this = this;

        Swal.fire({
          title: 'Bạn có chắc chắn xóa ảnh này?',
          text: "Nó sẽ không thể phục hồi một khi đồng ý xóa!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#2ba6e1',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Đồng ý xóa!',
          cancelButtonText: 'Hủy'
        }).then((result) => {
          if (result.value) {
            const $grid = $('#grid-container');

            $.ajax({
              url: TEE.ajaxurl,
              type: 'post',
              dataType: 'json',
              data: {
                action: 'tee_ajax_function',
                method: 'delete_img_from_album',
                auth: _this.auth,
                attach_id,
                event_slug,
                n: TEE.n
              },
              beforeSend() {
                $grid.LoadingOverlay('show');
              },
              success(response) {
                $grid.LoadingOverlay('hide');

                if (typeof response.status !== 'undefined') {
                  const { status } = response;
                  if (status === 'OK') {
                    $grid.cubeportfolio('remove', $('.cbp-item-album#attach-'+attach_id));
                    toastr['success']("Bạn vừa xóa ảnh thành công!");
                  } else {
                    toastr['error'](response.message);
                  }
                }
              },
              error(e) {
                console.log(e);
                $grid.LoadingOverlay('hide');
                toastr['error']("Xóa ảnh không thành công!");
              }
            });
          }
        });
      },
      deleteChar(charID) {
        toastr["error"]("Chức năng này đang được xây dựng! Bạn vui lòng xóa nhân vật từ game.");
      },
      getJobName(jobId) {
        return getJobName(jobId);
      },
      convertZeny(num) {
        return Number(num).formatZeny(0);
      }
    },
    mounted() {
      if ($('.cbp').length > 0) {
        initCube();
      }
    },
    watch: {
      birthday(value) {
        const dateSel = new Date(value);

        let date = dateSel.getDate();
        let month = dateSel.getMonth() + 1;
        let year = dateSel.getFullYear();
        let y = year.toString().substr(-2);

        if (date < 10) {
          date = "0" + date;
        }

        if (month < 10) {
          month = "0" + month;
        }

        this.birthdayFormatted = date + "/" + month + "/" + year + " (" + y + month + date + ")";
      }
    }
  });

  function initCube() {
    // init cubeportfolio
    $('#grid-container').cubeportfolio({
      filters: '#js-filters-masonry',
      layoutMode: 'grid',
      defaultFilter: '*',
      animationType: 'slideDelay',
      gapHorizontal: 20,
      gapVertical: 20,
      gridAdjustment: 'responsive',
      mediaQueries: [{
        width: 1500,
        cols: 5,
      }, {
        width: 1100,
        cols: 4,
      }, {
        width: 800,
        cols: 4,
      }, {
        window: 480,
        cols: 3
      }],
      caption: 'overlayBottomPush',
      displayType: 'sequentially',
      displayTypeSpeed: 100,

      // lightbox
      lightboxDelegate: '.cbp-lightbox',
      lightboxGallery: true,
      lightboxTitleSrc: 'data-title',
      lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} của {{total}}</div>',

      singlePageDelegate: null
    });
  }

  initCalendar();
  function initCalendar() {
    $("#birthday").flatpickr({
      dateFormat: 'd/m/Y (ymd)',
      onChange: function(selectedDates, dateStr, instance) {
        const dateSel = new Date(selectedDates);
        let date = dateSel.getDate();
        let month = dateSel.getMonth() + 1;
        let year = dateSel.getFullYear();

        if (date < 10) {
          date = "0" + date;
        }

        if (month < 10) {
          month = "0" + month;
        }

        $.ajax({
          url: TEE.ajaxurl,
          type: 'post',
          dataType: 'json',
          data: {
            action: 'tee_ajax_function',
            method: 'ragnarok_update_birthday',
            birthday: year + "-" + month + "-" + date,
            auth: Cookies.get('laniakeaA'),
            n: TEE.n
          },
          beforeSend() {
            $('#birthday').LoadingOverlay('show');
          },
          success(response) {
            $('#birthday').LoadingOverlay('hide');
            if (typeof response.status !== 'undefined') {
              if (response.status === 'OK') {
                toastr["success"](response.message);
              }
            } else {
              toastr["error"](response.message);
            }
          },
          error(e) {
            $('#birthday').LoadingOverlay('hide');
            toastr["error"]('Có lỗi xảy ra :( Vui lòng thử lại sau!');
          }
        });
      },
    });
  }

  function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 1024;

    let byteCharacters = atob(b64Data);
    let byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      let slice = byteCharacters.slice(offset, offset + sliceSize);

      let byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      let byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    return new Blob(byteArrays, {type: contentType});
  }

  window.slimCheck = (file, image, meta) => {
    const w = image.width;
    const h = image.height;

    if (w < 250 || h < 250) {
      return 'Kích thước quá nhỏ. Kích thước phải lớn hơn 250x250.';
    }

    vm.errors.remove('image');
    return true;
  };

  function showError(text = '') {
    Swal.fire({
      type: 'error',
      title: 'Oops...',
      text: (text === '') ? 'Có lỗi xảy ra. Vui lòng thử lại sau hoặc liên hệ GM.' : text,
      footer: 'Nếu bạn cần hỗ trợ. Hãy nhắn tin cho GM qua&nbsp;<a href="https://www.facebook.com/laniakea.ro/">Fanpage</a>.',
    });
  }

  function getWeekDay(day) {
    const array = ['Chủ nhật', 'Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7'];
    return array[day];
  }

  function getStateText(state) {
    switch (state) {
      case 0:
        return 'Bình thường';
      case 1:
        return 'Chưa kích hoạt';
      case 3:
        return 'Đã hết hạn';
      default:
        return 'Bị khóa';
    }
  }

  function getJobName(jobId) {
    switch (Number(jobId)) {
      case 0:
        return 'Novice';
      case 1:
        return 'Swordman';
      case 2:
        return 'Magician';
      case 3:
        return 'Archer';
      case 4:
        return 'Acolyte';
      case 5:
        return 'Merchant';
      case 6:
        return 'Thief';
      case 7:
      case 13:
        return 'Knight';
      case 8:
        return 'Priest';
      case 9:
        return 'Wizard';
      case 10:
        return 'Blacksmith';
      case 11:
        return 'Hunter';
      case 12:
        return 'Assassin';
      case 14:
      case 21:
        return 'Crusader';
      case 15:
        return 'Monk';
      case 16:
        return 'Sage';
      case 17:
        return 'Rogue';
      case 18:
        return 'Alchemist';
      case 19:
        return 'Bard';
      case 20:
        return 'Dancer';
      case 22:
        return 'Đám cưới';
      case 23:
        return 'Super Novice';
      case 24:
        return 'Gunslinger';
      case 25:
        return 'Ninja';
      case 4001:
        return 'Novice High';
      case 4002:
        return 'Swordman High';
      case 4003:
        return 'Magician High';
      case 4004:
        return 'Archer High';
      case 4005:
        return 'Acolyte High';
      case 4006:
        return 'Merchant High';
      case 4007:
        return 'Thief High';
      case 4008:
      case 4014:
        return 'Lord Knight';
      case 4009:
        return 'High Priest';
      case 4010:
        return 'High Wizard';
      case 4011:
        return 'Whitesmith';
      case 4012:
        return 'Sniper';
      case 4013:
        return 'Assassin Cross';
      case 4015:
      case 4022:
        return 'Paladin';
      case 4016:
        return 'Champion';
      case 4017:
        return 'Professor';
      case 4018:
        return 'Stalker';
      case 4019:
        return 'Creator';
      case 4020:
        return 'Clown';
      case 4021:
        return 'Gypsy';
      case 4023:
        return 'Baby Novice';
      case 4024:
        return 'Baby Swordman';
      case 4025:
        return 'Baby Magician';
      case 4026:
        return 'Baby Archer';
      case 4027:
        return 'Baby Acolyte';
      case 4028:
        return 'Baby Merchant';
      case 4029:
        return 'Baby Thief';
      case 4030:
      case 4036:
        return 'Baby Knight';
      case 4031:
        return 'Baby Priest';
      case 4032:
        return 'Baby Wizard';
      case 4033:
        return 'Baby Blacksmith';
      case 4034:
        return 'Baby Hunter';
      case 4035:
        return 'Baby Assassin';
      case 4037:
      case 4044:
        return 'Baby Crusader';
      case 4038:
        return 'Baby Monk';
      case 4039:
        return 'Baby Sage';
      case 4040:
        return 'Baby Rogue';
      case 4041:
        return 'Baby Alchemist';
      case 4042:
        return 'Baby Bard';
      case 4043:
        return 'Baby Dancer';
      case 4045:
        return 'Baby Super Novice';
      case 4046:
        return 'Taekwon';
      case 4047:
        return 'Star Gladiator';
      case 4048:
        return 'Star Gladiator (Union)';
      case 4049:
        return 'Soul Linker';
      case 4050:
        return 'Gangsi (Bongun/Munak)';
      case 4051:
        return 'Death Knight';
      case 4052:
        return 'Dark Collector';
      case 4053:
      case 4080:
        return 'Rune Knight (Regular)';
      case 4055:
        return 'Warlock (Regular)';
      case 4056:
      case 4084:
        return 'Ranger (Regular)';
      case 4057:
        return 'Arch Bishop (Regular)';
      case 4058:
      case 4086:
        return 'Mechanic (Regular)';
      case 4059:
        return 'Guillotine Cross (Regular)';
      case 4060:
      case 4081:
        return 'Rune Knight (Trans)';
      case 4061:
        return 'Warlock (Trans)';
      case 4062:
      case 4085:
        return 'Ranger (Trans)';
      case 4063:
        return 'Arch Bishop (Trans)';
      case 4064:
      case 4087:
        return 'Mechanic (Trans)';
      case 4065:
        return 'Guillotine Cross (Trans)';
      case 4066:
      case 4082:
        return 'Royal Guard (Regular)';
      case 4067:
        return 'Sorcerer (Regular)';
      case 4068:
        return 'Minstrel (Regular)';
      case 4069:
        return 'Wanderer (Regular)';
      case 4070:
        return 'Sura (Regular)';
      case 4071:
        return 'Genetic (Regular)';
      case 4072:
        return 'Shadow Chaser (Regular)';
      case 4073:
      case 4083:
        return 'Royal Guard (Trans)';
      case 4074:
        return 'Sorcerer (Trans)';
      case 4075:
        return 'Minstrel (Trans)';
      case 4076:
        return 'Wanderer (Trans)';
      case 4077:
        return 'Sura (Trans)';
      case 4078:
        return 'Genetic (Trans)';
      case 4079:
        return 'Shadow Chaser (Trans)';
      case 4096:
      case 4109:
        return 'Baby Rune Knight';
      case 4097:
        return 'Baby Warlock';
      case 4098:
      case 4111:
        return 'Baby Ranger';
      case 4099:
        return 'Baby Arch Bishop';
      case 4100:
      case 4112:
        return 'Baby Mechanic';
      case 4101:
        return 'Baby Guillotine Cross';
      case 4102:
      case 4110:
        return 'Baby Royal Guard';
      case 4103:
        return 'Baby Sorcerer';
      case 4104:
        return 'Baby Minstrel';
      case 4105:
        return 'Baby Wanderer';
      case 4106:
        return 'Baby Sura';
      case 4107:
        return 'Baby Genetic';
      case 4108:
        return 'Baby Shadow Chaser';
      case 4190:
        return 'Super Novice (Expanded)';
      case 4191:
        return 'Super Baby (Expanded)';
      case 4211:
        return 'Kagerou';
      case 4212:
        return 'Oboro';
      case 4215:
        return 'Rebellion';
      case 4218:
        return 'Summoner';
      case 4220:
        return 'Baby Summoner';
      case 4222:
        return 'Baby Ninja';
      case 4223:
        return 'Baby Kagerou';
      case 4224:
        return 'Baby Oboro';
      case 4225:
        return 'Baby Taekwon';
      case 4226:
        return 'Baby Star Gladiator';
      case 4227:
        return 'Baby Soul Linker';
      case 4228:
        return 'Baby Gunslinger';
      case 4229:
        return 'Baby Rebellion';
      case 4238:
        return 'Baby Star Gladiator (Union)';
      case 4239:
        return 'Star Emperor';
      case 4240:
        return 'Soul Reaper';
      case 4241:
        return 'Baby Star Emperor';
      case 4242:
        return 'Baby Soul Reaper';
    }
  }

})(jQuery);