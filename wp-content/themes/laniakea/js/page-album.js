(function($, window, document, undefined) {
  'use strict';

  if ($('#album-can-vote').length > 0) {
    let $vote = new Vue({
      el: '#album-can-vote',
      data: {
        facebookID: '',
        facebookName: '',
        albumVotes: 0,
        albumId: 0,
        albumAuthor: '',
        progress: false
      },
      methods: {
        preVote(albumId = 0, albumAthor = '') {
          const _this = this;

          if ((_this.albumId === 0 && _this.albumAuthor === '') || (albumId > 0 && albumId !== _this.albumId)) {
            _this.albumId = albumId;
            _this.albumAuthor = albumAthor;
          }

          if (_this.progress)
            return;

          if (_this.facebookID === '')
            return _this.fbLogin();

          Swal.fire({
            title: 'Xác nhận bình chọn cho ' + _this.albumAuthor,
            text: 'Một khi bình chọn xong, bạn không thể thay đổi!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Đồng ý bình chọn',
            cancelButtonText: 'Hủy'
          }).then((result) => {
            if (result.value) {
              _this.vote();
            }
          });
        },
        vote() {
          const _this = this;

          $.ajax({
            url: TEE.ajaxurl,
            type: 'post',
            dataType: 'json',
            data: {
              action: 'tee_ajax_function',
              method: 'vote_album',
              albumId: _this.albumId,
              type: 'facebook',
              name: _this.facebookName,
              value: _this.facebookID,
              n: TEE.n
            },
            beforeSend() {
              _this.onProgress(true);
            },
            success(response) {
              _this.onProgress(false);
              if (typeof response.status !== 'undefined') {

                const { status, title, message } = response;

                Swal.fire({
                  type: status === 'OK' ? 'success' : 'error',
                  title,
                  text: message
                });

                if (status === 'OK') {
                  _this.albumVotes = response.albumVotes;
                }
              }
            },
            error() {
              _this.onProgress(false);
              Swal.fire({
                icon: 'error',
                title: 'Oops!',
                text: 'Có lỗi xảy ra... Hãy tải lại trang hoặc thử lại sau.'
              });
            }
          });
        },
        fbLogin() {
          const _this = this;

          FB.login(response => {
            if (response.authResponse) {
              FB.api('/me', function(response) {
                _this.facebookID = response.id;
                _this.facebookName = response.name;

                // callback Vote
                _this.preVote();
              });
            } else {
              console.log('User cancelled login or did not fully authorize.');
            }
          });
        },
        fbLogout() {
          const _this = this;

          FB.logout(() => {
            _this.facebookID = '';
            _this.facebookName = '';
          });
        },
        onProgress(onProgress) {
          this.progress = onProgress;
          $.LoadingOverlay(onProgress ? 'show' : 'hide');
        }
      },
      mounted() {
        initCube();

        const $votesNum = $('#albumVotes');
        if ($votesNum.length > 0) {
          this.albumVotes = $votesNum.val();
        }

        $.getScript('https://connect.facebook.net/vn_VN/sdk.js', function() {
          FB.init({
            appId: '422381378449138',
            autoLogAppEvents: true,
            version: 'v5.0',
            xfbml: true,
          });

          FB.getLoginStatus(response => {
            if (response.status === 'connected') {
              FB.api('/me', response => {
                $vote.facebookID = response.id;
                $vote.facebookName = response.name;
              });
            }
          });

        });
      }
    });
  }

  function initCube() {
    // init cubeportfolio
    $('#grid-container').cubeportfolio({
      layoutMode: 'grid',
      defaultFilter: '*',
      animationType: 'slideLeft',
      gapHorizontal: 15,
      gapVertical: 15,
      gridAdjustment: 'responsive',
      mediaQueries: [{
        width: 1500,
        cols: 5,
      }, {
        width: 1100,
        cols: 3,
      }, {
        width: 800,
        cols: 2,
      }, {
        width: 480,
        cols: 1
      }],
      caption: 'default',
      displayType: 'sequentially',
      displayTypeSpeed: 100,

      // singlePageInline
      singlePageInlineDelegate: '.cbp-singlePageInline',
      singlePageInlinePosition: 'below',
      singlePageInlineInFocus: true,
      singlePageInlineCallback: function(url, element) {
        //to update singlePageInline content use the following method: this.updateSinglePageInline(yourContent)
        let t = this;

        const index = $(element).data('index');
        const postID = $(element).data('post-id');

        $.ajax({
          url: TEE.ajaxurl,
          type: 'POST',
          dataType: 'html',
          data: {
            action: 'tee_ajax_function',
            method: 'load_single_image_content',
            index,
            postID,
            n: TEE.n
          },
          success(response) {
            t.updateSinglePageInline(response);
          },
          error() {
            t.updateSinglePageInline('Có lỗi xảy ra... Vui lòng tải lại trang.');
          }
        });
      }
    });
  }
})(jQuery, window, document);