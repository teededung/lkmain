(function($) {

  $('.col-socials a').tooltip();

  $('.count').each(function () {
    $(this).prop('Counter',0).animate({
      Counter: $(this).text()
    }, {
      duration: 4000,
      easing: 'swing',
      step: function (now) {
        $(this).text(Math.ceil(now));
      }
    });
  });

  const tl = new TimelineMax({ paused: true });

  tl.from('.col-char i', 1, { fontSize: '1px', ease: Elastic.easeOut })
  .from('.col-login i', 1, { fontSize: '1px', ease: Elastic.easeOut })
  .from('.col-map i', 1, { fontSize: '1px', ease: Elastic.easeOut });

  setTimeout(() => {
    tl.play();
  }, 2500);

})(jQuery);
