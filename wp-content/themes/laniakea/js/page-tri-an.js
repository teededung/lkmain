(function($) {
  const messages = {
    _default: (field) => `Giá trị của ${field} không đúng`,
    after: (field, [target]) => `${field} phải xuất hiện sau ${target}`,
    alpha: (field) => `${field} chỉ có thể chứa các kí tự chữ`,
    alpha_dash: (field) => `${field} có thể chứa các kí tự chữ (A-Z a-z), số (0-9), gạch ngang (-) và gạch dưới (_)`,
    alpha_num: (field) => `${field} chỉ có thể chứa các kí tự chữ và số`,
    alpha_spaces: (field) => `${field} chỉ có thế chứa các kí tự và khoảng trắng`,
    before: (field, [target]) => `${field} phải xuất hiện trước ${target}`,
    between: (field, [min, max]) => `${field} phải có giá trị nằm trong khoảng giữa ${min} và ${max}`,
    confirmed: (field, [confirmedField]) => `${field} khác với ${confirmedField}`,
    credit_card: (field) => `Đã điền ${field} không chính xác`,
    date_between: (field, [min, max]) => `${field} phải có giá trị nằm trong khoảng giữa  ${min} và ${max}`,
    date_format: (field, [format]) => `${field} phải có giá trị dưới định dạng ${format}`,
    decimal: (field, [decimals = '*'] = []) => `${field} chỉ có thể chứa các kí tự số và dấu thập phân${!decimals || decimals === '*' ? '' : ' ' + decimals}`,
    digits: (field, [length]) => `Trường ${field} chỉ có thể chứa các kí tự số và bắt buộc phải có độ dài là ${length}`,
    dimensions: (field, [width, height]) => `${field} phải có chiều rộng ${width} pixels và chiều cao ${height} pixels`,
    email: (field) => `${field} phải là một địa chỉ email hợp lệ`,
    excluded: (field) => `${field} phải chứa một giá trị hợp lệ`,
    ext: (field) => `${field} phải là một tệp`,
    image: (field) => `Trường ${field} phải là một ảnh`,
    included: (field) => `${field} phải là một giá trị`,
    ip: (field) => `${field} phải là một địa chỉ ip hợp lệ`,
    max: (field, [length]) => `${field} không thể có nhiều hơn ${length} kí tự`,
    max_value: (field, [max]) => `${field} phải nhỏ hơn hoặc bằng ${max}`,
    mimes: (field) => `${field} phải chứa kiểu tệp phù hợp`,
    min: (field, [length]) => `${field} phải chứa ít nhất ${length} kí tự`,
    min_value: (field, [min]) => `${field} phải lớn hơn hoặc bằng ${min}`,
    numeric: (field) => `${field} chỉ có thể có các kí tự số`,
    regex: (field) => `${field} có định dạng không đúng`,
    required: (field) => `${field} là bắt buộc`,
    size: (field, [size]) => `${field} chỉ có thể chứa tệp nhỏ hơn ${formatFileSize(size)}`,
    url: (field) => `${field} không phải là một địa chỉ URL hợp lệ`
  };

  const locale = {
    name: 'vi',
    messages,
    attributes: {}
  };

  VeeValidate.Validator.localize({ [locale.name]: locale });

  Vue.use(VeeValidate, {
    locale: 'vi'
  });

  let $vm = new Vue({
    el: '#app',
    data: {
      username: '',
      password: '',
      auth: '',
      time: '',
      submitting: false,
      ariaValuenow: 0,
      step: 1,
      q1: 0,
      q2: 0,
      q3: 0,
      q4: 0,
      q5: 0,
      q6: 0,
      q7: 0,
      q8: 0,
      q9: 0,
      q10: 0,
      q6Text: '',
    },
    methods: {
      login() {
        const _this = this;

        $.ajax({
          url: TEE.ajaxurl,
          type: 'post',
          dataType: 'json',
          data: {
            action: 'tee_ajax_function',
            method: 'ragnarok_login',
            username: _this.username,
            password: _this.password,
            n: TEE.n
          },
          beforeSend() {
            _this.submitting = true;
          },
          success(response) {
            _this.submitting = false;

            if (typeof response.status !== 'undefined') {
              const status = response.status;
              if (status === 'OK') {
                _this.auth = response['laniakea'];
                _this.submit();
                toastr["success"]("Đăng nhập thành công! Đang nhận quà...");
              } else {
                _this.password = '';
                $vm.errors.clear();
                showError(response.message);
              }
            } else {
              showError('Vui lòng thử lại sau hoặc liên hệ GM.');
            }
          },
          error(e) {
            $.LoadingOverlay('hide');
            showError('Vui lòng thử lại sau hoặc liên hệ GM.');
          }
        });
      },
      submit() {
        const _this = this;

        $.ajax({
          url: TEE.ajaxurl,
          type: 'post',
          dataType: 'json',
          data: {
            action: 'tee_ajax_function',
            method: 'get_anniversary_gift',
            data: {
              auth: _this.auth,
              gift: {
                newbie: _this.q2,
                pvp: _this.q3,
                upper: Number(_this.q4),
                aura: Number(_this.q5),
                message: Number(_this.q6),
                messageText: _this.q6Text
              }
            },
            n: TEE.n
          },
          beforeSend() {
            _this.submitting = true;
          },
          success(response) {
            _this.submitting = false;

            if (typeof response.status !== 'undefined') {
              const status = response.status;
              if (status === 'OK') {
                Swal.fire({
                  type: 'success',
                  title: 'Tuyệt vời!',
                  text: response.message
                }).then(() => {
                  _this.step++;
                });
              } else {
                showError(response.message);
              }
            }
          },
          error(e) {
            _this.submitting = false;
          }
        });
      },
      getAuraName() {
        switch (Number(this.q5)) {
          case 1:
            return '[Aura Star - 0]';
          case 2:
            return '[Aura Fish - 3]';
          case 3:
            return '[Aura Colorful - 36]';
          case 4:
            return '[Aura Flowery - 24]';
          case 5:
            return '<img src="https://api.hahiu.com/data/items/icons/51504.png" alt="SeasonBox">&nbsp;<span class="item-name">Hộp Aura ngẫu nhiên</span> x1';
        }
      },
      getItemInfoQ4() {
        let item = {};
        switch (Number(this.q4)) {
          case 1:
            item = {
              id: 41077,
              name: 'Costume Bunny Hood (White)'
            };
            break;
          case 2:
            item = {
              id: 42367,
              name: 'Costume Tail Hat (Purple)'
            };
            break;
          case 3:
            item = {
              id: 41130,
              name: 'Costume Cat Hood (Pink)'
            };
            break;
          case 4:
            item = {
              id: 42118,
              name: 'Costume Archangeling Hairband (Yellow)'
            };
            break;
          case 5:
            item = {
              id: 31794,
              name: 'Costume Puppy Ears Hat'
            };
            break;
        }

        return item;
      },
      updateProgress() {
        const _this = this;
        let percent = 0;
        if (_this.q1 !== 0) {
          percent += 10;
        }
        if (_this.q2 !== 0) {
          percent += 10;
        }
        if (_this.q3 !== 0) {
          percent += 10;
        }
        if (_this.q4 !== 0) {
          percent += 10;
        }
        if (_this.q5 !== 0) {
          percent += 10;
        }
        if (_this.q6 !== 0) {
          percent += 10;
        }
        if (_this.q7 !== 0) {
          percent += 10;
        }
        if (_this.q8 !== 0) {
          percent += 10;
        }
        if (_this.q9 !== 0) {
          percent += 10;
        }
        if (_this.q10 !== 0) {
          percent += 10;
        }

        _this.ariaValuenow = percent;
      }
    },
    watch: {
      submitting(submitting) {
        if (submitting) {
          $('#login-form').LoadingOverlay('show');
        } else {
          $('#login-form').LoadingOverlay('hide');
        }
      },
      step(step) {
        const _this = this;

        if (step === 2)
          _this.q1 = 1;

        if (step === 9)
          _this.q9 = 1;

        if (step === 10)
          _this.q10 = 1;

        _this.updateProgress();
      }
    },
    mounted() {
      updateTime(this);
    }
  });

  function updateTime(vm) {
    setInterval(function() {
      let cd = new Date();
      vm.time = zeroPadding(cd.getHours(), 2) + ' giờ ' + zeroPadding(cd.getMinutes(), 2) + ' phút ' + zeroPadding(cd.getSeconds(), 2) + ' giây';
    }, 1000);

  }

  function zeroPadding(num, digit) {
    let zero = '';
    for(let i = 0; i < digit; i++) {
      zero += '0';
    }
    return (zero + num).slice(-digit);
  }

  function showError(message) {
    Swal.fire({
      type: 'error',
      title: 'Oops...',
      text: message,
      footer: 'Nếu bạn cần hỗ trợ. Hãy nhắn tin cho LaniakeaRO qua&nbsp;<a href="https://m.me/laniakea.ro/">Fanpage</a>.',
    });
  }

})(jQuery);