<?php
if (isset($_COOKIE['laniakeaA'])):
    $auth = $_COOKIE['laniakeaA'];
    $account = ragnarok_login_by_auth($auth);

    if (is_data_return_ok($account)):
        wp_redirect(home_url('/tai-khoan/'), 301);
        exit;
    else:
        setcookie('laniakeaA', null, -1, '/');
        header("Refresh:0");
        exit;
    endif;
endif;
?>

<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>
	<div class="container content-md">
        <h1>Đăng ký tài khoản</h1>

        <?php
        $allow_register = get_field('allow_register','option');
        $message = get_field('message_register', 'option');
        ?>

        <?php if ($message): ?>
            <div class="alert alert-info margin-bottom-30">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>

        <?php if ($allow_register): ?>
		<form id="register-form" @submit.prevent="register">
            <template v-if="!registered">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" v-model="email" data-vv-name="email" data-vv-as="Email" data-vv-delay="1000" v-validate="'required|email'" :class="{'is-invalid': errors.has('email') }" class="form-control input" id="email" aria-describedby="emailHelp" placeholder="Nhập địa chỉ email của bạn">
                    <small id="emailHelp" class="form-text text-muted">Email dùng để xóa nhân vật hoặc lấy lại mật khẩu.</small>
                    <div class="invalid-feedback" v-if="errors.has('email')">
                        {{errors.first('email')}}
                    </div>
                </div>

                <div class="form-group">
                    <label for="username">Tên đăng nhập</label>
                    <input type="text" name="username" v-model="username" @keydown.space.prevent data-vv-name="username" data-vv-as="Tên đăng nhập" data-vv-delay="1000" v-validate="'required|min:6|max:30'" min="4" max="30" :class="{'is-invalid' : errors.has('username')}" class="form-control input" id="username" placeholder="Nhập tên đăng nhập">
                    <div class="invalid-feedback" v-if="errors.has('username')">
                        {{ errors.first('username') }}
                    </div>
                </div>

                <div class="form-group">
                    <label for="password">Mật khẩu</label>
                    <input type="password" name="password" v-model="password" data-vv-name="password" data-vv-as="Mật khẩu" data-vv-delay="1000" v-validate="'required|min:6|max:30'" min="6" max="30" :class="{'is-invalid' : errors.has('password')}" class="form-control input" id="password" placeholder="Nhập mật khẩu của bạn" ref="password">
                    <div class="invalid-feedback" v-if="errors.has('password')">
                        {{ errors.first('password') }}
                    </div>
                </div>

                <div class="form-group">
                    <label for="passwordConfirm">Nhập lại mật khẩu</label>
                    <input type="password" name="passwordConfirm" v-model="passwordConfirm" data-vv-name="passwordConfirm" data-vv-as="Xác nhận mật khẩu" v-validate="'required|confirmed:password'" data-vv-delay="1000" min="6" max="30" :class="{'is-invalid' : errors.has('passwordConfirm')}" class="form-control input" id="passwordConfirm" placeholder="Nhập lại mật khẩu của bạn">
                    <div class="invalid-feedback" v-if="errors.has('passwordConfirm')">
                        {{ errors.first('passwordConfirm') }}
                    </div>
                </div>

                <div class="mb-3">
                    <div class="g-recaptcha" data-callback="enableSubmit" data-expired-callback="disableSubmit" data-sitekey="6LfD60YUAAAAAMQtwQxtKy95L0M-p3KUTiJ68n43"></div>
                </div>

                <button type="submit" :disabled="!captcha" class="btn btn-primary">{{ getSubmitButtonText() }}</button>
            </template>
			<template v-else>
                <h3>Đăng ký thành công!</h3>
            </template>
		</form>
        <?php endif; ?>
	</div>
<?php endwhile; ?>
<?php get_footer(); ?>
