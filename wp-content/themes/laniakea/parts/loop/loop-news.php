<!-- Blog Post -->
<div class="card mb-4">
	<?php if (has_post_thumbnail()): ?>
		<?php the_post_thumbnail('large', array('class' => 'card-img-top')); ?>
	<?php else: ?>
		<img class="card-img-top img-fluid" src="http://placehold.it/750x300" alt="Chưa có hình">
	<?php endif; ?>
	<div class="card-body">
		<h2 class="card-title">
            <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
			    <?php the_title(); ?>
            </a>
        </h2>

		<div class="card-text">
			<?php the_excerpt(); ?>
		</div>
		<a href="<?php the_permalink(); ?>" class="btn btn-primary">Đọc tiếp &rarr;</a>
	</div>
	<div class="card-footer text-muted">
		<?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' trước'; ?>
	</div>
</div>