<?php
$to_left = get_field('to_left');
$class_name = ($to_left == 1) ? 'frst-odd-item' : 'frst-even-item';
$icon_text = get_field('icon_class_name');
?>

<div class="frst-timeline-block <?php echo $class_name; ?>">
	<div class="frst-timeline-img">
		<span><i class="<?php echo $icon_text; ?>"></i></span>
	</div>

	<div class="frst-timeline-content">
		<div class="frst-timeline-content-inner">
			<?php
			if (has_post_thumbnail()):
				the_post_thumbnail('update-thumbnail');
			endif;
			?>
			<span class="frst-date"><i class="icon-clock"></i><?php echo get_the_date(); ?></span>
			<div class="text-content">
				<h2><?php the_title(); ?></h2>
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</div>