<div class="cbp-item">
    <a href="<?php the_permalink() ?>">
        <?php if (has_post_thumbnail()): ?>
            <?php the_post_thumbnail('full', array('class' => 'img-thumbnail')); ?>
        <?php endif; ?>
        <h3 class="post-album-title">
            <?php the_title(); ?>
        </h3>
    </a>
</div>