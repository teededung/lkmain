<div class="cbp-item">
    <a href="<?php the_permalink() ?>">
        <div class="cbp-caption">
            <div class="cbp-caption-defaultWrap">
                <?php if (has_post_thumbnail()): ?>
                    <?php the_post_thumbnail('full'); ?>
                <?php endif; ?>
            </div>
        </div>
        <h3 class="post-costume-title">
            <?php the_title(); ?>
        </h3>
    </a>
</div>