<?php
/**
 * @var $can_vote
 */
?>
<div class="cbp-item">
    <a href="<?php the_permalink() ?>">
        <?php if (has_post_thumbnail()): ?>
            <?php the_post_thumbnail('medium', array('class' => 'img-thumbnail')); ?>
        <?php else: ?>
            <?php $gallery = get_field('gallery'); ?>
            <?php if ($gallery): ?>
                <img class="img-thumbnail" src="<?php echo $gallery[0]['url']; ?>" alt="<?php echo $gallery[0]['alt']; ?>">
            <?php endif; ?>
        <?php endif; ?>

        <h3 class="post-album-title">
            <?php $author = get_field('char_name'); ?>
            <?php if ($author): ?>
            <span>Album của</span>&nbsp;<span class="char-name"><?php the_field('char_name'); ?></span>
            <?php else: ?>
            <span>Tổng hợp bởi LaniakeaRO</span>
            <?php endif; ?>
        </h3>
    </a>

    <?php if ($can_vote): ?>
    <a class="a-vote" href="#" @click.prevent="preVote(<?php echo get_the_ID() ?>, `<?php echo $author; ?>`)" ><i class="ion-ios-heart"></i></a>
    <?php endif; ?>
</div>