<div class="sidebar-search">
	<div class="card">
		<h5 class="card-header">Tìm kiếm</h5>
		<div class="card-body">
			<form action="<?php echo home_url('/tim-kiem/'); ?>" method="get" id="search-form">
				<div class="input-group">
					<input type="text" name="keyword" class="form-control" placeholder="Từ khóa...">
					<div class="input-group-append">
						<button class="btn btn-secondary" type="submit">Tìm</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

