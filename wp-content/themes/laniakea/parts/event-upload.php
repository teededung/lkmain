<h2 class="mb-3">Sự kiện</h2>

<?php
$args = array(
    'post_type' => 'albums',
    'post_status' => array('pending'),
    'posts_per_page' => 1,
    'meta_query' => array(
        'relation' => 'AND',
        array(
            'key'	 	=> 'account_id',
            'value'	  	=> $account_id,
            'compare' 	=> '=',
        ),
    )
);

$album_object = new WP_Query($args);
?>

<?php if ($album_object->have_posts()): ?>
    <?php while ($album_object->have_posts()): $album_object->the_post(); ?>
        <div class="card border-primary">
            <div class="card-body">
                <h5 class="card-title">Sự kiện "Tự sướng Halloween 2019"</h5>
                <?php $gallery_items = get_field('gallery'); ?>

                <?php if ($gallery_items): ?>
                    <?php $count = count($gallery_items); ?>
                    <?php if ($count < 10): ?>
                        <p class="card-text">Bạn đã đăng <?php echo $count; ?> ảnh vào album "Tự sướng Halloween 2019". Bạn có thể thêm được <?php echo (10 - $count); ?> ảnh nữa.</p>
                    <?php else: ?>
                        <p class="card-text">Bạn đã đăng <?php echo $count; ?> ảnh vào album "Tự sướng Halloween 2019". Bạn không thể đăng thêm ảnh được nữa.</p>
                    <?php endif; ?>
                    <div class="cbp mb-3" id="grid-container">
                        <?php foreach ($gallery_items as $img): ?>
                            <div class="cbp-item cbp-item-album" id="attach-<?php echo $img['id']; ?>">
                                <button class="delete-img" @click="deleteImage(<?php echo $img['id'] ?>)">&#10006;</button>
                                <div class="cbp-caption">
                                    <div class="cbp-caption-defaultWrap">
                                        <a class="cbp-lightbox" href="<?php echo $img['url'] ?>" data-title="<?php echo $img['description']; ?>">
                                            <img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>" width="100%">
                                        </a>
                                    </div>
                                    <div class="cbp-caption-activeWrap">
                                        <h3><?php echo str_replace("Halloween 2019 -", "", $img['title']); ?></h3>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php else: ?>
                    <div class="alert alert-info">
                        Bạn chưa có ảnh nào trong album "Halloween 2019".
                    </div>
                <?php endif; ?>

                <button class="btn btn-primary mb-3" data-toggle="collapse" data-target="#collapse-img-form" aria-expanded="false" aria-controls="collapse-img-form">Thêm ảnh vào album Halloween 2019 của bạn.</button>

                <div class="collapse" id="collapse-img-form">
                    <form @submit.prevent="submit" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="file">Hình ảnh:</label>
                            <input class="slim"
                                   type="file"
                                   data-label="Kéo thả hình vào đây hoặc click để chọn hình"
                                   data-fetcher="<?php echo get_template_directory_uri() . '/functions/slim/fetch.php' ?>"
                                   data-size="640,640"
                                   data-ratio="free"
                                   data-button-confirm-label="Xác nhận"
                                   data-button-cancel-label="Hủy"
                                   data-button-edit-title="Sửa"
                                   data-button-remove-title="Xóa"
                                   data-button-rotate-title="Xoay"
                                   data-label-loading="'Đang xử lý...'"
                                   data-max-file-size="8"
                                   data-status-file-size="Dung lượng hình quá lớn. Vui lòng chọn hình nhỏ hơn 8MB"
                                   data-status-file-type="Định dạng hình ảnh phải là ($0)"
                                   data-post="input"
                                   data-did-load="slimCheck"
                                   data-vv-name="image"
                                   data-vv-as="Hình ảnh"
                            />
                            <div class="invalid-feedback d-block" v-if="errors.has('image')">
                                {{ errors.first('image') }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="imgTitle">Tiêu đề ảnh:</label>
                            <input class="form-control" id="imgTitle" data-vv-name="imgTitle" data-vv-as="Tiêu đề ảnh" v-validate="'required'" v-model="imgTitle" :class="{'is-invalid': errors.has('imgTitle')}" name="imgTitle" placeholder="Tiêu đề ảnh"/>
                            <div class="invalid-feedback" v-if="errors.has('imgTitle')">
                                {{ errors.first('imgTitle') }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="message">Nội dung giới thiệu cho ảnh:</label>
                            <textarea class="form-control" id="message" data-vv-name="imgDescription" data-vv-as="Nội dung giới thiệu" v-validate="'required'" v-model="imgDescription" :class="{'is-invalid': errors.has('imgDescription')}" name="imgDescription" placeholder="Nội dung giới thiệu cho ảnh" rows="3"></textarea>
                            <div class="invalid-feedback" v-if="errors.has('imgDescription')">
                                {{ errors.first('imgDescription') }}
                            </div>
                        </div>

                        <div class="text-right">
                            <button class="btn btn-primary" type="submit">Gửi</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php else: ?>
    <div class="card border-primary">
        <img class="card-img-top" src="<?php echo get_template_directory_uri() . '/images/event-halloween.jpg' ?>" alt="laniakea-ro-halloween-event-2019">
        <div class="card-body">
            <h5 class="card-title">Sự kiện "Tự sướng Halloween 2019"</h5>
            <p class="card-text">Để tham gia sự kiện, bạn hãy đăng tải hình ảnh Halloween của bạn ở Ragnarok Online. Sau khi ảnh được duyệt, ảnh của bạn sẽ được đăng vào album Halloween 2019 ở trang chủ LaniakeaRO. Xem chi tiết tại <a target="_blank" href="https://laniakea.hahiu.com/thong-bao/su-kien-tu-suong-halloween-2019/">đây</a>.</p>

            <button class="btn btn-primary mb-3" data-toggle="collapse" data-target="#collapse-img-form" aria-expanded="false" aria-controls="collapse-img-form">Tải ảnh lên</button>

            <div class="collapse" id="collapse-img-form">
                <form @submit.prevent="submit" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="file">Hình ảnh:</label>
                        <input class="slim"
                               type="file"
                               data-label="Kéo thả hình vào đây hoặc click để chọn hình"
                               data-fetcher="<?php echo get_template_directory_uri() . '/functions/slim/fetch.php' ?>"
                               data-size="640,640"
                               data-ratio="free"
                               data-button-confirm-label="Xác nhận"
                               data-button-cancel-label="Hủy"
                               data-button-edit-title="Sửa"
                               data-button-remove-title="Xóa"
                               data-button-rotate-title="Xoay"
                               data-label-loading="'Đang xử lý...'"
                               data-max-file-size="8"
                               data-status-file-size="Dung lượng hình quá lớn. Vui lòng chọn hình nhỏ hơn 8MB"
                               data-status-file-type="Định dạng hình ảnh phải là ($0)"
                               data-post="input"
                               data-did-load="slimCheck"
                               data-vv-name="image"
                               data-vv-as="Hình ảnh"
                        />
                        <div class="invalid-feedback d-block" v-if="errors.has('image')">
                            {{ errors.first('image') }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="imgCharName">Chọn nhân vật chính (dùng để cho các bạn khác biết tên nhân vật của bạn):</label>
                        <select class="form-control" name="author" id="imgCharName" v-model="imgCharName" v-validate="'required'" :class="{'is-invalid': errors.has('imgCharName')}" data-vv-name="imgCharName" data-vv-as="Chọn nhân vật chính">
                            <option disabled selected>-- Chọn nhân vật --</option>
                            <?php if ($row_chars): ?>
                                <?php foreach($row_chars as $char): ?>
                                    <option value="<?php echo $char->name; ?>"><?php echo $char->name; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                        <div class="invalid-feedback" v-if="errors.has('imgCharName')">
                            {{ errors.first('imgCharName') }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="imgTitle">Tiêu đề ảnh:</label>
                        <input class="form-control" id="imgTitle" data-vv-name="imgTitle" data-vv-as="Tiêu đề ảnh" v-validate="'required'" v-model="imgTitle" :class="{'is-invalid': errors.has('imgTitle')}" name="imgTitle" placeholder="Tiêu đề ảnh"/>
                        <div class="invalid-feedback" v-if="errors.has('imgTitle')">
                            {{ errors.first('imgTitle') }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="message">Nội dung giới thiệu cho ảnh:</label>
                        <textarea class="form-control" id="message" data-vv-name="imgDescription" data-vv-as="Nội dung giới thiệu" v-validate="'required'" v-model="imgDescription" :class="{'is-invalid': errors.has('imgDescription')}" name="imgDescription" placeholder="Nội dung giới thiệu cho ảnh" rows="3"></textarea>
                        <div class="invalid-feedback" v-if="errors.has('imgDescription')">
                            {{ errors.first('imgDescription') }}
                        </div>
                    </div>

                    <div class="text-right">
                        <button class="btn btn-primary" type="submit">Gửi</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
<?php endif; ?>