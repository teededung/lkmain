<div class="section mb-3" id="step-1" v-if="step === 1">
    <h3 class="text-primary-color mb-3">1. Quà tri ân 2 năm hoạt động từ LaniakeaRO</h3>

    <p>
        <strong>Ngày close beta:</strong> 02/03/2018<br>
        <strong>Ngày open beta:</strong> 07/03/2018<br>
        <?php
        $now = time(); // or your date as well
        $your_date = strtotime("2018-03-07");
        $datediff = $now - $your_date;

        $number_days = round($datediff / (60 * 60 * 24));
        ?>
        <strong>Tính từ open tới nay:</strong> <?php echo $number_days; ?> ngày
        <span class="time">{{ time }}</span>
    </p>

    <p class="lead">LaniakeaRO bước vào cộng đồng Ragnarok với mục tiêu duy trì lâu nhất?</p>
    <p class="lead">Với một Private Server thì khi cam kết rằng sẽ duy trì lâu nhất nó là câu nói rất bình thường.</p>
    <p class="lead">LaniakeaRO đã đặt ra mục tiêu đó là phải đạt mốc duy trì 2 năm, khi có một con số cụ thể ra thì sẽ rõ ràng hơn nhiều.</p>
    <p class="lead">
        Và thế là thời gian cứ trôi qua, đôi khi ta cũng muốn thời gian trôi chậm lại, đôi khi ta cũng muốn thời gian trôi qua nhanh...<br>
        Một ngày nọ, "ey, sắp tới sinh nhật LaniakeaRO tròn 2 năm kìa... Ối, chết mất, chưa chuẩn bị gì hết..."<br>
    </p>
    <p class="lead">...</p>

    <h5 class="text-secondary-color mb-2">Quà tặng cho những bạn đang xem trang này:</h5>
    <ul class="border rounded bg-primary-light p-3 pl-4">
        <li class="mb-2">
            <?php item_icon(12210); ?>&nbsp;<span class="item-name">Bubble Gum</span>&nbsp;<span>x5</span><br>
            <small>Nhân đôi tỉ lệ rơi vật phẩm trong 30 phút.</small>
        </li>
        <li class="mb-2">
            <?php item_icon(12208); ?>&nbsp;<span class="item-name">Battle Manual</span>&nbsp;<span>x5</span><br>
            <small>Tăng thêm 50% EXP khi tiêu diệt quái vật trong 30 phút.</small>
        </li>
        <li class="mb-2">
            <?php item_icon(25274); ?>&nbsp;<span class="item-name">Vé VIP 7 ngày</span>&nbsp;<span>x2</span><br>
            <small>Kích hoạt 7 ngày sử dụng tài khoản VIP. Đọc thêm về <a target="_blank" href="https://laniakea.hahiu.com/thong-bao/thong-vip-cua-laniakea-ro-co-gi-hot/">Tài khoản VIP khác gì với tài khoản thường?</a></small>
        </li>
        <li class="mb-2">
            <strong>1,000 Free cash</strong><br>
            <small>Dùng để mua vật phẩm ở Cashshop.</small>
        </li>
    </ul>

    <nav class="__page" aria-label="page navigation">
        <ul class="pagination justify-content-center">
            <li class="page-item">
                <a class="page-link" href="#" @click.prevent="step++">Các quà tặng tiếp theo</a>
            </li>
        </ul>
    </nav>
</div>