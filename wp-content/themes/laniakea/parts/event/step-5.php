<div v-cloak class="section mb-5" id="step-5" v-show="step === 5">
    <h3 class="text-primary-color mb-3">5. Chọn 1 Aura mà bạn thích</h3>

    <div class="form-option-question mb-3">
        <div class="row">
            <div class="col-md-5">
                <p class="lead">
                    Hệ thống Aura dưới chân không còn xa lạ gì với các bạn đã từng chơi các Private Server. <br>
                    Theo nhiều request thì LaniakeaRO cũng cập nhật cho server và cũng làm ra những Aura riêng biệt.
                </p>

                <div class="form-check">
                    <input class="form-check-input" type="radio" v-model="q5" name="q5" id="q5-1" value="1">
                    <label class="form-check-label" for="q5-1">
                        Star - 0
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" v-model="q5" name="q5" id="q5-2" value="2">
                    <label class="form-check-label" for="q5-2">
                        Fish - 3
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" v-model="q5" name="q5" id="q5-3" value="3">
                    <label class="form-check-label" for="q5-3">
                        Colorful - 36
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" v-model="q5" name="q5" id="q5-4" value="4">
                    <label class="form-check-label" for="q5-4">
                        Flowery - 24
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" v-model="q5" name="q5" id="q5-5" value="5">
                    <label class="form-check-label" for="q5-5">
                        <img src="<?php echo get_template_directory_uri() . '/images/icons/SeasonBox.png' ?>" alt="SeasonBox">&nbsp;<span class="item-name">Hộp Aura ngẫu nhiên</span>
                    </label>
                </div>
            </div>
            <div class="col-md-7">
                <template v-if="q5">
                    <h5 class="text-secondary-color mb-2">Quà tặng tương ứng với lựa chọn này:</h5>

                    <ul class="list-unstyled">
                        <li class="mb-2" v-show="q5 == 1">
                            <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/event/0-star-showcase-style2.jpg' ?>" alt="">
                        </li>
                        <li class="mb-2" v-show="q5 == 2">
                            <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/event/3-fish-showcase-style2.jpg' ?>" alt="">
                        </li>
                        <li class="mb-2" v-show="q5 == 3">
                            <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/event/36-colorful-showcase.jpg' ?>" alt="">
                        </li>
                        <li class="mb-2" v-show="q5 == 4">
                            <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/event/24-flowery-showcase-style2.jpg' ?>" alt="">
                        </li>
                        <li class="mb-2" v-show="q5 == 5">
                            Nhận <?php item_icon(51504); ?>&nbsp;<span class="item-name">Hộp Aura ngẫu nhiên</span>. Khi mở hộp bạn sẽ nhận được 1 Aura ngẫu nhiên trong 200 aura độc từ LaniakeaRO!
                        </li>
                    </ul>

                    <span v-if="q5 != 5">Bạn sẽ mở khóa được Aura <strong>{{ getAuraName() }}</strong> và 7 ngày năng lượng (Bạn có thể sạc thêm Năng lượng bằng cách tiêu diệt MvP, tham gia Battleground).</span>
                </template>
            </div>
        </div>
    </div>

    <nav class="__page" aria-label="page navigation">
        <ul class="pagination justify-content-center">
            <li class="page-item">
                <a class="page-link" href="#" @click.prevent="step--">Quay lại</a>
            </li>
            <li class="page-item" :class="{'disabled' : !q5}">
                <a class="page-link" href="#" @click.prevent="step++">Tiếp tục</a>
            </li>
        </ul>
    </nav>
</div>