<div v-cloak class="mb-3" v-show="step === 10">
    <div class="row mt-5 text-center align-items-center justify-content-center">
        <div class="col-sm-12">
            <h3 class="text-primary-color mb-3">Nhận quà thành công!</h3>
        </div>
        <div class="col-sm-12">
            <p class="lead">Ngay bây giờ bạn đã có thể đăng nhập vào game và nhận quà từ Nhân viên VIP.</p>
        </div>
        <div class="col-sm-12">
            <p class="lead">Chúc bạn có những giây phút trải nghiệm tuyệt vời ở LaniakeaRO.</p>
        </div>
    </div>
</div>