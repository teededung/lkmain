<div v-cloak class="section mb-5" id="step-6" v-show="step === 6">
    <h3 class="text-primary-color mb-3">6. Gửi thông điệp của bạn đến với mọi người</h3>

    <div class="form-option-question mb-3">
        <div class="row">
            <div class="col-md-7">
                <p class="lead">
                    LaniakeaRO cũng nói nhiều rồi, giờ là tới phiên bạn nói. Hãy gửi thông điệp của bạn cho những bạn khác :))
                </p>

                <div class="form-check">
                    <input class="form-check-input" type="radio" v-model="q6" name="q6" id="q6-1" value="1">
                    <label class="form-check-label" for="q6-1">
                        Chúc các roker đang FA mau sớm có bồ.
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" v-model="q6" name="q6" id="q6-2" value="2">
                    <label class="form-check-label" for="q6-2">
                        Mong các bạn trong server yêu quý nhau hơn, giúp đỡ newbie nhiều hơn.
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" v-model="q6" name="q6" id="q6-3" value="3">
                    <label class="form-check-label" for="q6-3">
                        Mong LaniakeaRO ra nhiều sự kiện và nhiều quà hơn.
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" v-model="q6" name="q6" id="q6-4" value="4">
                    <label class="form-check-label" for="q6-4">
                        Gửi thông điệp riêng.
                    </label>
                    <div class="mt-1">
                        <input v-if="q6 == 4" type="text" v-model="q6Text" class="form-control" placeholder="Ghi thông điệp của bạn muốn gửi cho mọi người" value="">
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <template v-if="q6">
                    <h5 class="text-secondary-color mb-2">Quà tặng tương ứng với lựa chọn này:</h5>

                    <ul class="list-unstyled">
                        <li class="mb-2" v-show="q6 == 1">
                            <p>Bạn nhận được x1 ngọc cường hóa costume hiệu ứng <strong>Hoa hồng lãng mạn</strong> vị trí ngẫu nhiên (Upper, Middle hoặc Lower)</p>
                            <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/event/rose.gif' ?>" alt="rose">
                        </li>
                        <li class="mb-2" v-show="q6 == 2">
                            <p>Bạn nhận được x1 ngọc cường hóa costume hiệu ứng <strong>Popping Ống thuốc trắng</strong> vị trí ngẫu nhiên (Upper, Middle hoặc Lower).</p>
                            <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/event/slim.gif' ?>" alt="slim">
                        </li>
                        <li class="mb-2" v-show="q6 == 3">
                            <p>Bạn nhận được x1 ngọc cường hóa costume hiệu ứng <strong>Popping Hộp quà</strong> vị trí ngẫu nhiên (Upper, Middle hoặc Lower).</p>
                            <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/event/gif.gif' ?>" alt="giftbox">
                        </li>
                        <li class="mb-2" v-show="q6 == 4">
                            <p>1 trứng thú cưng ngẫu nhiên chưa cập nhật vào server. Có kèm theo trang sức :3</p>
                        </li>
                    </ul>
                </template>
            </div>
        </div>

    </div>

    <nav class="__page" aria-label="page navigation">
        <ul class="pagination justify-content-center">
            <li class="page-item">
                <a class="page-link" href="#" @click.prevent="step--">Quay lại</a>
            </li>
            <li class="page-item" :class="{ 'disabled' : !q6 || (q6 == 4 && q6Text.trim() === '') }">
                <a class="page-link" href="#" @click.prevent="step++">Tiếp tục</a>
            </li>
        </ul>
    </nav>
</div>