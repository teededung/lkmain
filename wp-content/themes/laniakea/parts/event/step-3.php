<div v-cloak class="section mb-5" id="step-3" v-show="step === 3">
    <h3 class="text-primary-color mb-3">3. Bạn thích đánh quái (PvM) hay thích đánh với người (PvP/BG/WoE)?</h3>

    <div class="mb-3">
        <p class="lead">Vậy server nghiêng về bên nào? PvM hay PvP? Ăn chay hay ăn mặn?</p>
        <p class="lead">Câu trả lời đó là tùy. Như LaniakeaRO đã nói, ở mỗi giai đoạn của server, server lại có một trải nghiệm mới. Hiện tại thì LaniakeaRO đang có những trải nghiệm về Battleground, Yeh! Rất sôi nổi.</p>
        <p class="lead">Đơn giản vì LaniakeaRO dành cho những bạn muốn có trải nghiệm mới, dành cho những bạn đã ôn xong tuổi thơ, về riêng LaniakeaRO thì Renewal rất thử thách cho phía GM, đã thử thách thì không ngại, triển!</p>
    </div>

    <hr>

    <h5 class="text-secondary-color mb-2">Thế bạn thì sao? Bạn thích đi cảnh hơn hay là đánh quái:</h5>
    <div class="form-option-question mb-3">
        <div class="form-check">
            <input class="form-check-input" type="radio" v-model="q3" name="q3" id="q3-1" value="1">
            <label class="form-check-label" for="q3-1">
                Thích đi cảnh hơn
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" v-model="q3" name="q3" id="q3-2" value="2">
            <label class="form-check-label" for="q3-2">
                Thích đánh với người hơn
            </label>
        </div>
    </div>

    <template v-if="q3">
        <h5 class="text-secondary-color mb-2">Quà tặng tương ứng với lựa chọn này:</h5>
    </template>

    <ul class="border rounded bg-primary-light p-3 pl-4" v-if="q3 == 1">
        <li class="mb-2">
            <?php item_icon(6732); ?>&nbsp;<span class="item-name">Monster Coin</span>&nbsp;<span>x500</span><br>
            <small>Dùng để mua vật phẩm ở Cửa hàng Newbie.</small>
        </li>
        <li class="mb-2">
            <img class="item-icon" src="<?php echo get_template_directory_uri() . '/images/icons/MetalBox.png' ?>" alt="MetalBox">&nbsp;<span class="item-name">Hộp trang bị True Hunting +9 (áo giáp, áo choàng, giày, 2 trang sức)</span>&nbsp;<span>x1</span><br>
            <small>Để mặc được yêu cầu cấp độ đạt 100 (không thể giao dịch với tài khoản khác).</small>
        </li>
    </ul>

    <ul class="border rounded bg-primary-light p-3 pl-4" v-if="q3 == 2">
        <li class="mb-2">
            <?php item_icon(7829); ?>&nbsp;<span class="item-name">Valor Badge</span>&nbsp;<span>x500</span><br>
            <small>Valor Badge để mua thêm trang bị từ Cửa hàng PvP.</small>
        </li>
        <li class="mb-2">
            <img class="item-icon" src="<?php echo get_template_directory_uri() . '/images/icons/OBATTLECRATE.png' ?>" alt="OBATTLECRATE">&nbsp;<span class="item-name">Hộp trang bị WoE +9 (áo giáp, áo choàng, giày)</span>&nbsp;<span>x1</span><br>
            <small>Bộ trang bị tăng cường sát thương và tăng sự bền bỉ ở PvP, BG, Woe. Bạn có thể tinh luyện ở Thợ rèn Thúc mà không có rủi ro thất bại.</small>
        </li>
    </ul>

    <nav class="__page" aria-label="page navigation">
        <ul class="pagination justify-content-center">
            <li class="page-item">
                <a class="page-link" href="#" @click.prevent="step--">Quay lại</a>
            </li>
            <li class="page-item" :class="{'disabled' : !q3}">
                <a class="page-link" href="#" @click.prevent="step++">Tiếp tục</a>
            </li>
        </ul>
    </nav>
</div>