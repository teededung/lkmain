<div v-cloak class="section" id="step-8" v-show="step === 8">
    <h3 class="text-primary-color mb-3">8. Chung tay góp phần đẩy lùi dịch bệnh</h3>
    <p>
        Mặc dù đã nới lỏng cách ly nhưng các bạn cũng không được chủ quan.<br>
        Đeo khẩu trang, rửa tay thường xuyên, không tụ tập nơi đông người.
    </p>

    <div class="mb-3">
        <button class="btn btn-primary" v-if="!q8" @click="q8 = 1">Nhất trí!</button>
    </div>

    <template v-if="q8">
        <hr>
        <p>Thế nên... LaniakeaRO đã chuẩn bị 1 bộ costume để bảo vệ sức khỏe nhân vật của bạn...</p>
        <img class="img-thumbnail mb-3" src="<?php echo get_template_directory_uri() . '/images/event/nurse-pack.jpg' ?>" alt="nurse-pack">
        <p>Bạn nhận được 1 bộ thời trang bảo vệ sức khỏe mùa địch bệnh.</p>
        <ul>
            <li><?php item_icon(19637) ?>&nbsp;<span class="item-name">Costume Nurse Cap</span>&nbsp;<span>x1</span></li>
            <li><?php item_icon(19634) ?>&nbsp;<span class="item-name">Costume Flu Mask</span>&nbsp;<span>x1</span></li>
            <li><img class="item-icon" src="<?php echo get_template_directory_uri() . '/images/icons/alchemist-relic.png'; ?>" alt="alchemist-relic">&nbsp;<span class="item-name">Costume Alchemist Relic</span>&nbsp;<span>x1</span></li>
        </ul>
    </template>

    <nav class="__page" aria-label="page navigation">
        <ul class="pagination justify-content-center">
            <li class="page-item">
                <a class="page-link" href="#" @click.prevent="step--">Quay lại</a>
            </li>
            <li class="page-item" :class="{'disabled' : !q8}">
                <a class="page-link" href="#" @click.prevent="step++">Tiếp tục</a>
            </li>
        </ul>
    </nav>
</div>
