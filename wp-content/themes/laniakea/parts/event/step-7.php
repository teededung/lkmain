<div v-cloak class="section" id="step-7" v-show="step === 7">
    <h3 class="text-primary-color mb-3">7. Quà kỉ niệm 20 năm thành lập Gravity (Nhà sản xuất Ragnarok Online)</h3>

    <p class="lead">Không phải kỉ niệm Ragnarok Online, đây là kỉ niệm 20 năm thành lập công ty Gravity, trụ sở phát hành Ragnarok Online trực tuyến và các tựa game Ragnarok phiên bản khác.</p>
    <p class="lead">Gravity cũng không quên đứa con của mình, họ đã thiết kế 1 chiếc mũ Costume cực độc dành tặng cho các roker yêu quý Ragnarok Online.</p>
    <button class="btn btn-primary" v-if="!q7" @click="q7 = 1">Xem đó là costume gì...</button>

    <template v-if="q7">
        <img class="img-thumbnail mb-3" src="<?php echo get_template_directory_uri() . '/images/event/anniversary.gif' ?>" alt="anniversary">
        <p>... đó là <img class="item-icon" src="<?php echo get_template_directory_uri() . '/images/icons/' . '20th_Anniversary.png' ?>" alt="20-anniversary">&nbsp;<span class="item-name">Costume Anniversary Balloon (Lower)</span>.</p>
        <p>...bạn thấy số 20 nho nhỏ đập như nhịp tim chứ? Vâng nó cũng có ý nghĩa là chúng tôi dành cả trái tim này để làm game cho các bạn tận hưởng... và nếu bạn là game thủ ở LaniakeaRO, bạn cũng sẽ nhận được costume này!</p>
    </template>

    <nav class="__page" aria-label="page navigation">
        <ul class="pagination justify-content-center">
            <li class="page-item">
                <a class="page-link" href="#" @click.prevent="step--">Quay lại</a>
            </li>
            <li class="page-item" :class="{'disabled' : !q7}">
                <a class="page-link" href="#" @click.prevent="step++">Tiếp tục</a>
            </li>
        </ul>
    </nav>
</div>