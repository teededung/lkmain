<div v-cloak class="section mb-5" id="step-4" v-show="step === 4">
    <h3 class="text-primary-color mb-3">4. Chọn 1 mũ thời trang phía trên cùng mà bạn thích nhất</h3>

    <div class="form-option-question mb-3">
        <div class="row">
            <div class="col-md-7">
                <div class="mb-3">
                    <p class="lead">
                        Về thời trang, ở LaniakeaRO luôn cập nhật những mẫu thời trang mới nhất... Nhưng... các costume cổ điển thì sao?<br>
                        Costume cổ điển cũng vậy, LaniakeaRO có rất nhiều nhiệm vụ riêng để tậu mũ cổ điển, bạn có thể xem ở <a target="_blank" href="https://hahiu.com/huong-dan/nhiem-vu-tao-mu/">Nhiệm vụ tạo mũ</a>.
                    </p>
                    <p>LaniakeaRO dành tặng tiếp cho bạn 1 mũ costume cổ điển, những costume có dấu * kí hiệu mũ này có hỗ trợ đổi màu từ NPC.</p>
                </div>

                <div class="form-check mb-1">
                    <input class="form-check-input" type="radio" v-model="q4" name="q4" id="q4-1" value="1">
                    <label class="form-check-label" for="q4-1">
                        <?php item_icon(41077) ?>&nbsp;<span>Costume Bunny Hood (White) *</span><br>
                    </label>
                </div>
                <div class="form-check mb-1">
                    <input class="form-check-input" type="radio" v-model="q4" name="q4" id="q4-2" value="2">
                    <label class="form-check-label" for="q4-2">
                        <?php item_icon(42367) ?>&nbsp;<span>Costume Tail Hat (Purple) *</span><br>
                    </label>
                </div>
                <div class="form-check mb-1">
                    <input class="form-check-input" type="radio" v-model="q4" name="q4" id="q4-3" value="3">
                    <label class="form-check-label" for="q4-3">
                        <?php item_icon(41130) ?>&nbsp;<span>Costume Cat Hood (Pink) *</span><br>
                    </label>
                </div>
                <div class="form-check mb-1">
                    <input class="form-check-input" type="radio" v-model="q4" name="q4" id="q4-4" value="4">
                    <label class="form-check-label" for="q4-4">
                        <?php item_icon(42118) ?>&nbsp;<span>Costume Archangeling Hairband (Yellow) *</span><br>
                    </label>
                </div>
                <div class="form-check mb-1">
                    <input class="form-check-input" type="radio" v-model="q4" name="q4" id="q4-5" value="5">
                    <label class="form-check-label" for="q4-5">
                        <?php item_icon(31794) ?>&nbsp;<span>Costume Puppy Ears Hat</span>
                    </label>
                </div>
            </div>
            <div class="col-md-5">
                <template v-if="q4">
                    <h5 class="text-secondary-color mb-2">Quà tặng tương ứng với lựa chọn này:</h5>

                    <ul class="list-unstyled">
                        <li class="mb-2" v-show="q4 == 1">
                            <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/event/bunny-hood.jpg' ?>" alt="bunny-hood">
                        </li>
                        <li class="mb-2" v-show="q4 == 2">
                            <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/event/tail-hat.jpg' ?>" alt="tail-hat">
                        </li>
                        <li class="mb-2" v-show="q4 == 3">
                            <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/event/cat-hood.jpg' ?>" alt="cat-hood">
                        </li>
                        <li class="mb-2" v-show="q4 == 4">
                            <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/event/archangeling.jpg' ?>" alt="archangeling">
                        </li>
                        <li class="mb-2" v-show="q4 == 5">
                            <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/event/puppy-ears.jpg' ?>" alt="puppy-ears">
                        </li>
                    </ul>

                    <p>Bạn sẽ nhận <img class="item-icon" :src="`https://api.hahiu.com/data/items/icons/${getItemInfoQ4().id}.png`" :alt="getItemInfoQ4().name">&nbsp;<span class="item-name">{{ getItemInfoQ4().name }}</span></p>
                </template>
            </div>
        </div>
    </div>

    <nav class="__page" aria-label="page navigation">
        <ul class="pagination justify-content-center">
            <li class="page-item">
                <a class="page-link" href="#" @click.prevent="step--">Quay lại</a>
            </li>
            <li class="page-item" :class="{'disabled' : !q4}">
                <a class="page-link" href="#" @click.prevent="step++">Tiếp tục</a>
            </li>
        </ul>
    </nav>
</div>