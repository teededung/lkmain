<div v-cloak class="section" id="step-9" v-show="step === 9">
    <h3 class="text-primary-color mb-3">9. Sắp xong rồi... Kiểm tra lại các thứ đã...</h3>

    <p class="lead">Bạn hãy kiểm tra lại những quà tặng, nếu bạn cần chỉnh sửa gì thì hãy bấm quay lại.</p>

    <div class="row mb-5">
        <div class="col-md-8 mb-3 mb-md-0">
            <ul class="list-group">
                <li class="list-group-item li-q0"><strong>1,000 Free cash</strong></li>
                <li class="list-group-item li-q0"><?php item_icon(12210); ?>&nbsp;<span class="item-name">Bubble Gum</span>&nbsp;<span>x5</span></li>
                <li class="list-group-item li-q0"><?php item_icon(12208); ?>&nbsp;<span class="item-name">Battle Manual</span>&nbsp;<span>x5</span></li>
                <li class="list-group-item li-q0"> <?php item_icon(25274); ?>&nbsp;<span class="item-name">Vé VIP 7 ngày</span>&nbsp;<span>x2</span></li>

                <template v-if="q2 == 1">
                    <li class="list-group-item li-q1"><?php item_icon(7638); ?>&nbsp;<span class="item-name">Child Potion Box</span>&nbsp;<span>x10</span></li>
                    <li class="list-group-item li-q1"><?php item_icon(25464); ?>&nbsp;<span class="item-name">World Moving Rights</span>&nbsp;<span>x100</span></li>
                    <li class="list-group-item li-q1"><?php item_icon(28914); ?>&nbsp;<span class="item-name">Enhanced Time Keeper Shield</span>&nbsp;<span>x1</span></li>
                    <li class="list-group-item li-q1"><?php item_icon(13740); ?>&nbsp;<span class="item-name">Hộp trang bị Shadow Promotion (6 món)</span> <span>x1</span></li>
                </template>
                <template v-if="q2 == 2">
                    <li class="list-group-item li-q1"><?php item_icon(52009); ?>&nbsp;<span class="item-name">Laniakea Token</span>&nbsp;<span>x1000</span></li>
                    <li class="list-group-item li-q1"><?php item_icon(22529); ?>&nbsp;<span class="item-name">Shadow Exchange SynthesisBox</span>&nbsp;<span>x10</span></li>
                    <li class="list-group-item li-q1"><?php item_icon(12134); ?>&nbsp;<span class="item-name">Vé reset cooldown Instance (special)</span>&nbsp;<span>x3</span></li>
                </template>

                <template v-if="q3 == 1">
                    <li class="list-group-item li-q0"><?php item_icon(6732); ?>&nbsp;<span class="item-name">Monster Coin</span>&nbsp;<span>x500</span></li>
                    <li class="list-group-item li-q0"><img class="item-icon" src="<?php echo get_template_directory_uri() . '/images/icons/MetalBox.png' ?>" alt="MetalBox">&nbsp;<span class="item-name">Hộp trang bị True Hunting +9 (áo giáp, áo choàng, giày, 2 trang sức)</span>&nbsp;<span>x1</span></li>
                </template>
                <template v-if="q3 == 2">
                    <li class="list-group-item li-q0"><?php item_icon(7829); ?>&nbsp;<span class="item-name">Valor Badge</span>&nbsp;<span>x500</span></li>
                    <li class="list-group-item li-q0"><img class="item-icon" src="<?php echo get_template_directory_uri() . '/images/icons/OBATTLECRATE.png' ?>" alt="OBATTLECRATE">&nbsp;<span class="item-name">Hộp trang bị WoE (áo giáp, áo choàng, giày)</span>&nbsp;<span>x1</span></li>
                </template>

                <li class="list-group-item li-q1" v-if="q4">
                    <img class="item-icon" :src="`https://api.hahiu.com/data/items/icons/${getItemInfoQ4().id}.png`" :alt="getItemInfoQ4().name">&nbsp;<span class="item-name">{{ getItemInfoQ4().name }}</span>&nbsp;<span>x1</span>
                </li>

                <li class="list-group-item li-q0" v-if="q5">
                    <div v-html="getAuraName()"></div>
                </li>

                <li class="list-group-item li-q1" v-if="q6">
                    <img class="item-icon" src="<?php echo get_template_directory_uri() . '/images/icons/' . '20th_Anniversary.png' ?>" alt="20-anniversary">&nbsp;<span class="item-name">Costume Anniversary Balloon</span>&nbsp;<span>x1</span>
                </li>

                <template v-if="q7">
                    <li class="list-group-item li-q0"><?php item_icon(19637) ?>&nbsp;<span class="item-name">Costume Nurse Cap</span>&nbsp;<span>x1</span></li>
                    <li class="list-group-item li-q0"><?php item_icon(19634) ?>&nbsp;<span class="item-name">Costume Flu Mask</span>&nbsp;<span>x1</span></li>
                    <li class="list-group-item li-q0"><img class="item-icon" src="<?php echo get_template_directory_uri() . '/images/icons/alchemist-relic.png'; ?>" alt="alchemist-relic">&nbsp;<span class="item-name">Costume Alchemist Relic</span>&nbsp;<span>x1</span></li>
                </template>
            </ul>
        </div>
        <div class="col-md-4">
            <form class="mb-3" id="login-form" action="" method="post" @submit.prevent="login">
                <div class="form-group">
                    <label for="username">Tên đăng nhập</label>
                    <input type="text" name="username" v-model="username" data-vv-name="username" data-vv-as="Tên đăng nhập" data-vv-delay="1000" v-validate="'required|min:6|max:30'" min="4" max="30" :class="{'is-invalid' : errors.has('username')}" class="form-control input" id="username" placeholder="Nhập tên đăng nhập">
                    <div class="invalid-feedback" v-if="errors.has('username')">
                        {{ errors.first('username') }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="password">Mật khẩu</label>
                    <input type="password" name="password" v-model="password" data-vv-name="password" data-vv-as="Mật khẩu" data-vv-delay="1000" v-validate="'required|min:6|max:30'" min="6" max="30" :class="{'is-invalid' : errors.has('password')}" class="form-control input" id="password" placeholder="Nhập mật khẩu của bạn">
                    <div class="invalid-feedback" v-if="errors.has('password')">
                        {{ errors.first('password') }}
                    </div>
                </div>

                <button class="btn btn-primary" type="submit">Đăng nhập</button>

                <div class="mt-4">
                    <a target="_blank" href="<?php echo home_url('/quen-mat-khau/'); ?>">Bạn quên mật khẩu?</a>
                </div>
            </form>

            <div class="alert alert-warning">
                <strong>Chú ý:</strong> Bạn chỉ có thể nhận quà 1 lần. Các tài khoản phụ (clone) sẽ không thể nhận thêm được.
            </div>
        </div>
    </div>

    <nav class="__page" aria-label="page navigation">
        <ul class="pagination justify-content-center">
            <li class="page-item">
                <a class="page-link" href="#" @click.prevent="step--">Quay lại</a>
            </li>
        </ul>
    </nav>
</div>