<div v-cloak class="section" id="step-2" v-show="step === 2">
    <h3 class="text-primary-color mb-3">2. Bạn là ma mới hay ma cũ?</h3>

    <p class="lead">Sau 2 năm chắn hẳn người chơi sẽ không còn mặn mà như lúc mới mở, với LaniakeaRO thì chuyện đó cũng bình thường.</p>
    <p class="lead">Số lượng online của server vẫn ổn, có những lúc quá vắng cũng chỉ là tạm thời, server sau mỗi lần như vậy sẽ bước sang giai đoạn mới, mỗi giai đoạn mới thì lại có những người bạn mới, những câu chuyện mới, cảm giác mới... yeh, cảm giác này rất khó tả.</p>

    <hr>

    <h5 class="text-secondary-color mb-2">Bạn đã thành thạo Ragnarok Online chưa?</h5>
    <div class="form-option-question mb-3">
        <div class="form-check">
            <input class="form-check-input" type="radio" v-model="q2" name="q2" id="q2-1" value="1">
            <label class="form-check-label" for="q2-1">
                Newbie hoàn toàn
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" v-model="q2" name="q2" id="q2-2" value="2">
            <label class="form-check-label" for="q2-2">
                Người cũ... Đã từng chơi Ragnarok Online rồi...
            </label>
        </div>
    </div>

    <template v-if="q2">
        <h5 class="text-secondary-color mb-2">Quà tặng tương ứng với lựa chọn này:</h5>
    </template>

    <ul v-if="q2 == 1" class="border rounded bg-primary-light p-3 pl-4">
        <li class="mb-2">
            <?php item_icon(7638); ?>&nbsp;<span class="item-name">Child Potion Box</span>&nbsp;<span>x10</span><br>
            <small>Mỗi hộp đựng 200 bình máu trắng loại đặc biệt.</small>
        </li>
        <li class="mb-2">
            <?php item_icon(25464); ?>&nbsp;<span class="item-name">World Moving Rights</span>&nbsp;<span>x100</span><br>
            <small>Vé dịch chuyển nhanh tới một bản đồ nào đó, bạn có thể xem thêm tại <a target="_blank" href="https://hahiu.com/he-thong-tinh-nang/dich-chuyen-khong-can-kafra-private-airship/">đây</a>.</small>
        </li>
        <li class="mb-2">
            <?php item_icon(28914); ?>&nbsp;<span class="item-name">Enhanced Time Keeper Shield</span>&nbsp;<span>x1</span><br>
            <small>1 tấm khiên giúp bạn gia tăng phòng thủ.</small>
        </li>
        <li class="mb-2">
            <?php item_icon(13740); ?>&nbsp;<span class="item-name">Hộp trang bị Shadow Promotion +7 (6 món)</span>&nbsp;<span>x1</span><br>
            <small>Mặc 6 món sẽ mở khóa thêm hiệu ứng.</small>
        </li>
    </ul>

    <ul v-if="q2 == 2" class="border rounded bg-primary-light p-3 pl-4">
        <li class="mb-2">
            <?php item_icon(52009); ?>&nbsp;<span class="item-name">Laniakea Token</span>&nbsp;<span>x1000</span><br>
            <small>Dùng để mua vật phẩm ở Cửa hàng Laniakea.</small>
        </li>
        <li class="mb-2">
            <?php item_icon(22529); ?>&nbsp;<span class="item-name">Shadow Exchange SynthesisBox</span>&nbsp;<span>x10</span><br>
            <small>Tổ hợp 3 Shadow bất kì tạo thành 1 trang bị Shadow ngẫu nhiên khác.</small>
        </li>
        <li class="mb-2">
            <?php item_icon(12134); ?>&nbsp;<span class="item-name">Vé reset cooldown Instance (special)</span>&nbsp;<span>x3</span><br>
            <small>Dùng vé để reset cooldown toàn bộ Instance, kể cả Laniakea Token nhận từ Instance.</small>
        </li>
    </ul>

    <nav class="__page" aria-label="page navigation">
        <ul class="pagination justify-content-center">
            <li class="page-item">
                <a class="page-link" href="#" @click.prevent="step--">Quay lại</a>
            </li>
            <li class="page-item" :class="{'disabled' : !q2}">
                <a class="page-link" href="#" @click.prevent="step++">Tiếp tục</a>
            </li>
        </ul>
    </nav>
</div>

