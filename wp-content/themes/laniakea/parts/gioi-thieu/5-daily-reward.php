<section class="section" id="section-5">
	<div class="container content-lg">

		<div class="text-center">
			<h2>Quà tặng đăng nhập mỗi ngày</h2>

			<div class="w-relative-daily">
                <div class="will-hide">
                    <img src="<?php echo get_template_directory_uri() . '/images/daily-reward-s.jpg'; ?>" alt="laniakea-daily-reward">

                    <img class="sub-daily" id="daily-26" src="<?php echo get_template_directory_uri() .'/images/daily-reward-26.jpg'; ?>" alt="daily-reward-26">
                    <img class="sub-daily" id="daily-27" src="<?php echo get_template_directory_uri() .'/images/daily-reward-27.jpg'; ?>" alt="daily-reward-27">
                    <img class="sub-daily" id="daily-28" src="<?php echo get_template_directory_uri() .'/images/daily-reward-28.jpg'; ?>" alt="daily-reward-28">
                </div>
				<div class="will-show">
                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/daily-reward.png'; ?>" alt="laniakea-daily-reward">
                </div>
			</div>

			<div class="help-text-daily">
				Quà tặng đăng nhập hàng ngày sẽ thay đổi theo định kì hàng tháng.
			</div>
		</div>

	</div>
</section>