<section class="section" id="section-3">
	<div class="container-fluid content-padding">

        <div class="row align-items-center row-job-1">
            <div class="col-md-7">
                <div class="content-job">
                    <div class="title-job">
                        <h2 id="new-job-1">Nghề mới</h2>
                        <h3 id="new-job-2">Soul Reaper & Star Emperor</h3>
                    </div>

                    <div id="content-soul-reaper-star-emperor">
                        <p>
                            Không để người chơi Soul Linker và Star Gladiator chịu thiệt thòi, một thời gian khá lâu sau đó Gravity đã cập nhật nghề mới tiếp theo của nghề Soul Linker và Soul Reaper.
                        </p>
                        <p>2 nghề mới mang đến bộ kỹ năng mới, hiệu ứng mới, trang phục và mẫu tóc cũng mới toanh.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="row row-img-job justify-content-center">
                    <div class="col-6 text-center">
                        <img id="soul-reaper" src="<?php echo get_template_directory_uri() . '/images/soul-reaper.png'; ?>" alt="">
                    </div>
                    <div class="col-6 text-center">
                        <img id="star-emperor" src="<?php echo get_template_directory_uri() . '/images/star-emperor.png' ?>" alt="">
                    </div>
                </div>
            </div>
        </div>

        <?php echo do_shortcode("[rev_slider alias=\"jobs\"]") ?>

		<div class="row row-img-job align-items-center row-job-2">
			<div class="col-md-5">
				<div class="row justify-content-center">
					<div class="col-6 text-center">
						<img id="doram-2" src="<?php echo get_template_directory_uri() . '/images/doram-2.png'; ?>" alt="">
					</div>
					<div class="col-6 text-center">
						<img id="doram-1" src="<?php echo get_template_directory_uri() . '/images/doram-1.png' ?>" alt="">
					</div>
				</div>
			</div>
			<div class="col-md-7">
				<div class="content-job">
					<div class="title-job">
						<h2 id="doram-3">Chủng tộc mới</h2>
						<h3 id="doram-4">DORAM - SUMMONER</h3>
					</div>

					<div id="doram-5">
						<p>
							Ragnarok Online đã cập nhật một chủng tộc hoàn toàn mới. Chủng tộc có tên gọi là Summoner hay còn gọi là Doram.
							Chúng tộc Summoner được thiết kế theo hình dáng loài mèo.
						</p>

						<p>
							Nếu bạn chưa chơi chủng tộc này bao giờ, thì đây là cơ hội để các bạn điều khiển chú "boss" của mình tiêu diệt quái vật.
						</p>
					</div>
				</div>
			</div>
		</div>


	</div>
</section>
