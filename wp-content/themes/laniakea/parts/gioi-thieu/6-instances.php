<?php $instance_source = get_template_directory_uri() . '/images/instance/'; ?>

<section class="section" id="section-6">
	<div class="container-fluid content-lg">

		<h2 class="text-center">Nhiều Instance để bạn khám phá</h2>

		<div class="owl-carousel instance-carousel">
			<div class="each-instance">
				<img src="<?php echo $instance_source . 'hazy-forest.jpg'; ?>" alt="laniakea-ragnarok-online-hazy-forest">

				<div class="instance-title">
					<h3>Hazy Forest</h3>
					<h4>Yêu cầu cấp độ: 95+</h4>
				</div>
			</div>
			<div class="each-instance">
                <img src="<?php echo $instance_source . 'nidhoggurs-nest.jpg'; ?>" alt="laniakea-ragnarok-online-nidhoggurs-nest">

                <div class="instance-title">
                    <h3>Nidhoggur's Nest</h3>
                    <h4>Yêu cầu cấp độ: 70+</h4>
                </div>
            </div>
            <div class="each-instance">
                <img src="<?php echo $instance_source . 'charleston-crisis.jpg'; ?>" alt="laniakea-ragnarok-online-charleston-crisis">

                <div class="instance-title">
                    <h3>Charleston Crisis</h3>
                    <h4>Yêu cầu cấp độ: 130+</h4>
                </div>
            </div>
			<div class="each-instance">
				<img src="<?php echo $instance_source . 'old-glastheim.jpg'; ?>" alt="laniakea-ragnarok-online-old-glastheim">

				<div class="instance-title">
					<h3>Old Glast Heim</h3>
					<h4>Yêu cầu cấp độ: 130+</h4>
				</div>
			</div>
            <div class="each-instance">
                <img src="<?php echo $instance_source . 'old-glastheim.jpg'; ?>" alt="laniakea-ragnarok-online-old-glastheim-hard-mode">

                <div class="instance-title">
                    <h3>Old Glast Heim (Chế độ khó)</h3>
                    <h4>Yêu cầu cấp độ: 130+</h4>
                </div>
            </div>
			<div class="each-instance">
				<img src="<?php echo $instance_source . 'endless-tower.jpg'; ?>" alt="laniakea-ragnarok-online-endless-tower">

				<div class="instance-title">
					<h3>Endless Tower</h3>
					<h4>Yêu cầu cấp độ: 50+</h4>
				</div>
			</div>
			<div class="each-instance">
				<img src="<?php echo $instance_source . 'horror-toy-factory.jpg'; ?>" alt="laniakea-ragnarok-online-horro-toy-factory">

				<div class="instance-title">
					<h3>Horror Toy Factory</h3>
					<h4>Yêu cầu cấp độ: 140+</h4>
				</div>
			</div>
			<div class="each-instance">
				<img src="<?php echo $instance_source . 'ghost-palace.jpg'; ?>" alt="laniakea-ragnarok-online-ghost-palace">

				<div class="instance-title">
					<h3>Ghost Palace</h3>
					<h4>Yêu cầu cấp độ: 120+</h4>
				</div>
			</div>
			<div class="each-instance">
				<img src="<?php echo $instance_source . 'horror-toy-factory.jpg'; ?>" alt="laniakea-ragnarok-online-horro-toy-factory">

				<div class="instance-title">
					<h3>Horror Toy Factory</h3>
					<h4>Yêu cầu cấp độ: 140+</h4>
				</div>
			</div>
			<div class="each-instance">
				<img src="<?php echo $instance_source . 'faceworms-nest.jpg'; ?>" alt="laniakea-ragnarok-online-faceworms-nest">

				<div class="instance-title">
					<h3>Faceworm's Nest</h3>
					<h4>Yêu cầu cấp độ: 140+</h4>
				</div>
			</div>
			<div class="each-instance">
				<img src="<?php echo $instance_source . 'bakonawa-lake.jpg'; ?>" alt="laniakea-ragnarok-online-bakonawa-lake">

				<div class="instance-title">
					<h3>Bakonawa Lake</h3>
					<h4>Yêu cầu cấp độ: 140+</h4>
				</div>
			</div>
		</div>
	</div>
</section>