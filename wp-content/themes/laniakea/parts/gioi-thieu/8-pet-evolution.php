<section class="section pet-evolution-section" :class="{'loaded': loaded}" id="section-8">
	<div class="container content-lg">

		<div class="text-center title-pet-evo">
			<h2>Hệ thống Thú cưng tiến hóa (Pet Evolution)</h2>

			<p>
				1 trong những tính năng thú vị không thể kể đến chính là hệ thống thú cưng của Ragnarok, hệ thống Pet Evolution là một bản nâng cấp hệ thống thú cưng của kRO.
				Hiện tại Laniakea RO đã tích hợp hệ thống Pet Evolution cho mọi người cùng khám phá.
			</p>
		</div>

		<?php $pet_source = get_template_directory_uri() . '/images/pet/' ?>
		<?php $icon_source = get_template_directory_uri() . '/images/icons/' ?>

		<input type="hidden" id="pet_source" value="<?php echo $pet_source; ?>">
		<input type="hidden" id="icon_source" value="<?php echo $icon_source; ?>">

		<div class="list-pets">
			<div class="row row-pet" v-for="pet in pets">

				<div class="col-md-8 col-pet-left d-flex flex-column align-items-center justify-content-center">
					<div class="pet-evo-info" v-if="hasEvo(pet.ID) && !hasEvo2(pet.ID)">
						Đã tiến hóa thành công từ <strong>{{ pet.name }}</strong> thành <strong>{{ pet.evo }}</strong>
					</div>

					<div class="pet-evo-info" v-if="hasEvo2(pet.ID)">
						Đã tiến hóa thành công từ <strong>{{ pet.evo }}</strong> thành <strong>{{ pet.evo2 }}</strong>
					</div>

                    <div class="row w-100 d-flex align-items-center text-center">
                        <div class="col">
                            <h4>{{ pet.name }}</h4>
                            <ul class="list-unstyled pet-stats">
                                <li v-for="stat in pet.stats">{{ stat }}</li>
                            </ul>
                        </div>

                        <div class="col" v-if="hasEvo(pet.ID)">
                            <h4>{{ pet.evo }}</h4>
                            <ul class="list-unstyled pet-stats">
                                <li v-for="stat in pet.statsEvo">{{ stat }}</li>
                            </ul>
                        </div>

                        <div class="col" v-if="hasEvo2(pet.ID)">
                            <h4>{{ pet.evo2 }}</h4>
                            <ul class="list-unstyled pet-stats" v-if="hasEvo2(pet.ID)">
                                <li v-for="stat in pet.statsEvo2">{{ stat }}</li>
                            </ul>
                        </div>
                    </div>
				</div>

				<div class="col-md-4 col-pet-right text-center">
					<div class="pet-normal">
						<div class="w-pet-imgs">
							<img v-if="!hasEvo(pet.ID)" :src="petSource + 'pet_' + pet.name.toLowerCase() + '.bmp'" alt="laniakea-pet-poring">
							<img v-if="hasEvo(pet.ID) && !hasEvo2(pet.ID)" :src="petSource + 'pet_' + pet.evo.toLowerCase().replace(' ', '_') + '.bmp'" alt="laniakea-pet-mastering">
							<img v-if="hasEvo2(pet.ID)" :src="petSource + 'pet_' + pet.evo2.toLowerCase().replace(' ', '_') + '.bmp'" alt="laniakea-pet-angeling">
							<div class="progress" v-if="pet.running">
								<div class="progress-bar" role="progressbar" :style="'width: ' + pet.progress + '%'" :aria-valuenow="pet.progress" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>

						<div class="pet-name">{{ getPetName(pet.ID) }}</div>
						<button class="btn btn-secondary" :disabled="pet.running || pet.evoLvl === pet.maxEvo" @click="evo(pet.ID)">{{ getButtonText(pet.ID) }}</button>
					</div>

					<div class="ingredients" v-if="!hasEvo(pet.ID) && !hasEvo2(pet.ID)">
						<div class="each-item d-flex justify-content-center" v-for="ingredient in pet.ingredients">
							<img :src="iconSource + ingredient.id + '.png'" alt="laniakea-610">
							<span> x {{ ingredient.quantity }} {{ ingredient.name }}</span>
						</div>
					</div>

					<div class="ingredients" v-if="hasEvo(pet.ID) && !hasEvo2(pet.ID)">
						<div class="each-item d-flex justify-content-center" v-for="ingredient in pet.ingredients2">
							<img :src="iconSource + ingredient.id + '.png'" alt="laniakea-610">
							<span> x {{ ingredient.quantity }} {{ ingredient.name }}</span>
						</div>
					</div>
				</div>
			</div>
		</div>

		<h3 class="evo-text-footer">Còn nhiều thú cưng khác đang đợi bạn ở <a target="_blank" rel="nofollow" href="https://hahiu.com/he-thong-tinh-nang/he-thong-thu-cung/">đây</a> !!</h3>
	</div>
</section>