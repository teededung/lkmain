<section class="section refine-section" :class="{'loaded':loaded}" id="section-7">

	<?php $source_url = get_template_directory_uri() . '/images/refine/'; ?>
	<input type="hidden" id="refine-source-url" value="<?php echo $source_url; ?>">
	<div class="hidden">
		<img src="<?php echo $source_url . 'refine-original/bg_refiningA_00.bmp'; ?>" alt="refining-1A">
		<img src="<?php echo $source_url . 'refine-original/bg_refiningA_01.bmp'; ?>" alt="refining-2A">
		<img src="<?php echo $source_url . 'refine-original/bg_refiningA_02.bmp'; ?>" alt="refining-3A">
		<img src="<?php echo $source_url . 'refine-original/bg_refiningA_03.bmp'; ?>" alt="refining-4A">
		<img src="<?php echo $source_url . 'refine-original/bg_refiningA_04.bmp'; ?>" alt="refining-5A">
		<img src="<?php echo $source_url . 'refine-original/bg_refiningA_05.bmp'; ?>" alt="refining-6A">
		<img src="<?php echo $source_url . 'refine-original/bg_refiningA_06.bmp'; ?>" alt="refining-7A">
		<img src="<?php echo $source_url . 'refine-original/bg_refiningA_07.bmp'; ?>" alt="refining-8A">
		<img src="<?php echo $source_url . 'refine-original/bg_refiningA_08.bmp'; ?>" alt="refining-9A">
		<img src="<?php echo $source_url . 'refine-original/bg_refiningA_09.bmp'; ?>" alt="refining-10A">
		<img src="<?php echo $source_url . 'refine-original/bg_refiningA_10.bmp'; ?>" alt="refining-11A">
		<img src="<?php echo $source_url . 'refine-original/bg_refiningA_11.bmp'; ?>" alt="refining-12A">
		<img src="<?php echo $source_url . 'refine-original/bg_refiningA_12.bmp'; ?>" alt="refining-13A">
		<img src="<?php echo $source_url . 'refine-original/bg_refiningA_13.bmp'; ?>" alt="refining-14A">
		<img src="<?php echo $source_url . 'refine-original/bg_refiningA_14.bmp'; ?>" alt="refining-15A">
		<img src="<?php echo $source_url . 'refine-failed/bg_refiningA_fail00_none.bmp'; ?>" alt="refiningAn-fail00">
		<img src="<?php echo $source_url . 'refine-failed/bg_refiningA_fail01_none.bmp'; ?>" alt="refiningAn-fail01">
		<img src="<?php echo $source_url . 'refine-failed/bg_refiningA_fail02_none.bmp'; ?>" alt="refiningAn-fail02">
		<img src="<?php echo $source_url . 'refine-failed/bg_refiningA_fail03_none.bmp'; ?>" alt="refiningAn-fail03">
		<img src="<?php echo $source_url . 'refine-failed/bg_refiningA_fail04_none.bmp'; ?>" alt="refiningAn-fail04">
		<img src="<?php echo $source_url . 'refine-failed/bg_refiningA_fail05_none.bmp'; ?>" alt="refiningAn-fail05">
		<img src="<?php echo $source_url . 'refine-failed/bg_refiningA_fail06_none.bmp'; ?>" alt="refiningAn-fail06">
		<img src="<?php echo $source_url . 'refine-failed/bg_refiningA_fail07_none.bmp'; ?>" alt="refiningAn-fail07">

		<img src="<?php echo $source_url . 'refine-success/bg_refiningA_success00.bmp'; ?>" alt="refiningA-success00">
		<img src="<?php echo $source_url . 'refine-success/bg_refiningA_success01.bmp'; ?>" alt="refiningA-success01">
		<img src="<?php echo $source_url . 'refine-success/bg_refiningA_success02.bmp'; ?>" alt="refiningA-success02">
		<img src="<?php echo $source_url . 'refine-success/bg_refiningA_success03.bmp'; ?>" alt="refiningA-success03">
	</div>

	<div class="container content-md">

		<h2 class="text-center">Hệ thống Refine mới</h2>

		<div class="row">

			<div class="col-lg-6 col-refine-left">
				<div class="row-refine">
					<div class="col-refine-main">
						<img :src="currentRefineImg" alt="">

						<div class="refine-success" v-if="showPercent && !showFailed">
							Success <span class="refine-success-percent">{{ percent }}%</span>
						</div>

						<div class="refine-failed" v-if="showPercent && showFailed">
							Upgrade failed!
						</div>

						<div class="refine-main-slot" v-if="!showFailed && !hideMainSlot">
							<img v-if="currentSelectedEQID" :src="refineSourceUrl + currentSelectedEQID + '.png'" alt="">
						</div>

						<div class="refine-main-name" v-if="showEQName">
							{{ getCurrentRefined(true) + currentSelectedEQName }}
						</div>

						<div class="refine-selected-material" v-if="status !== 'init'">
							<img class="elu" :src="getCurrentMaterialImage()" alt="">
							<span v-if="getCurrentMaterialImage()" class="material-quantity">{{ elu }}</span>
						</div>

						<div class="refine-btn" :class="{'has-back' : btnStatus === 'back'}" @click="refine">
							<img class="btn-state" :src="currentRefineBtn" alt="">
							<img class="btn-press" src="<?php echo get_template_directory_uri() . '/images/refine/refine-btn/bt_refining_press.bmp'; ?>" alt="">
							<img class="btn-back-press" v-if="btnStatus === 'back'" src="<?php echo get_template_directory_uri() . '/images/refine/refine-btn/bt_refining_back_press.bmp'; ?>" alt="">

							<span class="text-refine-btn no-select" v-if="btnStatus !== 'init' && btnStatus !== 'back'" :class="{'refine-text-light' : btnStatus !== 'init'}">Refining</span>
							<span class="text-refine-btn refine-text-back no-select refine-text-light" v-if="btnStatus === 'back'">Back</span>
							<span class="price-label no-select" v-if="currentSelectedMaterialID !== ''">2000</span>
						</div>
					</div>
					<div class="col-refine-slots">
						<img src="<?php echo get_template_directory_uri() . '/images/refine/bg_list.bmp'; ?>" alt="">

						<div class="material-slot first-material-slot" v-if="currentSelectedEQID && elu" :class="{'selected' : currentSelectedMaterialID === 985}" @click="selectMaterial(985)">
							<img class="elu no-select" src="<?php echo get_template_directory_uri() . '/images/refine/985.png'; ?>" alt="">
							<span class="material-quantity no-select">{{ elu }}</span>
							<div class="material-name no-select">Elunium</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-refine-right">
				<h3 class="help-text-refine">Hãy thử chọn vật phẩm dưới đây và bắt đầu refine:</h3>

				<div class="row w-slots">
					<div v-for="item in items" class="slot-item" :class="{'selected' : currentSelectedEQID === item.id, 'choose' : item.isChoose}" @click="selectEquip(item.name, item.id)">
						<img :src="refineSourceUrl + item.id + '.png'" alt="">
						<span v-if="item.id === 985">{{ getEluQuantity() }}</span>
					</div>
				</div>

				<div class="refine-speed">
					<div class="form-check">
						<input class="form-check-input" type="radio" name="refine-speed" id="refine-speed-1" value="90" v-model="speed">
						<label class="form-check-label" for="refine-speed-1">
							Tốc độ mặc định
						</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="refine-speed" id="refine-speed-2" value="30" v-model="speed">
						<label class="form-check-label" for="refine-speed-2">
							Tốc độ nhanh
						</label>
					</div>
				</div>

				<div class="refine-description">
					<p>
						Sẽ không còn những dòng chữ nhàm chán nữa.
						Laniakea RO đã tích hợp hệ thống refine mới nhất nhằm tăng sự trải nghiệm cho bạn.
						Nhưng coi chừng quá đà nhé, hư trang bị là mình không đền đâu...
					</p>
				</div>
			</div>
		</div>

	</div>
</section>