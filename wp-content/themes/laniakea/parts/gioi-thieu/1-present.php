<section class="section" id="section-1">
	<div class="container content-lg">
		<div class="w-img-1">
			<img src="<?php echo get_template_directory_uri() . '/images/ro-creator.png'; ?>" alt="">
		</div>

		<div class="text-center short-description">
			<div class="w-title-ro">
                <span>RENEWAL - Episode 17.1</span>
                <hr>
                <span>Level 200/70</span>
			</div>

            <div class="row row-rates mb-5">
                <div class="col">
                    <div>Base Level</div>
                    <div>x <span class="count">30</span></div>
                </div>
                <div class="col">
                    <div>Job Level</div>
                    <div>x <span class="count">30</span></div>
                </div>
                <div class="col">
                    <div>Tỉ lệ rớt đồ</div>
                    <div>x <span class="count">30</span></div>
                </div>
                <div class="col">
                    <div>Tỉ lệ rớt card</div>
                    <div>x <span class="count">5</span></div>
                </div>
                <div class="col">
                    <div>MVP</div>
                    <div>x <span class="count">1</span></div>
                </div>
            </div>

			<p>
				Ragnarok đến với chúng ta như một món quà, nó không đơn giản là một trò chơi mà còn là niềm vui, nỗi buồn, những cảm xúc mạnh mẽ.
				Qua một thời gian khá lâu vắng bóng, Ragnarok Online trở lại với nhiều tính năng mới, thu hút đông đảo game thủ tham gia trên thế giới.
                Ragnarok sẽ mãi như thế, sẽ không bao giờ chết, sẽ luôn tồn tại và <span>Laniakea RO</span> lập ra để mọi người kết nối với nhau, cùng nhau khám phá Ragnarok.
            </p>
		</div>
	</div>
</section>