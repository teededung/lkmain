<section class="section" id="section-2">
    <div class="cbp" id="grid-container">

        <div class="cbp-item cbp-item-album">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <a class="cbp-lightbox" href="<?php echo get_template_directory_uri() . '/images/present/bg.jpg' ?>" data-title="title">
                        <img src="<?php echo get_template_directory_uri() . '/images/present/bg.jpg' ?>" alt="" width="100%">
                    </a>
                </div>
                <div class="cbp-caption-activeWrap">
                    <h3>Battle Ground</h3>
                </div>
            </div>
        </div>

        <div class="cbp-item cbp-item-album">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <a class="cbp-lightbox" href="<?php echo get_template_directory_uri() . '/images/present/dam-cuoi.jpg' ?>" data-title="title">
                        <img src="<?php echo get_template_directory_uri() . '/images/present/dam-cuoi.jpg' ?>" alt="" width="100%">
                    </a>
                </div>
                <div class="cbp-caption-activeWrap">
                    <h3>1 cặp đôi vừa đám cưới</h3>
                </div>
            </div>
        </div>

        <div class="cbp-item cbp-item-album">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <a class="cbp-lightbox" href="<?php echo get_template_directory_uri() . '/images/present/fishing.jpg' ?>" data-title="title">
                        <img src="<?php echo get_template_directory_uri() . '/images/present/fishing.jpg' ?>" alt="" width="100%">
                    </a>
                </div>
                <div class="cbp-caption-activeWrap">
                    <h3>Câu cá lúc rảnh rỗi</h3>
                </div>
            </div>
        </div>

        <div class="cbp-item cbp-item-album">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <a class="cbp-lightbox" href="<?php echo get_template_directory_uri() . '/images/present/new-ripper-slasher.jpg' ?>" data-title="title">
                        <img src="<?php echo get_template_directory_uri() . '/images/present/new-ripper-slasher.jpg' ?>" alt="" width="100%">
                    </a>
                </div>
                <div class="cbp-caption-activeWrap">
                    <h3>Cross Ripper Slasher</h3>
                </div>
            </div>
        </div>

        <div class="cbp-item cbp-item-album">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <a class="cbp-lightbox" href="<?php echo get_template_directory_uri() . '/images/present/new-sacrament.jpg' ?>" data-title="title">
                        <img src="<?php echo get_template_directory_uri() . '/images/present/new-sacrament.jpg' ?>" alt="" width="100%">
                    </a>
                </div>
                <div class="cbp-caption-activeWrap">
                    <h3>Sacrament</h3>
                </div>
            </div>
        </div>

        <div class="cbp-item cbp-item-album">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <a class="cbp-lightbox" href="<?php echo get_template_directory_uri() . '/images/present/soul-reaper-star-emperor.jpg' ?>" data-title="title">
                        <img src="<?php echo get_template_directory_uri() . '/images/present/soul-reaper-star-emperor.jpg' ?>" alt="" width="100%">
                    </a>
                </div>
                <div class="cbp-caption-activeWrap">
                    <h3>Soul Reaper & Star Emperor</h3>
                </div>
            </div>
        </div>

        <div class="cbp-item cbp-item-album">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <a class="cbp-lightbox" href="<?php echo get_template_directory_uri() . '/images/present/star-emperor-test.jpg' ?>" data-title="title">
                        <img src="<?php echo get_template_directory_uri() . '/images/present/star-emperor-test.jpg' ?>" alt="" width="100%">
                    </a>
                </div>
                <div class="cbp-caption-activeWrap">
                    <h3>Star Emperor</h3>
                </div>
            </div>
        </div>

        <div class="cbp-item cbp-item-album">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <a class="cbp-lightbox" href="<?php echo get_template_directory_uri() . '/images/present/bg2.jpg' ?>" data-title="title">
                        <img src="<?php echo get_template_directory_uri() . '/images/present/bg2.jpg' ?>" alt="" width="100%">
                    </a>
                </div>
                <div class="cbp-caption-activeWrap">
                    <h3>Battle Ground</h3>
                </div>
            </div>
        </div>

        <div class="cbp-item cbp-item-album">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <a class="cbp-lightbox" href="<?php echo get_template_directory_uri() . '/images/present/bg3.jpg' ?>" data-title="title">
                        <img src="<?php echo get_template_directory_uri() . '/images/present/bg3.jpg' ?>" alt="" width="100%">
                    </a>
                </div>
                <div class="cbp-caption-activeWrap">
                    <h3>Battle Ground</h3>
                </div>
            </div>
        </div>

        <div class="cbp-item cbp-item-album">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <a class="cbp-lightbox" href="<?php echo get_template_directory_uri() . '/images/present/trinh-dien-thoi-trang.jpg' ?>" data-title="title">
                        <img src="<?php echo get_template_directory_uri() . '/images/present/trinh-dien-thoi-trang.jpg' ?>" alt="" width="100%">
                    </a>
                </div>
                <div class="cbp-caption-activeWrap">
                    <h3>Trình diễn thời trang</h3>
                </div>
            </div>
        </div>

    </div>
</section>