<section class="section" id="section-5">
    <div class="container content-lg">
        <div class="text-center">
            <h2>Quản lý tự động nhặt đồ</h2>

            <div class="row">
                <div class="col-md-3 mb-3 mb-md-0">
                    <img class="img-thumbnail" src="<?php echo get_template_directory_uri() . '/images/quan-ly-nhat-do.png' ?>" alt="quan-ly-nhat-do-laniakea-ragnarok-online">
                </div>
                <div class="col-md-8">
                    <p class="lead">
                        Quản lý tự động nhặt đồ là một tính năng riêng biệt của Laniakea RO. <br>
                        Tính năng này giúp bạn lưu lại danh sách tự động nhặt đồ và bạn có thể dùng lại ở các lần sau.
                    </p>
                    <p class="lead">
                        Không cần phải tốn nhiều gian để gõ lại các lệnh <code>@autoloot</code>, <code>@alootid</code> và <code>@autoloottype</code> mỗi lần đăng nhập.
                    </p>
                    <p class="lead">
                        Để xem hướng dẫn chi tiết bạn xem tại <a href="https://hahiu.com/he-thong-tinh-nang/quan-ly-tu-dong-nhat-do/">đây</a>.
                    </p>
                </div>
            </div>

        </div>
    </div>
</section>