<aside class="sidebar bg-light border p-3">
    <h2 class="">Các bài viết khác</h2>
    <hr>
    <?php $post_objects = get_related_posts(get_the_category()); ?>
    <?php if ($post_objects->have_posts()): ?>
        <?php while ($post_objects->have_posts()): $post_objects->the_post(); ?>
            <div class="media">
                <div class="d-flex mr-3 align-self-start">
                    <a title="<?php the_title(); ?>" href="<?php the_permalink() ?>"><?php the_post_thumbnail('thumbnail', array('class' => 'img-fluid border small-feature-image')) ?></a>
                </div>
                <div class="media-body">
                    <h5 class="mt-0"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
                    <p class="mb-0">
                        <?php echo excerpt(15); ?>
                    </p>
                </div>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
</aside>