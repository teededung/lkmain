<div class="w-table-chars">

    <?php $users = get_ragnarok_users(1); ?>
    <?php if ($users['status'] == 'OK'): ?>

    <div class="row align-items-center">
        <div class="col">
            Total Online: <?php echo $users['count']; ?>
        </div>
        <div class="col text-right">
            <button class="btn btn-primary" @click="getUsers">Get Users</button>
        </div>
    </div>

    <hr>
    <?php endif; ?>

	<div class="filter-and-search margin-bottom-30">
		<div class="row align-items-center">
			<div class="col-8">
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" id="filter-online" name="filter-online" value="1" v-model.number="currentUserTab" class="custom-control-input">
					<label class="custom-control-label" for="filter-online">Online</label>
				</div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="filter-vending" name="filter-vending" value="2" v-model.number="currentUserTab" class="custom-control-input">
                    <label class="custom-control-label" for="filter-vending">Vending</label>
                </div>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" id="filter-offline" name="filter-online" value="3" v-model.number="currentUserTab" class="custom-control-input">
					<label class="custom-control-label" for="filter-offline">Offline</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" id="filter-all" name="filter-online" value="4" v-model.number="currentUserTab" class="custom-control-input">
					<label class="custom-control-label" for="filter-all">Tất cả</label>
				</div>
			</div>
			<div class="col-4">
				<input type="text" class="form-control" v-model="search" placeholder="Search">
			</div>
		</div>
	</div>

	<table class="table">
		<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Account ID</th>
			<th scope="col">Char ID</th>
			<th scope="col">Tên nhân vật</th>
			<th scope="col" class="col-action">Action</th>
		</tr>
		</thead>
		<tbody>
		<tr v-for="(char, i) in filterItems(chars)">
			<th scope="row">{{i+1}}</th>
			<td>{{ char.account_id }}</td>
			<td>{{ char.char_id }}</td>
			<td>{{ char.name }}</td>
			<td>
				<div class="dropdown">
					<button class="btn btn-secondary dropdown-toggle" type="button" :id="'dropdownMenuButton-' + (i+1)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Action
					</button>
					<div class="dropdown-menu" :aria-labelledby="'dropdownMenuButton-' + (i+1)">
						<a class="dropdown-item" @click.prevent="detail(char,i)" href="#">Chi tiết</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" @click.prevent="sendCash(char.account_id)" href="#">Gửi CP</a>
                        <a class="dropdown-item" @click.prevent="sendItems(char.account_id)" href="#">Gửi vật phẩm</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" @click.prevent="deleteItem(char.char_id)" href="#">Xóa vật phẩm</a>
						<a class="dropdown-item" @click.prevent="resetSprites(char.char_id)" href="#">Quay về thời trang gốc</a>
						<a class="dropdown-item" @click.prevent="getOffClothes(char.char_id)" href="#">Cởi hết đồ</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" @click.prevent="banChar(char.char_id)" href="#">Khóa Nhân vật</a>
						<a class="dropdown-item" @click.prevent="banAcc(char.account_id)" href="#">Khóa Tài khoản</a>
						<a class="dropdown-item" @click.prevent="banIP(char.account_id)" href="#">Khóa IP</a>
						<a class="dropdown-item" @click.prevent="showModalGepard(char.account_id)" href="#">Khóa Unique ID</a>
					</div>
				</div>
			</td>
		</tr>
		</tbody>
	</table>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-detail" v-if="char">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Thông tin nhân vật {{ char.name }}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<table class="table mb-0">
                    <tr>
                        <td>Unique ID:</td>
                        <td>{{ char.last_unique_id }}</td>
                    </tr>
					<tr>
						<td>Account ID:</td>
						<td>{{ char.account_id }}</td>
					</tr>
					<tr>
						<td>Char ID:</td>
						<td>{{ char.char_id }}</td>
					</tr>
					<tr>
						<td>Job:</td>
						<td>{{ getJobName(char.class) }}</td>
					</tr>
					<tr>
						<td>Base Level / Job Level</td>
						<td>{{ char.base_level }} / {{ char.job_level }}</td>
					</tr>
					<tr>
						<td>Zeny:</td>
						<td>{{ formatZ(char.zeny) }} zeny</td>
					</tr>
					<tr>
						<td>Cash Point:</td>
						<td>{{ formatZ(char.cp) }} CP</td>
					</tr>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
			</div>
		</div>
	</div>
</div>