<div class="form-group margin-bottom-30">

    <div id="accordion">
        <div class="card">
            <div class="card-header" id="heading-1">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-1" aria-expanded="true" aria-controls="collapse-1">
                        Lấy thông tin quái vật từ divine-pride.net
                    </button>
                </h5>
            </div>
            <div id="collapse-1" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-12">
                            <form id="formGetMobInfo" action="" @submit.prevent="getMobInfo">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" placeholder="ID Quái vật" aria-label="ID Quái vật" v-model="mobID">
                                    <div class="input-group-append">
                                        <button class="btn btn-secondary" type="submit">Get</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-12">
                            <template v-if="mobInfo.id > 0">
                                <table class="table table-bordered mt-3">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Race</th>
                                        <th>Element</th>
                                        <th>Scale</th>
                                        <th>Level</th>
                                        <th>B.ATK</th>
                                        <th>B.MATK</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.name">{{ mobInfo.name }}</td>
                                        <td class="clipboard" :data-clipboard-text="getMobRace(mobInfo.stats.race)">{{ getMobRace(mobInfo.stats.race) }}</td>
                                        <td class="clipboard" :data-clipboard-text="getMobElement(mobInfo.stats.element)">{{ getMobElement(mobInfo.stats.element) }}</td>
                                        <td class="clipboard" :data-clipboard-text="getMobScale(mobInfo.stats.scale)">{{ getMobScale(mobInfo.stats.scale) }}</td>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.stats.level">{{ mobInfo.stats.level }}</td>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.stats.atk1">{{ mobInfo.stats.atk1 }}</td>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.stats.atk2">{{ mobInfo.stats.atk2 }}</td>
                                    </tr>
                                    </tbody>
                                </table>

                                <table class="table table-bordered mt-3">
                                    <thead>
                                    <tr>
                                        <th>Range</th>
                                        <th>STR</th>
                                        <th>AGI</th>
                                        <th>VIT</th>
                                        <th>INT</th>
                                        <th>DEX</th>
                                        <th>LUK</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.stats.attackRange">{{ mobInfo.stats.attackRange }}</td>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.stats.str">{{ mobInfo.stats.str }}</td>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.stats.agi">{{ mobInfo.stats.agi }}</td>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.stats.vit">{{ mobInfo.stats.vit }}</td>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.stats.int">{{ mobInfo.stats.int }}</td>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.stats.dex">{{ mobInfo.stats.dex }}</td>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.stats.luk">{{ mobInfo.stats.luk }}</td>
                                    </tr>
                                    </tbody>
                                </table>

                                <table class="table table-bordered mt-3">
                                    <thead>
                                    <tr>
                                        <th>Speed</th>
                                        <th>DEF</th>
                                        <th>MDEF</th>
                                        <th>B.EXP</th>
                                        <th>J.EXP</th>
                                        <th>aDelay</th>
                                        <th>aMotion</th>
                                        <th>dMotion</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.stats.movementSpeed">{{ mobInfo.stats.movementSpeed }}</td>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.stats.defense">{{ mobInfo.stats.defense }}</td>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.stats.magicDefense">{{ mobInfo.stats.magicDefense }}</td>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.stats.baseExperience">{{ mobInfo.stats.baseExperience }}</td>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.stats.jobExperience">{{ mobInfo.stats.jobExperience }}</td>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.stats.rechargeTime">{{ mobInfo.stats.rechargeTime }}</td>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.stats.attackSpeed">{{ mobInfo.stats.attackSpeed }}</td>
                                        <td class="clipboard" :data-clipboard-text="mobInfo.stats.attackedSpeed">{{ mobInfo.stats.attackedSpeed }}</td>
                                    </tr>
                                    </tbody>
                                </table>

                                <div class="mb-3">
                                    <h5>DB Text:</h5>
                                    <code data-toggle="tooltip" data-placement="top" title="Copy" class="clipboard" :data-clipboard-text="mobInfo.mobDBText">
                                        {{ mobInfo.mobDBText }}
                                    </code>
                                </div>

                                <div class="mb-3">
                                    <h5>Skill DB <small>(chỉ để tham khảo)</small>:</h5>
                                    <ol v-if="mobInfo.mobSkillTexts.length">
                                        <li class="clipboard mb-1" v-for="skill in mobInfo.mobSkillTexts" :data-clipboard-text="skill">{{ skill }}</li>
                                    </ol>
                                </div>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header" id="heading-7">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-7" aria-expanded="false" aria-controls="collapse-7">
                        Đếm vật phẩm có trong server
                    </button>
                </h5>
            </div>

            <div id="collapse-7" class="collapse" aria-labelledby="heading-7" data-parent="#accordion">
                <div class="card-body">
                    <form action="" method="post" @submit.prevent="countItemInDatabase">
                        <div class="input-group mb-0">
                            <input type="text" placeholder="Điền item id muốn đếm" v-model="itemIdToCount" class="form-control">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header" id="heading-2">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-2" aria-expanded="false" aria-controls="collapse-2">
                        Convert Time
                    </button>
                </h5>
            </div>

            <div id="collapse-2" class="collapse" aria-labelledby="heading-2" data-parent="#accordion">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <input type="text" class="form-control" v-model="hours" placeholder="Số giờ">
                        </div>
                        <div class="col">
                            <span data-toggle="tooltip" data-placement="top" title="Copy" class="clipboard" :data-clipboard-text="seconds">{{ seconds }} giây</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!--        <div class="card">-->
<!--            <div class="card-header" id="heading-7">-->
<!--                <h5 class="mb-0">-->
<!--                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-7" aria-expanded="false" aria-controls="collapse-7">-->
<!--                        Gửi lại Email kích hoạt cho bài viết đăng kí tài khoản-->
<!--                    </button>-->
<!--                </h5>-->
<!--            </div>-->
<!---->
<!--            <div id="collapse-7" class="collapse" aria-labelledby="heading-7" data-parent="#accordion">-->
<!--                <div class="card-body">-->
<!--                    <form action="" method="post" @submit.prevent="resendActivationEmail">-->
<!--                        <div class="form-group" class="mb-0">-->
<!--                            <input type="text" placeholder="Điền post_id của bài viết đăng kí tài khoản" v-model="postIdToSendActivationEmail" class="form-control">-->
<!--                        </div>-->
<!--                    </form>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->

        <div class="card">
            <div class="card-header" id="heading-3">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-3" aria-expanded="false" aria-controls="collapse-3">
                        Công cụ tạo mã thưởng
                    </button>
                </h5>
            </div>
            <div id="collapse-3" class="collapse" aria-labelledby="heading-3" data-parent="#accordion">
                <div class="card-body">
                    <div class="row align-items-center">
                        <form action="" class="w-100" id="form-redeem-code" method="post" @submit.prevent="generateRedeemCodes">
                            <div class="col-12 margin-bottom-20">
                                <h3>Công cụ tạo mã thưởng</h3>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" v-model="redeemCodeNumber" placeholder="Số lượng mã">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" v-model="redeemCodeCount" placeholder="Số lượng sử dụng">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" v-model="redeemCodeRewards" placeholder="ID,quantity:ID,quantity">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" v-model="redeemCodePrefix" placeholder="Prefix">
                                </div>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">TẠO</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header" id="heading-4">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-4" aria-expanded="false" aria-controls="collapse-4">
                        Convert Skill Lua to Skill HTML
                    </button>
                </h5>
            </div>
            <div id="collapse-4" class="collapse" aria-labelledby="heading-4" data-parent="#accordion">
                <div class="card-body">
                    <div class="form-group">
                        <textarea class="form-control" v-model="skillLua" placeholder="Lua Text" @input="convertLuaToHTML" cols="30" rows="10"></textarea>
                    </div>
                    <hr>
                    <div class="form-group">
                        <textarea class="form-control" id="skillHTML" placeholder="HTML code" cols="30" rows="10">{{ skillHTML }}</textarea>
                    </div>
                    <button class="btn btn-primary clipboard" data-clipboard-target="#skillHTML">Copy HTML code to clipboard</button>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header" id="heading-5">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-5" aria-expanded="false" aria-controls="collapse-5">
                        Xóa item id trong database
                    </button>
                </h5>
            </div>
            <div id="collapse-5" class="collapse" aria-labelledby="heading-5" data-parent="#accordion">
                <div class="alert alert-warning mb-0">
                    Đảm bảo là server đang bảo trì!
                </div>
                <div class="card-body">
                    <form action="" @submit.prevent="removeItemIDInDatabase">
                        <div class="input-group mb-3">
                            <input type="number" v-model="itemIDToRemove" class="form-control" placeholder="Item ID" aria-label="Item ID" aria-describedby="btn-submit-item-id">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit" id="btn-submit-item-id">Submit</button>
                            </div>
                        </div>
                    </form>

                    <div class="db-log" v-if="dbLog" data-html="dbLog"></div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header" id="heading-6">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-6" aria-expanded="false" aria-controls="collapse-6">
                        Thay đổi item id trong database
                    </button>
                </h5>
            </div>
            <div id="collapse-6" class="collapse" aria-labelledby="heading-6" data-parent="#accordion">
                <div class="alert alert-warning mb-0">
                    Đảm bảo là server đang bảo trì!
                </div>
                <div class="card-body">
                    <form action="" @submit.prevent="changeItemIDInDatabase">
                        <div class="input-group mb-3">
                            <input type="number" v-model="oldItemID" class="form-control" placeholder="Old Item ID" aria-label="Old Item ID" aria-describedby="btn-submit-change-item-id">
                            <input type="number" v-model="newItemID" class="form-control" placeholder="New Item ID" aria-label="New Item ID" aria-describedby="btn-submit-change-item-id">

                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit" id="btn-submit-change-item-id">Submit</button>
                            </div>
                        </div>
                    </form>
                    
                    <div class="db-log" v-if="dbLog" data-html="dbLog"></div>
                </div>
            </div>
        </div>
    </div>
</div>