<?php
/*
Template Name: Full Width Page
*/
?>

<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>

<div class="container-fluid content-sm">
		
	<?php the_content(); ?>
	
</div><!-- /.container -->

<?php endwhile; ?>
<?php get_footer(); ?>
