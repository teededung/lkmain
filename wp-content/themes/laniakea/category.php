<?php get_header(); ?>

<div class="container content">
    <div class="row">

        <div class="col-md-8">

            <h1>Danh mục: <?php echo single_cat_title(); ?></h1>

			<?php
			if (have_posts()):
				while (have_posts()): the_post();
					get_template_part('/parts/loop/loop-news');
				endwhile;
			endif;
			?>
        </div>

        <div class="col-md-4">
			<?php get_template_part('/parts/sidebar'); ?>
        </div>

    </div>
</div>

<?php get_footer(); ?>
