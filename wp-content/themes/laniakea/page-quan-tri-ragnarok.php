<?php
if (!is_user_logged_in() && !is_super_admin(get_current_user_id())):
	wp_redirect(home_url('/'));
	exit;
endif;
?>

<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

	<div class="container content" id="ragnarok-manage">
        <div class="row">
            <div class="col-lg-8">
                <template v-if="mode === 'chars'">
                    <?php get_template_part('/parts/quan-tri/chars'); ?>
                </template>
                <template v-if="mode === 'tools'">
                    <?php get_template_part('/parts/quan-tri/tools'); ?>
                </template>
                <template v-if="mode === 'redeem_code'">
                    <div class="margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="filter-code-available" name="filter-online" value="1" v-model="getAvailableRedeemCode" class="custom-control-input">
                                    <label class="custom-control-label" for="filter-code-available">Còn</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="filter-code-not-available" name="filter-online" value="0" v-model="getAvailableRedeemCode" class="custom-control-input">
                                    <label class="custom-control-label" for="filter-code-not-available">Đã xài</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="filter-code-all" name="filter-online" value="2" v-model="getAvailableRedeemCode" class="custom-control-input">
                                    <label class="custom-control-label" for="filter-code-all">Tất cả</label>
                                </div>
                            </div>
                            <div class="col">
                                <button class="btn btn-primary" @click="getRedeemCodes">Lấy danh sách coupon</button>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" v-if="redeemCodes.length > 0">
                        <label for="filter-redeem-code">Lọc theo chữ: </label>
                        <input type="text" class="form-control" id="filter-redeem-code" v-model="filterRedeemCodeText" placeholder="Điền chữ bạn muốn lọc">
                    </div>

                    <table class="table" v-if="redeemCodes.length > 0">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Code</th>
                            <th scope="col">Count</th>
                            <th scope="col">Rewards</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="(item, i) in filterRedeemCodes(redeemCodes)">
                            <th scope="row">{{i+1}}</th>
                            <td>{{ item.code }}</td>
                            <td>{{ item.count }}</td>
                            <td>{{ item.rewards }}</td>
                        </tr>
                        </tbody>
                    </table>
                </template>
            </div>
            <div class="col-lg-3">
                <ul class="list-group">
                    <li class="list-group-item" :class="{'active' : mode === 'chars'}" @click="mode = 'chars'">Tất cả nhân vật</li>
                    <li class="list-group-item" :class="{'active' : mode === 'tools'}" @click="mode = 'tools'">Công cụ</li>
                    <li class="list-group-item" :class="{'active' : mode === 'redeem_code'}" @click="mode = 'redeem_code'">Redeem Codes</li>
                </ul>
            </div>
        </div>

        <div class="modal fade" tabindex="-1" role="dialog" id="modal">
            <div class="modal-dialog" role="document">
                <form class="modal-content" @submit.prevent="banGepard">
                    <div class="modal-header">
                        <h5 class="modal-title">Ban Gepard</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="account_id">Account ID</label>
                            <input type="text" class="form-control" v-model="accountIDToBan" id="account_id">
                        </div>
                        <div class="form-group">
                            <label for="calendar-a">Unban Time:</label>
                            <input type="text" v-model="unbanTime" name="unban-time" class="form-control" id="calendar-a" placeholder="Unban Time">
                        </div>
                        <div class="form-group">
                            <label for="reason">Reason:</label>
                            <input type="text" v-model="reason" name="reason" class="form-control" id="reason" placeholder="Reason">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    </div>
                </form>
            </div>
        </div>
	</div>

<?php endwhile; ?>
<?php get_footer(); ?>
