<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>
    <?php $terms = get_the_terms( get_the_ID(), 'album_category' ); ?>

    <?php
    if (!empty($terms)):
        $term = array_shift( $terms );
        $slug = $term->slug;
    endif;
    ?>

    <div class="container content-sm" id="album-can-vote">
        <nav aria-label="breadcrumb" class="mb-5">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo home_url('/') ?>">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="<?php echo home_url('/album/') ?>">Album</a></li>
                <li class="breadcrumb-item"><a href="<?php echo home_url('/album/' . $slug) ?>"><?php echo $term->name ?></a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
            </ol>
        </nav>

        <h1 class="margin-bottom-50"><?php the_title(); ?></h1>

        <?php
        $terms = get_the_terms( get_the_ID(), 'album_category' );
        if (!empty($terms)):
            $term = array_shift($terms);
            $allow_vote = get_field('allow_vote', $term);

            if ($allow_vote):
                $can_vote = false;
                $event_name = get_field('event_name', $term);

                $start_time_str = get_field('vote_start_time', $term);
                $end_time_str   = get_field('vote_end_time', $term);

                if ($end_time_str == "" || ($start_time_str == "" && $end_time_str == "")):
                    $can_vote = true;
                    $vote_message = "đang diễn ra.";
                else:
                    $start_time = strtotime($start_time_str);
                    $end_time = strtotime($end_time_str);

                    $now = time();

                    if ($now > $start_time):
                        if ($now < $end_time):
                            $can_vote = true;
                            $vote_message = "đang diễn ra.";
                        else:
                            $vote_message = "đã kết thúc vào ngày " . date("d/m/Y", $end_time)  . '.';
                        endif;
                    else:
                        $vote_message = "sẽ bắt đầu vào ngày <strong>" . date("d/m/Y", $start_time) . "</strong> lúc <strong>" . date('H:i', $start_time) . "</strong> (" . human_time_diff( current_time( 'timestamp' ,7), $start_time ) . " nữa).";
                    endif;
                endif;
            endif;
        endif;
        ?>

        <?php if ($allow_vote): ?>
            <div class="alert alert-info mb-3">
                Sự kiện bình chọn album <strong><?php the_field('event_name', $term); ?></strong>&nbsp;<?php echo $vote_message ?><br>
            </div>
        <?php endif; ?>

        <div class="cbp mb-5" id="grid-container">
            <?php $gallery = get_field('gallery'); ?>

            <?php if ($gallery): ?>
                <?php foreach ($gallery as $index => $item): ?>
                    <div class="cbp-item">
                        <a class="cbp-singlePageInline" data-index="<?php echo $index ?>" data-post-id="<?php echo get_the_ID(); ?>" href="<?php echo $item['url']; ?>" data-title="<?php echo esc_html($item['description']); ?>">
                            <img src="<?php echo $item['url']; ?>" alt="<?php echo esc_html($item['alt']); ?>" width="100%">
                        </a>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>

        <?php
        if ($allow_vote):
            $author = get_field('char_name');
            $votes = get_field('votes');
            $names = array();
            if ($votes):
                $albumVotes = count($votes);
                foreach($votes as $vote):
                    $names[] = $vote['name'];
                endforeach;
            else:
                $albumVotes = 0;
            endif;
            ?>

            <div class="card border" id="vote-album">
                <div class="card-body">
                    <input type="hidden" id="eventName" value="<?php echo $event_name; ?>">
                    <input type="hidden" id="albumVotes" value="<?php echo $albumVotes; ?>">

                    <h5 class="card-title">Album <?php echo $event_name; ?> của <?php echo $author; ?></h5>
                    <p class="card-text mb-3">
                        Tổng số hình: <?php echo count($gallery); ?><br>
                        Tổng số lượt bình chọn bằng facebook: <span>{{ albumVotes }}</span><br>
                    </p>
                    <?php if ($votes): ?>
                        Người bình chọn: <?php echo implode(', ', $names) ?>
                    <?php endif; ?>

                    <div class="mb-3">
                        <div class="share">
                            <span>Chia sẻ qua</span>
                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_url(get_the_permalink()); ?>" data-toggle="tooltip" data-placement="top" title="Chia sẻ qua Facebook" class="social share-facebook">
                                <i class="ion-social-facebook"></i>
                            </a>
                            <a target="_blank" href="https://twitter.com/home?status=<?php echo esc_url(get_the_permalink()); ?>" data-toggle="tooltip" data-placement="top" title="Chia sẻ qua Twitter" class="social share-twitter">
                                <i class="ion-social-twitter"></i>
                            </a>
                        </div>
                    </div>

                    <?php if ($can_vote): ?>
                        <div class="mb-3">
                            <a href="#" @click.prevent="preVote(<?php echo get_the_ID(); ?>, `<?php echo $author ?>`)" class="btn btn-secondary">Bình chọn cho <?php echo $author; ?></a>
                        </div>
                        <?php if ($end_time_str != ""): ?>
                        <div class="alert alert-info mb-0">
                            Thời gian kết thúc bình chọn: <?php echo date("d/m/Y", $end_time); ?> lúc <?php echo date("H:i", $end_time) ?> (<?php echo human_time_diff( current_time( 'timestamp' ,7), $end_time ) ?> nữa).
                        </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>

<?php endwhile; ?>
<?php get_footer(); ?>