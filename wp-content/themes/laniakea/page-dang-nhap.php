<?php
if (isset($_COOKIE['laniakeaA'])):
    $auth = $_COOKIE['laniakeaA'];
    $account = ragnarok_login_by_auth($auth);

    if (is_data_return_ok($account)):
        wp_redirect(home_url('/tai-khoan/'), 301);
        exit;
    else:
        setcookie('laniakeaA', null, -1, '/');
        header("Refresh:0");
        exit;
    endif;
endif;
?>

<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>
    <div class="container content-md">
        <h1>Đăng nhập</h1>
        <form id="login-form" action="" method="post" @submit.prevent="login" v-cloak="">
            <template v-if="!loginOK">
                <div class="form-group">
                    <label for="username">Tên đăng nhập</label>
                    <input type="text" name="username" v-model="username" data-vv-name="username" data-vv-as="Tên đăng nhập" data-vv-delay="1000" v-validate="'required|min:6|max:30'" min="4" max="30" :class="{'is-invalid' : errors.has('username')}" class="form-control input" id="username" placeholder="Nhập tên đăng nhập">
                    <div class="invalid-feedback" v-if="errors.has('username')">
                        {{ errors.first('username') }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="password">Mật khẩu</label>
                    <input type="password" name="password" v-model="password" data-vv-name="password" data-vv-as="Mật khẩu" data-vv-delay="1000" v-validate="'required|min:6|max:30'" min="6" max="30" :class="{'is-invalid' : errors.has('password')}" class="form-control input" id="password" placeholder="Nhập mật khẩu của bạn">
                    <div class="invalid-feedback" v-if="errors.has('password')">
                        {{ errors.first('password') }}
                    </div>
                </div>

                <button class="btn btn-primary" type="submit">Đăng nhập</button>

                <div class="mt-4">
                    <a href="<?php echo home_url('/quen-mat-khau/'); ?>">Bạn quên mật khẩu?</a>
                </div>
            </template>
            <template v-else>
                <h3>Đăng nhập thành công!</h3>
                <p>Trang đang chuyển hướng...</p>
            </template>
        </form>
    </div>
<?php endwhile;; ?>
<?php get_footer(); ?>
