<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

    <div class="container content">
        <div class="row">
            <div class="col-md-8">
                <h1 class="page-title">Đọc Tin tức cùng <small>Laniakea RO</small></h1>

                <?php
                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                $args = array(
                    'post_type' => 'post',
                    'category_name'  => 'thong-bao',
                    'posts_per_page' => 10,
                    'paged' => $paged,
                    'post_status' => 'publish'
                );

                $news = new WP_Query($args);

                if ($news->have_posts()):
                    while ($news->have_posts()): $news->the_post();
                        get_template_part('/parts/loop/loop-news');
                    endwhile;
                endif;
                ?>
            </div>

            <div class="col-md-4">
                <?php get_template_part('/parts/sidebar/sidebar-search'); ?>
            </div>
        </div>
    </div>

<?php endwhile; ?>
<?php get_footer(); ?>
