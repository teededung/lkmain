<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>

    <div class="container content">

        <h1 class="text-center">Dev Notes</h1>

        <div class="frst-container">
            <div class="frst-timeline frst-timeline-style-18 frst-alternate frst-date-opposite">

                <?php
                $args = array(
                    'post_type'         => 'post',
                    'category_name'     => 'cap-nhat',
                    'posts_per_page'    => -1,
                    'post_status'       => 'publish'
                );

                $notes = new WP_Query($args);
                ?>

                <div class="frst-timeline-block frst-timeline-label-block">
                    <div class="frst-labels frst-start-label">
                        <span>2018</span>
                    </div>
                </div>

                <?php
                if ($notes->have_posts()):
                    while ($notes->have_posts()): $notes->the_post();
                        get_template_part('/parts/loop/loop-note');
                    endwhile;
                endif;

                wp_reset_postdata();
                ?>

            </div>
        </div>
    </div>

<?php endwhile; ?>
<?php get_footer(); ?>